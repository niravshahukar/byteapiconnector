﻿using LOSConnector.Core.Data.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Infrastructure.Database
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
    : base(options) { }
        public virtual DbSet<LOSBatchs> LOSBatches { get; set; }
        public virtual DbSet<LOSBatchDetails> LOSBatchDetails { get; set; }
        public virtual DbSet<FieldType> FieldType { get; set; }
        public virtual DbSet<FieldMapping> FieldMapping { get; set; }
        public virtual DbSet<BotConfiguration> BotConfiguration { get; set; }
        public virtual DbSet<BotFieldMapping> BotFieldMapping { get; set; }
    }
}
