﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class Package
    {
        public string PackageGuid { get; set; }
        public string PackageName { get; set; }
        public string DocSetGuid { get; set; }
        public string MetaDataSchemaName { get; set; }
        public string MetaData { get; set; }
        public List<string> RuleSets { get; set; }
        public object CreateDate { get; set; }
        public object ModDate { get; set; }
        public string Status { get; set; }
    }
}
