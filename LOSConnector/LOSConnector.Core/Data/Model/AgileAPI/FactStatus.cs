﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class FactStatus
    {
        public object Action { get; set; }
        public string Agent { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        public bool Evaluated { get; set; }
        public string Guid { get; set; }
        public bool Hidden { get; set; }
        public Identifier Identifier { get; set; }
        public string Name { get; set; }
        public string ObjectType { get; set; }
        public string Reason { get; set; }
        public Reasons Reasons { get; set; }
        public List<string> Rules { get; set; }
        public bool Status { get; set; }
        public Statuses Statuses { get; set; }
        public List<string> Tags { get; set; }
    }
}
