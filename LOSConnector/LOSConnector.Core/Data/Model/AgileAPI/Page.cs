﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class Page
    {
        public string PageGuid { get; set; }
        public object DocGuid { get; set; }
        public string PageType { get; set; }
        public string ImageFileName { get; set; }
        public string ThumbNailFileName { get; set; }
        public string PdfFileName { get; set; }
        public object OcrFileName { get; set; }
        public string PageProperties { get; set; }
        public object PageFeatures { get; set; }
        public object Classifications { get; set; }
        public double PageTypeConfidence { get; set; }
        public object AutoPageType { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public long CreateDate { get; set; }
        public long ModDate { get; set; }
    }
}
