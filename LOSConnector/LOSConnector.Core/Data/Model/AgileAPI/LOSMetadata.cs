﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class LOSMetadata
    {
        public string FieldFor { get; set; }
        public string LOSEntityID { get; set; }
        public string LOSTable { get; set; }
        public string LOSFieldID { get; set; }
        public string FieldValue { get; set; }
        public string FieldNameAlias { get; set; }
    }
}
