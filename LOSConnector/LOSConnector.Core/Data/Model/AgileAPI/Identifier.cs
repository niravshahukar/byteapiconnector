﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class Identifier
    {
        public string DocTypeName { get; set; }
        public string DocTypeGuid { get; set; }
        public string SatisfiedDynamicActionMap { get; set; }
        public string NotSatisfiedDynamicActionMap { get; set; }
        public string Name { get; set; }
        public string Guid { get; set; }
        public object NotSatisfiedDynamicActionType { get; set; }
        public object SatisfiedDynamicActionType { get; set; }
        public string Flip { get; set; }
        public string Enabled { get; set; }
        public string DocGuid { get; set; }
    }
}
