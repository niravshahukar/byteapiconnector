﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class Labels
    {
        public string Batch_guid { get; set; }
        public string Body { get; set; }
        public string Package_name { get; set; }
        public string Subject { get; set; }
        public string Topic { get; set; }
    }
}
