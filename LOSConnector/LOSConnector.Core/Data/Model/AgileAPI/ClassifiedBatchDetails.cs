﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class ClassifiedBatchDetails
    {
        public string BatchGuid { get; set; }
        public string BatchProperties { get; set; }
        public string BatchFileName { get; set; }
        public string BatchDirectory { get; set; }
        public int ContentChecksum { get; set; }
        public List<Page> Pages { get; set; }
        public List<ProcessedDocument> ProcessedDocuments { get; set; }
        public List<string> ProcessInstanceGuids { get; set; }
        public string State { get; set; }
        public object ExportFileName { get; set; }
        public long CreateDate { get; set; }
        public long ModDate { get; set; }
    }
}
