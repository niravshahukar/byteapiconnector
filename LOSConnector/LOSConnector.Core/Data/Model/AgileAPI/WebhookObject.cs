﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class WebhookObject
    {
        public string Receiver { get; set; }
        public string Status { get; set; }
        public List<Alert> Alert { get; set; }
        public GroupLabels GroupLabels { get; set; }
        public CommonLabels CommonLabels { get; set; }
        public CommonAnnotations CommonAnnotations { get; set; }
        public string ExternalURL { get; set; }
        public string Version { get; set; }
        public string GroupKey { get; set; }
    }
}
