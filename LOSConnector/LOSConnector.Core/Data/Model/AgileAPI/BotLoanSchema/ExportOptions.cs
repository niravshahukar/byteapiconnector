﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace LOSConnector.Core.Data.Model.AgileAPI.BotLoanSchema
{
    public class ExportOptions
    {
        [JsonPropertyName("documentBundling")]
        public string documentBundling { get; set; }
        [JsonPropertyName("documentNaming")]
        public DocumentNaming documentNaming { get; set; }
        [JsonPropertyName("folderGrouping")]
        public List<string> folderGrouping { get; set; }
        public ExportOptions()
        {
            documentBundling = "Individual Documents";
            documentNaming = new DocumentNaming();
            folderGrouping = new List<string>(){
                "Client ID",
                "Loan Number",
                "Document Group",
                "Document Type" };
        }
    }
}
