﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace LOSConnector.Core.Data.Model.AgileAPI.BotLoanSchema
{
    public class NotificationOptions
    {
        [JsonPropertyName("recepient")]
        public string Recepient { get; set; }
        public NotificationOptions()
        {
            Recepient = "aifoundry@hotmail.com";
        }
    }
}
