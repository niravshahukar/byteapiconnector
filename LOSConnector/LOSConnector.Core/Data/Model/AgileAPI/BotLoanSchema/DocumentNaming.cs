﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace LOSConnector.Core.Data.Model.AgileAPI.BotLoanSchema
{
    public class DocumentNaming
    {
        [JsonPropertyName("name")]
        public List<string> name { get; set; }
        [JsonPropertyName("separator")]
        public string separator { get; set; }
        public DocumentNaming()
        {
            name = new List<string>() {"Client ID",
                    "Loan Number",
                    "Document Group",
                    "Document Type",
                    "Borrower Name",
                    "Loan Type",
                    "Number of Pages",
                    "Creation Date" };
            separator = "_";
        }
    }
}
