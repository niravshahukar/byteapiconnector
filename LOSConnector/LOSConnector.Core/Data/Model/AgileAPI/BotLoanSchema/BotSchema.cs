﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace LOSConnector.Core.Data.Model.AgileAPI.BotLoanSchema
{
    public class BotSchema
    {
        [JsonPropertyName("packageName")]
        public string PackageName { get; set; }
        [JsonPropertyName("metaDataSchemaName")]
        public string MetaDataSchemaName { get; set; }
        [JsonPropertyName("metaData")]
        public string MetaData { get; set; }
        [JsonPropertyName("ruleSets")]
        public List<string> RuleSets { get; set; }        
    }
}
