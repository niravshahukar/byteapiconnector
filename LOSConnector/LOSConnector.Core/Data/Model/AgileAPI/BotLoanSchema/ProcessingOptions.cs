﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace LOSConnector.Core.Data.Model.AgileAPI.BotLoanSchema
{
    public class ProcessingOptions
    {
        [JsonPropertyName("classificationMethod")]
        public string classificationMethod { get; set; }
        [JsonPropertyName("processName")]
        public string processName { get; set; }
        public ProcessingOptions()
        {
            classificationMethod = "visual";
            processName = "Batch_Process";
        }
    }
}
