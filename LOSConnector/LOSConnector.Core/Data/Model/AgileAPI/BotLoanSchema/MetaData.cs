﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace LOSConnector.Core.Data.Model.AgileAPI.BotLoanSchema
{
    public class MetaData
    {
        [JsonPropertyName("clientId")]
        public string ClientId { get; set; }
        [JsonPropertyName("loanNumber")]
        public string LoanNumber { get; set; }
        [JsonPropertyName("loanType")]
        public string LoanType { get; set; }
        [JsonPropertyName("borrowerName")]
        public string BorrowerName { get; set; }
        [JsonPropertyName("processingOptions")]
        public ProcessingOptions ProcessingOptions { get; set; }
        [JsonPropertyName("exportOptions")]
        public ExportOptions ExportOptions { get; set; }
        [JsonPropertyName("notificationOptions")]
        public NotificationOptions NotificationOptions { get; set; }
        [JsonPropertyName("requiredDocuments")]
        public List<string> RequiredDocuments { get; set; }
        public MetaData()
        {
            ProcessingOptions = new ProcessingOptions();
            ExportOptions = new ExportOptions();
            NotificationOptions = new NotificationOptions();
            RequiredDocuments = new List<string>();
        }
    }
}
