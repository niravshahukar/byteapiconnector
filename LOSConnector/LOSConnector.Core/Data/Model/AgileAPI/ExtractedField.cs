﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class ExtractedField
    {
        public string FieldName { get; set; }
        public string FieldAlias { get; set; }
        public object FieldTags { get; set; }
        public string FieldGuid { get; set; }
        public int RowNumber { get; set; }
        public object TemplateFieldName { get; set; }
        public object TemplateFieldGuid { get; set; }
        public string FieldProperties { get; set; }
        public DataType DataType { get; set; }
        public List<FieldValue> FieldValues { get; set; }
        public ExtractedField()
        {
            FieldValues = new List<FieldValue>();
        }
    }
}
