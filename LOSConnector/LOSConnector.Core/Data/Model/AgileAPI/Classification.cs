﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class Classification
    {
        public string Type { get; set; }
        public int Confidence { get; set; }
        public object Score { get; set; }
        public object RawScore { get; set; }
        public string TypeGuid { get; set; }
    }
}
