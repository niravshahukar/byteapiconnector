﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class CommonLabels
    {
        [Required]
        public string Batch_guid { get; set; }
        public string Body { get; set; }
        [Required]
        public string Package_name { get; set; }
        public string Subject { get; set; }
        public string Topic { get; set; }
    }
}
