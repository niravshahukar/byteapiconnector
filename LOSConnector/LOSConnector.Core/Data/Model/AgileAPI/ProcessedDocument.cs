﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class ProcessedDocument
    {
        public List<Classification> Classifications { get; set; }
        public long CreateDate { get; set; }
        public string DocGuid { get; set; }
        public string DocProperties { get; set; }
        public string DocType { get; set; }
        public string DocTypeAlias { get; set; }
        public string DocTypeGroup { get; set; }
        public double DocTypeConfidence { get; set; }
        public string DocTypeGuid { get; set; }
        public List<ExtractedField> ExtractedFields { get; set; }
        public string ExportFileName { get; set; }
        public object ReviewType { get; set; }
        public long ModDate { get; set; }
        public List<string> PageGuids { get; set; }
        public ProcessedDocument()
        {
            TempData();
        }

        private void TempData()
        {
            ExtractedFields = new List<ExtractedField>
            {
                new ExtractedField
                {
                    FieldName = "ExtendedTextValue.ChetuBorrower",
                    FieldValues = new List<FieldValue>
                    {
                    new FieldValue
                    {
                    Value = "Liyakat 30mar"
                    }
                    }
                },
                new ExtractedField
                {
                    FieldName = "ExtendedDateValue.ChetuLoanDate",
                    FieldValues=new List<FieldValue>
                    {
                    new FieldValue
                    {
                    Value = "03/30/2020"
                    }
                    }
                },
                new ExtractedField
                {
                    FieldName = "ExtendedDecimalValue.ChetuLoanAmount",
                    FieldValues=new List<FieldValue>
                    {
                    new FieldValue
                    {
                    Value = "120"
                    }
                    }
                },
                new ExtractedField
                {
                    FieldName = "ExtendedDecimalValue.ChetuLoanInterest",
                    FieldValues=new List<FieldValue>
                    {
                    new FieldValue
                    {
                    Value = "10"
                    }
                    }
                },
                new ExtractedField
                {
                    FieldName = "CustomField.Field01",
                    FieldValues=new List<FieldValue>
                    {
                    new FieldValue
                    {
                    Value = "Richi 30mar"
                    }
                    }
                },
                new ExtractedField
                {
                    FieldName = "CustomField.Field02",
                    FieldValues=new List<FieldValue>
                    {
                    new FieldValue
                    {
                    Value = "200"
                    }
                    }
                }
            };
        }
    }
}
