﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class GroupLabels
    {
        public string Batch_guid { get; set; }
        public string Topic { get; set; }
    }
}
