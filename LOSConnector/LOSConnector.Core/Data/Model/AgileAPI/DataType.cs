﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class DataType
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string AllowCharFormat { get; set; }
        public string RegEx { get; set; }
        public int MinChar { get; set; }
        public object MaxChar { get; set; }
    }
}
