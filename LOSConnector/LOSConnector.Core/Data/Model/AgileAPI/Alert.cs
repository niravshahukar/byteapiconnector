﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class Alert
    {
        public string Status { get; set; }
        public Labels Labels { get; set; }
        public Annotations Annotations { get; set; }
        public string StartsAt { get; set; }
        public string EndsAt { get; set; }
        public string GeneratorURL { get; set; }
    }
}
