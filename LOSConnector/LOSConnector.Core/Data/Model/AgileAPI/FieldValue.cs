﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.AgileAPI
{
    public class FieldValue
    {
        public string Value { get; set; }
        public string NormalizedValue { get; set; }
        public string PageGuid { get; set; }
        public double Confidence { get; set; }
        public BoundingBox BoundingBox { get; set; }
        public bool Valid { get; set; }
    }
}
