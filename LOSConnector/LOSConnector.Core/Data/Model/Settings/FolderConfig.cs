﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.Settings
{
    public class FolderConfig
    {
        public string UploadDocPath { get; set; }
        public string DownloadDocPath { get; set; }
    }
}
