﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.Settings
{
    public class ByteProAPIConfig
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ServerName { get; set; }
        public string SiteName { get; set; }
        public string AuthorizationKey { get; set; }
        public string DocumentStatus { get; set; }
    }
}
