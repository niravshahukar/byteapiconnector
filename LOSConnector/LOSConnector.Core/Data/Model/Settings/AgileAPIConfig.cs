﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.Settings
{
    public class AgileAPIConfig
    {
        public string CEHost { get; set; }
        public string CEUser { get; set; }
        public string CEPassword { get; set; }
        public string CESecret { get; set; }
        public string PackageSchema { get; set; }
        public string PackageStatus { get; set; }
        public List<string> RequiredDocuments { get; set; }
    }
}
