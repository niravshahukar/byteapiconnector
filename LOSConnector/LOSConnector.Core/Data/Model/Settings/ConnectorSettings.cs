﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.Settings
{
    public class ConnectorSettings
    {
        public ByteProAPIConfig ByteProAPIConfig { get; set; }
        public AgileAPIConfig AgileAPIConfig { get; set; }
        public FolderConfig FolderConfig { get; set; }
        public OtherConfig OtherConfig { get; set; }
    }
}
