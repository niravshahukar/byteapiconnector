﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.Settings
{
    public class OtherConfig
    {
        public string FileDescription { get; set; }
        public string ByteProDB { get; set; }
    }
}
