﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProDB
{
    public class ByteDocumentDetails
    {
        public string BatchClass { get; set; }
        public Int32 EmbeddedDocID { get; set; }
        public Int32 Status { get; set; }
        public Int32 FileDataID { get; set; }
        public Guid GUID { get; set; }
        public string FileName { get; set; }
        public Int32 LoanStatus { get; set; }
        public DateTime DateCreated { get; set; }
        public string Description { get; set; }

    }
}
