﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.SearchLoan
{
    public class SearchRow
    {
        public List<SearchValue> SearchValues { get; set; }
    }
}
