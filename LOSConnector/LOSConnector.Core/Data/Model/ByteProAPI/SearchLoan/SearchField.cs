﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.SearchLoan
{
    public class SearchField
    {
        public string FieldName { get; set; }
        public string TableName { get; set; }
        public string FieldCaption { get; set; }
        public string FieldDescription { get; set; }
        public string FieldIdentifier { get; set; }
        public FieldFilter FieldFilter { get; set; }
    }
}
