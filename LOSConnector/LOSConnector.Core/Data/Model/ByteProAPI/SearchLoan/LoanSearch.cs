﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.SearchLoan
{
    public class LoanSearch
    {
        public bool SearchExportCSV { get; set; }
        public bool SearchDisplayHeaders { get; set; }
        public bool ShowDetailRecords { get; set; }
        public List<SearchField> SearchFields { get; set; }
        public int TotalRecords { get; set; }
        public List<SearchRow> SearchRows { get; set; }
        public object SearchData { get; set; }
    }
}
