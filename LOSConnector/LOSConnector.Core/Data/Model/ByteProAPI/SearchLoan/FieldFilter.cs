﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.SearchLoan
{
    public class FieldFilter
    {
        public List<string> FilterList { get; set; }
        public int FilterType { get; set; }
    }
}
