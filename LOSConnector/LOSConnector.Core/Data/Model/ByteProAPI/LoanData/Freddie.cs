﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Freddie
    {
        public int FreddieID { get; set; }
        public int AffordableProgramId { get; set; }
        public int OfferingId { get; set; }
        public string OtherOfferingIdentifier { get; set; }
        public int LoanDocumentationType { get; set; }
        public int BuildingStatusType { get; set; }
        public int OriginationProcessingPoint { get; set; }
        public string LoanKeyNumber { get; set; }
        public string EvaluationStatusType { get; set; }
        public object NumberOfSubmissions { get; set; }
        public object EvaluationDate { get; set; }
        public string CreditRiskType { get; set; }
        public string RepositorySource { get; set; }
        public string EvaluationType { get; set; }
        public object SubmissionDate { get; set; }
        public string PurchaseEligibility { get; set; }
        public object InitialLTV { get; set; }
        public string TransactionNumber { get; set; }
        public object InitialTLTV { get; set; }
        public string DocumentClassification { get; set; }
        public string RiskGrade { get; set; }
        public object LossCoverageEstimate { get; set; }
        public string MIDecisioin { get; set; }
        public object CreditScore { get; set; }
        public string CreditRiskComment { get; set; }
        public string RepositoryReason { get; set; }
        public int BuydownContributor { get; set; }
        public string SellerNumber { get; set; }
        public string TPONumber { get; set; }
        public string NOTP { get; set; }
        public int ConstructionPurpose { get; set; }
        public object FREReserves { get; set; }
        public string LoanIdentifier { get; set; }
        public string TransactionIdentifier { get; set; }
        public string LenderRegistrationNumber { get; set; }
        public string CreditReferenceNo { get; set; }
        public int NewConstruction { get; set; }
        public string CPMProjectID { get; set; }
        public int PropertyCategoryType { get; set; }
        public int OwnershipType { get; set; }
        public string OriginationProcessingPointOther { get; set; }
        public string PropertyCategoryTypeOther { get; set; }
        public string OwnershipTypeOther { get; set; }
        public string BuydownContributorOther { get; set; }
        public string BuildingStatusTypeOther { get; set; }
        public bool OrderMergedCredit { get; set; }
        public object FHABorrowerClosingCosts { get; set; }
        public object FHAFinancedDiscountPoints { get; set; }
        public object VAResidualIncome { get; set; }
        public string CreditAgencyCode { get; set; }
        public string CreditAffiliateCode { get; set; }
        public object FREMonthsInReserveOV { get; set; }
        public int AppraisalMethodType { get; set; }
        public int FileDataID { get; set; }
    }
}
