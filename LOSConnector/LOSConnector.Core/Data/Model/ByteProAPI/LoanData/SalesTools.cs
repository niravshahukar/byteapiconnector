﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class SalesTools
    {
        public int SalesToolsID { get; set; }
        public int ComparisonLoanPurp { get; set; }
        public object ComparisonID1 { get; set; }
        public object ComparisonID2 { get; set; }
        public object ComparisonID3 { get; set; }
        public object PrequalID1 { get; set; }
        public object PrequalID2 { get; set; }
        public object PrequalID3 { get; set; }
        public object IncomeOV { get; set; }
        public object DebtsOV { get; set; }
        public object DesiredPITI { get; set; }
        public string ComparisonComments { get; set; }
        public string OpenHousePropertyDesc { get; set; }
        public string OpenHouseComments { get; set; }
        public string OpenHousePicture { get; set; }
        public int OpenHousePartyCat { get; set; }
        public object PlannerPICurrent { get; set; }
        public object PlannerPIProposed { get; set; }
        public object PlannerClosingCosts { get; set; }
        public bool CalcFlood { get; set; }
        public bool ShowTotalSavings { get; set; }
        public object CashToBorrower { get; set; }
        public string DebtConsolidationComments { get; set; }
        public bool ShowPIOnly { get; set; }
        public bool ComparisonShowAPR { get; set; }
        public bool OpenHouseShowAPR { get; set; }
        public bool DebtConsolidationShowAPR { get; set; }
        public string OpenHousePictureAgent { get; set; }
        public string OpenHousePictureLO { get; set; }
        public object AntiSteeringID1 { get; set; }
        public object AntiSteeringID2 { get; set; }
        public object AntiSteeringID3 { get; set; }
        public object AntiSteeringID4 { get; set; }
        public string AntiSteeringChoiceExplanation { get; set; }
        public string AntiSteeringOption4Title { get; set; }
        public int AntiSteeringSelection { get; set; }
        public int FileDataID { get; set; }
    }
}
