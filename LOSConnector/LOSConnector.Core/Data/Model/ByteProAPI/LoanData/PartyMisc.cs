﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class PartyMisc
    {
        public int PartyMiscID { get; set; }
        public string HazPolicyNo { get; set; }
        public object HazEffectiveDate { get; set; }
        public object HazRenewalDate { get; set; }
        public bool HazInsTypeEarthquake { get; set; }
        public bool HazInsTypeFlood { get; set; }
        public bool HazInsTypeHazard { get; set; }
        public bool HazInsTypeWindStorm { get; set; }
        public int HazInsEscrowed { get; set; }
        public string EscrowAccountNo { get; set; }
        public bool WaiveEscrowAmended { get; set; }
        public int PropTaxesEscrowed { get; set; }
        public string FloodAccountNo { get; set; }
        public string MICertificateNo { get; set; }
        public object MIPerCov { get; set; }
        public string TitleAccountNo { get; set; }
        public string MortgageeClause { get; set; }
        public string LenderVesting { get; set; }
        public string LenderGovLaws { get; set; }
        public int FloodInsRequired { get; set; }
        public int FloodInsNFIPType { get; set; }
        public string FloodInsZone { get; set; }
        public object FloodInsApplicationDate { get; set; }
        public string FloodInsCommunity { get; set; }
        public string FloodInsInfoObtainedFrom { get; set; }
        public object FloodInsNFIPMapPanelDate { get; set; }
        public object FloodInsDeterminationDate { get; set; }
        public object ReferralFee { get; set; }
        public int TitleOrderSurvey { get; set; }
        public int TitleOrderPayoffs { get; set; }
        public bool ShowFirstAndSecondOnTitleOrder { get; set; }
        public object FloodCoverageAmount { get; set; }
        public string TitleUnderwriter { get; set; }
        public object _LoanOfficerLicenseExpDate { get; set; }
        public string CompanyLicenseNoOV { get; set; }
        public string CompanyNMLSIDOV { get; set; }
        public string BranchLicenseNoOV { get; set; }
        public string BranchNMLSIDOV { get; set; }
        public object _CompanyLicenseNo { get; set; }
        public object _CompanyNMLSID { get; set; }
        public object _CompanyLicenseExpirationDate { get; set; }
        public object _BranchLicenseNo { get; set; }
        public object _BranchNMLSID { get; set; }
        public object _BranchLicenseExpirationDate { get; set; }
        public string SupervisoryAppraiserLicenseNumber { get; set; }
        public string CompanyEINOV { get; set; }
        public object _CompanyEIN { get; set; }
        public string InvestorCode { get; set; }
        public int MICompanyNameType { get; set; }
        public string Haz2PolicyNo { get; set; }
        public object Haz2EffectiveDate { get; set; }
        public object Haz2RenewalDate { get; set; }
        public int Haz2InsType { get; set; }
        public string FloodInsCounty { get; set; }
        public string FloodInsCommunityNo { get; set; }
        public string NFIPMapIdentifier { get; set; }
        public object NFIPLetterOfMapDate { get; set; }
        public int NFIPMapIndicator { get; set; }
        public bool FloodInsIsInProtectedArea { get; set; }
        public object FloodInsProtectedAreaDesigDate { get; set; }
        public int FloodInsIsLifeOfLoan { get; set; }
        public string FloodCertificationIdentifier { get; set; }
        public object NFIPCommunityParticipationStartDate { get; set; }
        public int NFIPFloodDataRevisionType { get; set; }
        public object TitleReportDate { get; set; }
        public string TitleReportItems { get; set; }
        public string TitleReportEndorsements { get; set; }
        public bool BuilderOrSellerIsNonPersonEntity { get; set; }
        public string CreditAltLenderCaseNo { get; set; }
        public int ClosingAgentType { get; set; }
        public string ServicerCode { get; set; }
        public string WireSpecialInstructions { get; set; }
        public object TrusteeFeePercent { get; set; }
        public int Seller1CDSignatureMethod { get; set; }
        public string Seller1CDSignatureMethodOtherDesc { get; set; }
        public object _LoanOfficerLicenseStartDate { get; set; }
        public object _CompanyLicenseStartDate { get; set; }
        public object _BranchLicenseStartDate { get; set; }
        public int MIUnderwritingType { get; set; }
        public string MISpecialProgramCode { get; set; }
        public object FloodEffectiveDate { get; set; }
        public object FloodRenewalDate { get; set; }
        public object EducationBorrowerID { get; set; }
        public object CounselingBorrowerID { get; set; }
        public int FileDataID { get; set; }
    }
}
