﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class ClosingCost
    {
        public int CCID { get; set; }
        public int LoanID { get; set; }
        public Enum.HUDCCLineNo HUDLineNo { get; set; }
        public bool PPFC { get; set; }
        public object Points { get; set; }
        public string Name { get; set; }
        public object BorrowerAmount { get; set; }
        public object SellerAmount { get; set; }
        public bool POC { get; set; }
        public bool IsPaidToBroker { get; set; }
        public object PaidToBrokerSplit { get; set; }
        public bool NotCounted { get; set; }
        public string PaidToName { get; set; }
        public Enum.ClosingCostType ClosingCostType { get; set; }
        public object _TotalAmount { get; set; }
        public object _PaidToBroker { get; set; }
        public object _PaidToOthers { get; set; }
        public Enum.PaidByOtherType PaidByOtherType { get; set; }
        public object GFEDisclosedAmount { get; set; }
        public bool ProviderChosenByBorrower { get; set; }
        public Enum.HUDCCLineNo2010 HUDLineNo2010 { get; set; }
        public Enum.GFEBlock GFEBlock { get; set; }
        public Enum.ResponsiblePartyType ResponsiblePartyType { get; set; }
        public Enum.PaidToType PaidToType { get; set; }
        public bool NetFromWire { get; set; }
        public object _GLCode { get; set; }
        public bool Financed { get; set; }
        public object PointsAndFeesAmountOV { get; set; }
        public Enum.TRIDBlock TRIDBlock { get; set; }
        public Enum.YesNoNA IsOptionalOV { get; set; }
        public object GFEBaselineAmount { get; set; }
        public object BorrowerPOCAmountOV { get; set; }
        public object SellerPOCAmountOV { get; set; }
        public int FileDataID { get; set; }
    }
}
