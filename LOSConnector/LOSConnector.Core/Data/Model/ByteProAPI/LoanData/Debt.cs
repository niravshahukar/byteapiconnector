﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Debt
    {
        public int AppNo { get; set; }
        public int DebtID { get; set; }
        public int BorrowerID { get; set; }
        public int DisplayOrder { get; set; }
        public int? REOID { get; set; }
        public Enum.DebtType DebtType { get; set; }
        public string Name { get; set; }
        public string Attn { get; set; }
        public string FullAddress { get; set; }
        public string CityStateZip { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string AccountNo { get; set; }
        public double MoPayment { get; set; }
        public int PaymentsLeft { get; set; }
        public string PaymentsLeftTextOV { get; set; }
        public double UnpaidBal { get; set; }
        public bool NotCounted { get; set; }
        public bool ToBePaidOff { get; set; }
        public int LienPosition { get; set; }
        public bool Resubordinated { get; set; }
        public bool Omitted { get; set; }
        public string Notes { get; set; }
        public bool IsLienOnSubProp { get; set; }
        public double TotalPaymentsOV { get; set; }
        public string Fax { get; set; }
        public Enum.YesNoNA SSOrDILAccepted { get; set; }
        public bool ListedOnCreditReport { get; set; }
        public string QMATRNotes { get; set; }
        public string OtherDesc { get; set; }
        public Enum.DebtMortgageType MortgageType { get; set; }
        public double HELOCCreditLimit { get; set; }
        public int AccountHeldByType { get; set; }
        public int FileDataID { get; set; }
    }
}
