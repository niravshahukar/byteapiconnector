﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class VAValue
    {
        public int VAValueID { get; set; }
        public string TitleLimits { get; set; }
        public string FirmName { get; set; }
        public string FirmStreet { get; set; }
        public string FirmCityStateZip { get; set; }
        public string FirmComments { get; set; }
        public string LotDimensions { get; set; }
        public object LotSqFootage { get; set; }
        public object LotAcreage { get; set; }
        public bool IsIrregular { get; set; }
        public bool IsAcres { get; set; }
        public int ElectricUtil { get; set; }
        public int GasUtil { get; set; }
        public int WaterUtil { get; set; }
        public int SewerUtil { get; set; }
        public bool HasRange { get; set; }
        public bool HasRefrigerator { get; set; }
        public bool HasDishwasher { get; set; }
        public bool HasWasher { get; set; }
        public bool HasDryer { get; set; }
        public bool HasDisposal { get; set; }
        public bool HasVentFan { get; set; }
        public bool HasCarpet { get; set; }
        public bool HasOtherEquip { get; set; }
        public string OtherEquipDesc { get; set; }
        public int BuildingStatus { get; set; }
        public int BuildingType { get; set; }
        public int FactoryFab { get; set; }
        public object NoBuildings { get; set; }
        public object NoLivingUnits { get; set; }
        public int StreetAccess { get; set; }
        public int StreetMaint { get; set; }
        public int Warranty { get; set; }
        public string WarrantyProgram { get; set; }
        public string WarrantyExpires { get; set; }
        public string ConstCompleted { get; set; }
        public string Owner { get; set; }
        public int PropertyOccupancy { get; set; }
        public object Rent { get; set; }
        public string OccupantName { get; set; }
        public string OccupantPhone { get; set; }
        public string BrokerName { get; set; }
        public string BrokerPhone { get; set; }
        public string InspectionDate { get; set; }
        public bool CanInspectAM { get; set; }
        public bool CanInspectPM { get; set; }
        public string KeysAt { get; set; }
        public string InstNo { get; set; }
        public int InspectionBy { get; set; }
        public int Plans { get; set; }
        public string PlansCaseNo { get; set; }
        public string Comments { get; set; }
        public object Taxes { get; set; }
        public int MineralRightsReserved { get; set; }
        public string MineralRightsExpl { get; set; }
        public int LeaseType { get; set; }
        public string LeaseExpires { get; set; }
        public object GroundRent { get; set; }
        public int PurLotSep { get; set; }
        public int ContractAttached { get; set; }
        public string ContractNo { get; set; }
        public int UseLoanValue { get; set; }
        public int Signature { get; set; }
        public string RequestorTitle { get; set; }
        public string RequestorPhone { get; set; }
        public string AssignmentDate { get; set; }
        public bool OmitBuilder { get; set; }
        public string FirmEmail { get; set; }
        public string PointOfContactInfo { get; set; }
        public string BuilderVAID { get; set; }
        public int FileDataID { get; set; }
    }
}
