﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Fannie
    {
        public int FannieID { get; set; }
        public string CaseFileID { get; set; }
        public int CommunityLendingProduct { get; set; }
        public int CommSeconds { get; set; }
        public int NeighborsElig { get; set; }
        public string MSA { get; set; }
        public string Recommendation { get; set; }
        public string InstitutionID { get; set; }
        public int CommunitySecondsRepaymentSchedule { get; set; }
        public object UnderwritingRunDate { get; set; }
        public string SellerNumber { get; set; }
        public int CreditAgencyCode { get; set; }
        public bool RefiOfConstructionLoan { get; set; }
        public int HomebuyerEducationType { get; set; }
        public int ProductDescription { get; set; }
        public object EnergyImpAmount { get; set; }
        public object PACELoanPayoffAmount { get; set; }
        public string ProductDescriptionOther { get; set; }
        public bool IsSellerProvidingBelowMarketSubFi { get; set; }
        public int FileDataID { get; set; }
    }
}
