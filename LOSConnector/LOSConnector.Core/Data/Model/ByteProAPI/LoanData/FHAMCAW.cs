﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class FHAMCAW
    {
        public int FHAMCAWID { get; set; }
        public int ConstructionType { get; set; }
        public object StatutoryInvReqOV { get; set; }
        public object RequiredAdjustmentsOV { get; set; }
        public object SalesConcessionsOV { get; set; }
        public object MortgageBasisOV { get; set; }
        public object Refi10F1OV { get; set; }
        public object Refi10F2OV { get; set; }
        public object RefiRequiredInvestmentOV { get; set; }
        public object MinDownPaymentOV { get; set; }
        public object DiscountNotFinancedOV { get; set; }
        public object PrepaidsNotFinancedOV { get; set; }
        public object NonFinanceableRepairsOV { get; set; }
        public object NonRealtyOV { get; set; }
        public object TotalCashToCloseOV { get; set; }
        public object OtherCreditsOV { get; set; }
        public bool OtherCreditsCashOpt { get; set; }
        public bool OtherCreditsOtherOpt { get; set; }
        public object CashToCloseOV { get; set; }
        public bool AmountToBePaidCashOpt { get; set; }
        public bool AmountToBePaidOtherOpt { get; set; }
        public object AvailableAssetsOV { get; set; }
        public string SecondMortgageSource { get; set; }
        public object SecondMortgageOV { get; set; }
        public object BasePayBorOV { get; set; }
        public object OtherEarningsBorOV { get; set; }
        public object BasePayCoBorOV { get; set; }
        public object OtherEarningsCoBorOV { get; set; }
        public object NetRealEstateIncomeOV { get; set; }
        public object GrossMonthlyIncomeOV { get; set; }
        public object LTVOV { get; set; }
        public object FirstRatioOV { get; set; }
        public object SecondRatioOV { get; set; }
        public int CreditCharacteristics { get; set; }
        public int AdequacyOfIncome { get; set; }
        public int StabilityOfIncome { get; set; }
        public int AdequacyOfAssets { get; set; }
        public int TotalCCOption { get; set; }
        public object TotalCCOtherAmount { get; set; }
        public bool PurchasedWithinOneYear { get; set; }
        public object WeatherizationCosts { get; set; }
        public bool IncCCInExistingDebt { get; set; }
        public bool IncDPInExistingDebt { get; set; }
        public bool IncPPInExistingDebt { get; set; }
        public object OtherExistingDebtAmount { get; set; }
        public string Remarks { get; set; }
        public bool OrigCaseNoAssignedOnOrAfterJuly142008 { get; set; }
        public object NetRealEstateIncomeCoBorsOV { get; set; }
        public int AmortTypeOV { get; set; }
        public string SecondMortgageOtherDesc { get; set; }
        public int SecondMortgageSourceType { get; set; }
        public int Gift1SourceType { get; set; }
        public string Gift1OtherDesc { get; set; }
        public string Gift2Source { get; set; }
        public int Gift2SourceType { get; set; }
        public string Gift2OtherDesc { get; set; }
        public object Gift2Amount { get; set; }
        public int SellerFundedDAP { get; set; }
        public object CLTVOV { get; set; }
        public object SellerContributionOV { get; set; }
        public object MonthsInReserveFunds { get; set; }
        public int ScoredByTOTAL { get; set; }
        public int RiskClass { get; set; }
        public string AppraisalReviewerCHUMSNo { get; set; }
        public int PropertyTypeOV { get; set; }
        public string SourceOfFunds { get; set; }
        public bool EnergyEfficientMortgage { get; set; }
        public bool BuildingOnOwnLand { get; set; }
        public bool LoanPurposeOther { get; set; }
        public string LoanPurposeOtherDesc { get; set; }
        public int IntRateBuydownOV { get; set; }
        public int CaseNoAssignmentPeriod { get; set; }
        public object ClosingCostsAndPrepaidsPBAOV { get; set; }
        public int FileDataID { get; set; }
    }
}
