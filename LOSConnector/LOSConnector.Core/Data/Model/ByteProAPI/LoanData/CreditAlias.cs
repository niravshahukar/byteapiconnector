﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class CreditAlias
    {
        public int FileDataID { get; set; }
        public int AppNo { get; set; }
        public int? BorrowerID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string CreditorName { get; set; }
        public string AccountNo { get; set; }
        public Enum.CreditAliasType CreditAliasType { get; set; }
    }
}
