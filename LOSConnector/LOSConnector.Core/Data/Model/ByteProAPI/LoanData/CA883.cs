﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
   public class CA883
    {
        public int CA883ID { get; set; }
        public object BrokerFee { get; set; }
        public int HasAdditionalComp { get; set; }
        public object AdditionalComp { get; set; }
        public object CreditDisabilityInsAmount { get; set; }
        public int PrepayPenaltyOption { get; set; }
        public int PrepayPenaltyPrincipalOption { get; set; }
        public object PrepayPenaltyMonths { get; set; }
        public int BrokerFundedOption { get; set; }
        public bool OmitLandAndAlts { get; set; }
        public bool OmitMIPFF { get; set; }
        public bool OmitSubFi { get; set; }
        public bool OmitPaidBySeller { get; set; }
        public bool OmitOtherCredits { get; set; }
        public object OtherDeduction { get; set; }
        public string OtherDeductionText { get; set; }
        public string PrepayPenaltyOtherDescription { get; set; }
        public bool HasLimitedDocumentation { get; set; }
        public object PrepayPenaltyTerm { get; set; }
        public object PrepayPenaltyMaxAmount { get; set; }
        public int FileDataID { get; set; }
    }
}
