﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class FHA
    {
        public int FHAID { get; set; }
        public object MIPRefund { get; set; }
        public object EnergyImp { get; set; }
        public string SectionOfActOV { get; set; }
        public object ReasonableValue { get; set; }
        public int FHALoanPurpose { get; set; }
        public string CreditDataAgent { get; set; }
        public int ReasonableValueType { get; set; }
        public int ReceivedLeadPaintNotice { get; set; }
        public int ApprovedOrModified { get; set; }
        public object ModifiedLoanWith { get; set; }
        public object ModifiedIntRate { get; set; }
        public object ModifiedTerm { get; set; }
        public object ModifiedPayment { get; set; }
        public object ModifiedUFMIP { get; set; }
        public object ModifiedMIPayment { get; set; }
        public object ModifiedMITerm { get; set; }
        public bool ProposedConstInCompliance { get; set; }
        public bool NewConstComplete { get; set; }
        public bool BuildersWarrantyReq { get; set; }
        public bool PropertyHas10YearWarranty { get; set; }
        public bool CodeInspectionReq { get; set; }
        public bool OONotRequired { get; set; }
        public bool HighLTVForNonOOInMilitary { get; set; }
        public bool OtherCondition { get; set; }
        public string OtherConditionDesc { get; set; }
        public int ScorecardRating { get; set; }
        public string MortgageeRep { get; set; }
        public string DEUnderwriter { get; set; }
        public string DUCHUMSID { get; set; }
        public int MortgageeHasFinancialRel { get; set; }
        public string PropertyJuris { get; set; }
        public int HasWaterSupplyOrSewageSys { get; set; }
        public string Section221D2CodeLetter { get; set; }
        public object MCC { get; set; }
        public object MaxLoanLTVFactorOV { get; set; }
        public object MIDurationMonthsOV { get; set; }
        public bool IsSponsoredOrigination { get; set; }
        public bool MaxLoanCanExceedCountyLimitML2011_29 { get; set; }
        public object OrigFHAEndorsementDate { get; set; }
        public bool AllConditionsSatisfied { get; set; }
        public int URLAAddendumRoleOfOfficerOV { get; set; }
        public int FileDataID { get; set; }
    }
}
