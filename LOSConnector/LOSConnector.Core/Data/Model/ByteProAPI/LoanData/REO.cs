﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class REO
    {
        public int FileDataID { get; set; }
        public int AppNo { get; set; }
        public int? BorrowerID { get; set; }
        public int DisplayOrder { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public Enum.REOStatus REOStatus { get; set; }
        public Enum.REOType REOType { get; set; }
        public Decimal? MarketValue { get; set; }
        public Decimal? GrossRentalIncome { get; set; }
        public Decimal? Taxes { get; set; }
        public Decimal? NetRentalIncomeOV { get; set; }
        public Decimal? VacancyFactorOV { get; set; }
        public bool IsSubjectProperty { get; set; }
        public bool IsCurrentResidence { get; set; }
        public Decimal? PITIOV { get; set; }
        public bool TIIncludedInMortgage { get; set; }
        public bool VAPurchasedOrRefinancedWithVALoan { get; set; }
        public DateTime? VALoanDate { get; set; }
        public Enum.VAEntitlementRestoration VAEntitlementRestoration { get; set; }
        public bool MortgagesDNADesired { get; set; }
        public int StreetContainsUnitNumberOV { get; set; }
        public int AccountHeldByType { get; set; }
        public int? NoUnits { get; set; }
        public int CurrentUsageType { get; set; }
    }
}
