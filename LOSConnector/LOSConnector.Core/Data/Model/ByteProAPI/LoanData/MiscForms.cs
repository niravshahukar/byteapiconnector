﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class MiscForms
    {
        public int MiscFormsID { get; set; }
        public int MBCBrokerRepresents { get; set; }
        public int MBCShopOption { get; set; }
        public object MBCNumberOfLenders { get; set; }
        public string MBCCompAmount { get; set; }
        public string MBCCompNoGreaterThan { get; set; }
        public string MBCPoints { get; set; }
        public string MBCBorFee { get; set; }
        public string MBCLenderFee { get; set; }
        public object FLBANetProceedsOV { get; set; }
        public object FLBABrokerageFeeOV { get; set; }
        public object FLBAAppDepositOV { get; set; }
        public object FLBAThirdPartyFee { get; set; }
        public bool FLBAIsAppDepositRefundable { get; set; }
        public string MANoteExpDate { get; set; }
        public object MAAppFee { get; set; }
        public string FLECBrokerName { get; set; }
        public string NYAdvisedDesc1OV { get; set; }
        public string NYAdvisedDesc2OV { get; set; }
        public string NYProcessingAssistantTextOV { get; set; }
        public bool NYLenderFees { get; set; }
        public string NYLenderFeesPercent { get; set; }
        public string NYLenderFeesDollar { get; set; }
        public string NYLenderFeesPoints { get; set; }
        public bool NYFeeUnkown { get; set; }
        public string NYFeeUnknownPoints { get; set; }
        public bool NYFeesFromLoan { get; set; }
        public string NYFeesFromLoanPercent { get; set; }
        public string NYFeesFromLoanDollar { get; set; }
        public bool NYFeesDirect { get; set; }
        public string NYFeesDirectCommitment { get; set; }
        public string NYFeesDirectClosing { get; set; }
        public string NYFeesDirectPercent { get; set; }
        public string NYFeesDirectDollar { get; set; }
        public object NYApplicationFeeOV { get; set; }
        public object NYAppraisalFeeOV { get; set; }
        public object NYCreditFeeOV { get; set; }
        public string NYApplicationFeeRefundableOV { get; set; }
        public string NYRefundDesc1OV { get; set; }
        public string NYRefundDesc2OV { get; set; }
        public object NYProcFeeOV { get; set; }
        public string NYFeeDivisionName1 { get; set; }
        public string NYFeeDivisionName2 { get; set; }
        public string NYFeeDivisionFee1 { get; set; }
        public string NYFeeDivisionFee2 { get; set; }
        public string NYFeeDivisionFee3 { get; set; }
        public string NYFeeDivisionFee4 { get; set; }
        public string NYContactName { get; set; }
        public string NYContactPhone { get; set; }
        public string NYContactPhoneCollect { get; set; }
        public string NYContactPhoneTollFree { get; set; }
        public string NYDispositionStatusOV { get; set; }
        public object NYDispositionDateOV { get; set; }
        public object NYBrokerFeeAmountLender { get; set; }
        public object NYBrokerFeeAmountBorrower { get; set; }
        public object NYBrokerFeeAmountOther1 { get; set; }
        public object NYBrokerFeeAmountOther2 { get; set; }
        public string NYBrokerFeeDescOther1 { get; set; }
        public string NYBrokerFeeDescOther2 { get; set; }
        public string SSA89AgentOV { get; set; }
        public string FLInfoDisclosureBranchPhoneOV { get; set; }
        public int SSA89CompanyOption { get; set; }
        public int FileDataID { get; set; }
    }
}
