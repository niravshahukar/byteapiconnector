﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Income
    {
        public int AppNo { get; set; }
        public int IncomeID { get; set; }
        public int BorrowerID { get; set; }
        public int DisplayOrder { get; set; }
        public Enum.IncomeType IncomeType { get; set; }
        public double Amount { get; set; }
        public string DescriptionOV { get; set; }
        public Enum.IncomeFrequencyType IncomeFrequencyType { get; set; }
        public double IncomeRate { get; set; }
        public double HoursPerWeek { get; set; }
        public string Notes { get; set; }
        public string QMATRNotes { get; set; }
        public string VariablePeriod1Desc { get; set; }
        public Decimal? VariablePeriod1Income { get; set; }
        public Decimal? VariablePeriod1Months { get; set; }
        public string VariablePeriod2Desc { get; set; }
        public Decimal? VariablePeriod2Income { get; set; }
        public Decimal? VariablePeriod2Months { get; set; }
        public string VariablePeriod3Desc { get; set; }
        public Decimal? VariablePeriod3Income { get; set; }
        public Decimal? VariablePeriod3Months { get; set; }
        public string _CalcDescription { get; set; }
        public string _RateDescription { get; set; }
        public bool SelfEmploymentIncome { get; set; }
        public int EmployerID { get; set; }
        public int FileDataID { get; set; }
    }
}
