﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Expense
    {
        public int FileDataID { get; set; }
        public int AppNo { get; set; }
        public int? BorrowerID { get; set; }
        public Enum.ExpenseType ExpenseType { get; set; }
        public string OtherDesc { get; set; }
        public Decimal? MoPayment { get; set; }
        public int? PaymentsLeft { get; set; }
        public string QMATRNotes { get; set; }
        public string Notes { get; set; }
        public int AccountHeldByType { get; set; }
    }
}
