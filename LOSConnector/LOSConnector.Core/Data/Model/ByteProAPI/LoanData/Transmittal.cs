﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
   public class Transmittal
    {
        public int TransmittalID { get; set; }
        public int IsPropType1UnitOV { get; set; }
        public int IsPropType2To4UnitsOV { get; set; }
        public int IsPropTypeCondoOV { get; set; }
        public int IsPropTypePUDOV { get; set; }
        public int IsPropTypeCoopOV { get; set; }
        public int IsPropTypeManuOV { get; set; }
        public int IsPropTypeSinglewideOV { get; set; }
        public int IsPropTypeMultiwideOV { get; set; }
        public int AmortTypeOV { get; set; }
        public string AmortTypeDescOV { get; set; }
        public int LoanPurposeOV { get; set; }
        public int OriginatorType { get; set; }
        public string BrokerName { get; set; }
        public object GrossIncomeBorOV { get; set; }
        public object GrossIncomeCoBorsOV { get; set; }
        public object GrossIncomeTotalOV { get; set; }
        public object OtherIncomeBorOV { get; set; }
        public object OtherIncomeCoBorsOV { get; set; }
        public object OtherIncomeTotalOV { get; set; }
        public object CashFlowIncomeBorOV { get; set; }
        public object CashFlowIncomeCoBorsOV { get; set; }
        public object CashFlowIncomeTotalOV { get; set; }
        public object TotalIncomeBorOV { get; set; }
        public object TotalIncomeCoBorsOV { get; set; }
        public object TotalIncomeTotalOV { get; set; }
        public object FirstRatioOV { get; set; }
        public object SecondRatioOV { get; set; }
        public object GapRatioOV { get; set; }
        public object LTVOV { get; set; }
        public object CLTVOV { get; set; }
        public object HCLTVOV { get; set; }
        public int AppraisalType { get; set; }
        public string AppraisalFormNo { get; set; }
        public int RiskAssessmentMethod { get; set; }
        public string RiskAssessmentMethodOther { get; set; }
        public string AUSRecommendation { get; set; }
        public string AUSFileID { get; set; }
        public string DocumentClassification { get; set; }
        public object RepCreditScore { get; set; }
        public object PrimaryResFirstMortPIOV { get; set; }
        public object PrimaryResSecondMortPIOV { get; set; }
        public object PrimaryResHazOV { get; set; }
        public object PrimaryResPropTaxesOV { get; set; }
        public object PrimaryResMIOV { get; set; }
        public object PrimaryResHODOV { get; set; }
        public object PrimaryResLeaseOV { get; set; }
        public object PrimaryResOtherHousingExpOV { get; set; }
        public object PrimaryResPITIOV { get; set; }
        public object SubPropNetCashFlowNegOV { get; set; }
        public object AllOtherPaymentsOV { get; set; }
        public object TotalAllPaymentsOV { get; set; }
        public object FundsReqToCloseOV { get; set; }
        public object VerifiedAssetsOV { get; set; }
        public object MonthsInReserveOV { get; set; }
        public object SalesConcessionsPercOV { get; set; }
        public string UWCom { get; set; }
        public int QualRateOption { get; set; }
        public string SellerNo { get; set; }
        public string InvestorLoanNo { get; set; }
        public string SellerLoanNo { get; set; }
        public string MasterCommitmentNo { get; set; }
        public string ContractNo { get; set; }
        public object ContractSignatureDate { get; set; }
        public object RequiredMonthsInResOV { get; set; }
        public string SpecialFeatureCode01 { get; set; }
        public string SpecialFeatureCode02 { get; set; }
        public string SpecialFeatureCode03 { get; set; }
        public string SpecialFeatureCode04 { get; set; }
        public string SpecialFeatureCode05 { get; set; }
        public string SpecialFeatureCode06 { get; set; }
        public string SpecialFeatureCode07 { get; set; }
        public string SpecialFeatureCode08 { get; set; }
        public string SpecialFeatureCode09 { get; set; }
        public string SpecialFeatureCode10 { get; set; }
        public object _MonthsInReserve { get; set; }
        public bool UseCommentsAddendum { get; set; }
        public object InterestedPartyContributions { get; set; }
        public object IncomeNetRentalOV { get; set; }
        public object SubPropFirstMortPIOV { get; set; }
        public object SubPropOtherFinancingPIOV { get; set; }
        public object SubPropOtherPaymentOV { get; set; }
        public int FileDataID { get; set; }
    }
}
