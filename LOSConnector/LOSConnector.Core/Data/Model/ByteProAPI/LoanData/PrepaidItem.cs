﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class PrepaidItem
    {
        public int PrepaidItemID { get; set; }
        public int LoanID { get; set; }
        public int PrepaidItemType { get; set; }
        public string NameOV { get; set; }
        public object Payment { get; set; }
        public object MonthsInReserve { get; set; }
        public object MonthsInAdvance { get; set; }
        public bool PremiumPOC { get; set; }
        public bool ReservesPOC { get; set; }
        public bool IncludeInPITI { get; set; }
        public object ReservesPBSDesired { get; set; }
        public object PremiumPBSDesired { get; set; }
        public int DisbursementSched { get; set; }
        public int DisbursementStartPur { get; set; }
        public int DisbursementStartRefi { get; set; }
        public string DisbursementPeriods { get; set; }
        public object CushionOV { get; set; }
        public object PrequalPaymentOV { get; set; }
        public object ReservesOV { get; set; }
        public object PremiumOV { get; set; }
        public object _ReservesPBA { get; set; }
        public object _ReservesPBS { get; set; }
        public object _Premium { get; set; }
        public object _PremiumPBA { get; set; }
        public object _PremiumPBS { get; set; }
        public int _Cushion { get; set; }
        public object _CushionDollar { get; set; }
        public int ReservesPaidByOtherType { get; set; }
        public int PremiumPaidByOtherType { get; set; }
        public object DisbursementStartYear { get; set; }
        public int MISMOPrepaidItemTypeOV { get; set; }
        public object PremiumGFEDisclosedAmount { get; set; }
        public bool PremiumNetFromWire { get; set; }
        public string QMATRNotes { get; set; }
        public object PremiumPointsAndFeesAmountOV { get; set; }
        public object ReservesPointsAndFeesAmountOV { get; set; }
        public int PremiumPaidToType { get; set; }
        public object PeriodicPaymentAmount { get; set; }
        public object _YearlyPayment { get; set; }
        public string PaidToNameOV { get; set; }
        public object ReservesGFEDisclosedAmount { get; set; }
        public string DisbursementList { get; set; }
        public int FileDataID { get; set; }
    }
}
