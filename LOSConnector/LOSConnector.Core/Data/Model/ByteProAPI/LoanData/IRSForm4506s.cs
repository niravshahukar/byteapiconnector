﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
   public class IRSForm4506s
    {
        public int IRSForm4506ID { get; set; }
        public int BorrowerID { get; set; }
        public int FilingStatus { get; set; }
        public string LastReturnFullAddress { get; set; }
        public string LastReturnCityStateZip { get; set; }
        public string LastReturnStreet { get; set; }
        public string LastReturnCity { get; set; }
        public string LastReturnState { get; set; }
        public string LastReturnZip { get; set; }
        public string ThirdPartyName { get; set; }
        public string ThirdPartyFullAddress { get; set; }
        public string ThirdPartyCityStateZip { get; set; }
        public string ThirdPartyStreet { get; set; }
        public string ThirdPartyCity { get; set; }
        public string ThirdPartyState { get; set; }
        public string ThirdPartyZip { get; set; }
        public string ThirdPartyPhone { get; set; }
        public string TaxFormRequested { get; set; }
        public string TaxPeriod1 { get; set; }
        public string TaxPeriod2 { get; set; }
        public string TaxPeriod3 { get; set; }
        public string TaxPeriod4 { get; set; }
        public bool ReturnTranscript { get; set; }
        public bool AccountTranscript { get; set; }
        public bool RecordOfAccount { get; set; }
        public bool VerificationOfNonFiling { get; set; }
        public bool FormW2EtcTranscript { get; set; }
        public string PhoneOV { get; set; }
        public bool OverrideReturnName { get; set; }
        public bool OverrideSpouse { get; set; }
        public bool OverrideCurrentName { get; set; }
        public bool OverrideCurrentAddress { get; set; }
        public string ReturnFirstNameOV { get; set; }
        public string ReturnMiddleNameOV { get; set; }
        public string ReturnLastNameOV { get; set; }
        public string ReturnSuffixOV { get; set; }
        public string ReturnSSNOV { get; set; }
        public string SpouseFirstNameOV { get; set; }
        public string SpouseMiddleNameOV { get; set; }
        public string SpouseLastNameOV { get; set; }
        public string SpouseSuffixOV { get; set; }
        public string SpouseSSNOV { get; set; }
        public string CurrentFirstNameOV { get; set; }
        public string CurrentMiddleNameOV { get; set; }
        public string CurrentLastNameOV { get; set; }
        public string CurrentSuffixOV { get; set; }
        public string CurrentStreetOV { get; set; }
        public string CurrentCityOV { get; set; }
        public string CurrentStateOV { get; set; }
        public string CurrentZipOV { get; set; }
        public int TRVForm1 { get; set; }
        public int TRVForm2 { get; set; }
        public int TRVForm3 { get; set; }
        public object TRVForm1Year1 { get; set; }
        public object TRVForm1Year2 { get; set; }
        public object TRVForm1Year3 { get; set; }
        public object TRVForm1Year4 { get; set; }
        public object TRVForm2Year1 { get; set; }
        public object TRVForm2Year2 { get; set; }
        public object TRVForm2Year3 { get; set; }
        public object TRVForm2Year4 { get; set; }
        public object TRVForm3Year1 { get; set; }
        public object TRVForm3Year2 { get; set; }
        public object TRVForm3Year3 { get; set; }
        public object TRVForm3Year4 { get; set; }
        public string DVOrderId1 { get; set; }
        public string DVOrderId2 { get; set; }
        public string DVOrderId3 { get; set; }
        public bool IdentityTheft { get; set; }
        public string IRSForm4506Guid { get; set; }
        public int FileDataID { get; set; }
    }
}
