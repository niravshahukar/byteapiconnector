﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class FHAForms
    {
        public int FHAFormsID { get; set; }
        public int FairAnalysis { get; set; }
        public string FairAnalysisComments { get; set; }
        public string QualityComments { get; set; }
        public int ComparablesAcceptable { get; set; }
        public string ComparablesComments { get; set; }
        public int AdjustmentsAcceptable { get; set; }
        public string AdjustmentsComments { get; set; }
        public int ValueAcceptable { get; set; }
        public int ValueShouldBeCorrected { get; set; }
        public object ValueForFHAPurposes { get; set; }
        public string ValueCorrectionComments { get; set; }
        public string RepairConditions { get; set; }
        public string OtherAppraisalComments { get; set; }
        public object ActionDate { get; set; }
        public string CommitmentIssuedBy { get; set; }
        public object CommitmentIssued { get; set; }
        public object CommitmentExpires { get; set; }
        public int IsEligibleForMaxFi { get; set; }
        public bool HasAssuranceOfComp { get; set; }
        public object AssuranceCompAmount { get; set; }
        public bool HasAdditionaltemsAttached { get; set; }
        public string AdditionalConditions { get; set; }
        public bool ConditionsOnBackApply { get; set; }
        public int FileDataID { get; set; }
    }
}
