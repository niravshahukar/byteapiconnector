﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
   public class CreditDenial
    {
        public int CreditDenialID { get; set; }
        public bool CreditNoCreditFile { get; set; }
        public bool CreditNumberOfReferences { get; set; }
        public bool CreditInsufficientFiles { get; set; }
        public bool CreditLimitedExperience { get; set; }
        public bool CreditUnableToVerifyReferences { get; set; }
        public bool CreditGarnishment { get; set; }
        public bool CreditJudgment { get; set; }
        public bool CreditExcessiveObligation { get; set; }
        public bool CreditPaymentRecordPreviousMtg { get; set; }
        public bool CreditLackOfCashReserves { get; set; }
        public bool CreditDelinquentObligationOthers { get; set; }
        public bool CreditBankruptcy { get; set; }
        public bool CreditTypeOfReference { get; set; }
        public bool CreditPoorPerformanceUs { get; set; }
        public bool EmpUnableToVerify { get; set; }
        public bool EmpLength { get; set; }
        public bool EmpTempOrIrregular { get; set; }
        public bool IncInsufficientForAmount { get; set; }
        public bool IncUnableToVerify { get; set; }
        public bool IncExcessiveObligations { get; set; }
        public bool ResTemporary { get; set; }
        public bool ResLength { get; set; }
        public bool ResUnableToVerify { get; set; }
        public bool DeniedHUD { get; set; }
        public bool DeniedVa { get; set; }
        public bool DeniedFannie { get; set; }
        public bool DeniedFreddie { get; set; }
        public bool DeniedOther { get; set; }
        public string DeniedOtherDesc { get; set; }
        public bool OtherInsufficientFundsToClose { get; set; }
        public bool OtherCreditApplicationIncomplete { get; set; }
        public bool OtherValueOrTypeOfCollateral { get; set; }
        public bool OtherUnacceptableProperty { get; set; }
        public bool OtherInsufficientDataProperty { get; set; }
        public bool OtherUnacceptableAppraisal { get; set; }
        public bool OtherUnacceptalbeLeasehold { get; set; }
        public bool OtherTermsAndConditions { get; set; }
        public bool OtherSpecify { get; set; }
        public string OtherDescription { get; set; }
        public string ActionTakenDescription { get; set; }
        public string AccountDescription { get; set; }
        public string CompanyName { get; set; }
        public string CompanyFullAddress { get; set; }
        public string CompanyCityStateZip { get; set; }
        public string CompanyStreet { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyZip { get; set; }
        public string CompanyPhone { get; set; }
        public bool CreditDecision1 { get; set; }
        public bool CreditDecision2 { get; set; }
        public int NoticeDeliveryMethod { get; set; }
        public string PreparedBy { get; set; }
        public bool Equifax { get; set; }
        public bool Experian { get; set; }
        public bool TransUnion { get; set; }
        public bool CreditNumberOfInquiries { get; set; }
        public int FileDataID { get; set; }
    }
}
