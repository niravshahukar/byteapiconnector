﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Task
    {
        public int TaskID { get; set; }
        public DateTime DateDue { get; set; }
        public string Description { get; set; }
        public object DateCompleted { get; set; }
        public string CompletedBy { get; set; }
        public string Notes { get; set; }
        public int AssignedUserRole { get; set; }
        public string CreatedBy { get; set; }
        public int VisibleToUserRoles { get; set; }
        public bool SendNotification { get; set; }
        public int NotificationUserRoles { get; set; }
        public string NotificationOtherEmail { get; set; }
        public string NotificationMessage { get; set; }
        public int NotificationParties { get; set; }
        public int CreatedByUserRole { get; set; }
        public int TaskSecurity { get; set; }
        public string NotificationMessageBorrower { get; set; }
        public int TaskPriority { get; set; }
        public object TaskTemplateID { get; set; }
        public bool OverrideDescription { get; set; }
        public int FileDataID { get; set; }
    }
}
