﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
   public class IRSForm1098
    {
        public int IRSForm1098ID { get; set; }
        public int AddressOption { get; set; }
        public string AccountNumber { get; set; }
        public object MortgageInterest { get; set; }
        public object Points { get; set; }
        public object Refund { get; set; }
        public object MIP { get; set; }
        public object OutstandingPrincipalOnJanFirst { get; set; }
        public int FileDataID { get; set; }
    }
}
