﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class ByteLoanData
    {
        public int FileDataID { get; set; }
        public FileData FileData { get; set; }
        public Status Status { get; set; }
        public List<Application> Applications { get; set; }
        public SubProp SubProp { get; set; }
        public Loan Loan { get; set; }
        public List<ClosingCost> ClosingCosts { get; set; }
        public List<PrepaidItem> PrepaidItems { get; set; }
        public List<object> HELOCPeriods { get; set; }
        public List<object> DocPackages { get; set; }
        public DOT DOT { get; set; }
        public Fannie Fannie { get; set; }
        public Freddie Freddie { get; set; }
        public List<Party> Parties { get; set; }
        public PartyMisc PartyMisc { get; set; }
        public List<Task> Tasks { get; set; }
        public List<object> NeededItems { get; set; }
        public List<object> Conditions { get; set; }
        public List<object> ExtendedTextValues { get; set; }
        public List<object> ExtendedDateValues { get; set; }
        public List<object> ExtendedBooleanValues { get; set; }
        public List<object> ExtendedDecimalValues { get; set; }
        public CreditDenial CreditDenial { get; set; }
        public CA883 CA883 { get; set; }
        public List<IRSForm4506s> IRSForm4506s { get; set; }
        public IRSForm1098 IRSForm1098 { get; set; }
        public Transmittal Transmittal { get; set; }
        public HMDA HMDA { get; set; }
        public FHA FHA { get; set; }
        public FHA203K FHA203K { get; set; }
        public FHAForms FHAForms { get; set; }
        public FHAMCAW FHAMCAW { get; set; }
        public VA VA { get; set; }
        public VALoanSummary VALoanSummary { get; set; }
        public VAValue VAValue { get; set; }
        public List<object> SelfEmpIncs { get; set; }
        public List<object> SelfEmpIncYears { get; set; }
        public SalesTools SalesTools { get; set; }
        public MiscForms MiscForms { get; set; }
        public List<object> TrustAccountItems { get; set; }
        public TXForms TXForms { get; set; }
        public CustomFields CustomFields { get; set; }
        public HUD1 HUD1 { get; set; }
        public List<object> NYAppLogFees { get; set; }
        public List<object> FieldNotes { get; set; }
        public List<object> OSOResults { get; set; }
        public List<object> PriceAdjustments { get; set; }
        public List<object> VAServiceDatas { get; set; }
        public List<object> VAPreviousLoans { get; set; }
        public HAMP HAMP { get; set; }
        public List<object> VAAuthorizedAgents { get; set; }
        public List<object> ShoppableProviders { get; set; }
        public List<object> LockHistories { get; set; }
        public Secondary Secondary { get; set; }
        public Closing Closing { get; set; }
        public List<object> LoanPayments { get; set; }
        public List<object> RelatedLoans { get; set; }
        public List<object> Markups { get; set; }
        public List<object> Snapshots { get; set; }
        public List<object> AdditionalParties { get; set; }
        public List<object> ClosingProrations { get; set; }
        public List<object> ClosingAdjustments { get; set; }
        public List<object> ClosingPayoffs { get; set; }
        public List<object> DisclosureLogEntries { get; set; }
        public List<object> DocSigners { get; set; }
        public List<object> ServiceOrders { get; set; }
    }
}
