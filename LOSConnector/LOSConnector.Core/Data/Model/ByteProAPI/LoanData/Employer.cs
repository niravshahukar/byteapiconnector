﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Employer
    {
        public int AppNo { get; set; }
        public int EmployerID { get; set; }
        public int BorrowerID { get; set; }
        public int DisplayOrder { get; set; }
        public Enum.EmployerStatus Status { get; set; }
        public string Name { get; set; }
        public string Attn { get; set; }
        public string FullAddress { get; set; }
        public string CityStateZip { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public bool SelfEmp { get; set; }
        public string Position { get; set; }
        public string TypeBus { get; set; }
        public string Phone { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public double? YearsOnJob { get; set; }
        public double? YearsInProf { get; set; }
        public double? MoIncome { get; set; }
        public string Notes { get; set; }
        public string Fax { get; set; }
        public string VOEVendorID { get; set; }
        public string VOESalaryID { get; set; }
        public int? TimeInLineOfWorkYears { get; set; }
        public int? TimeInLineOfWorkMonths { get; set; }
        public Enum.YesNoNA IsSpecialRelationship { get; set; }
        public int OwnershipInterest { get; set; }
        public bool IsForeignEmployment { get; set; }
        public bool IsSeasonalEmployment { get; set; }
        public int FileDataID { get; set; }
    }
}
