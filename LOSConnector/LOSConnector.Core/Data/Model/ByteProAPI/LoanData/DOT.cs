﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class DOT
    {
        public int DOTID { get; set; }
        public object RefiDebtsToBePaidOffOV { get; set; }
        public object EstPrepaidsOV { get; set; }
        public object EstClosingCostsOV { get; set; }
        public object MIPFFTotalOV { get; set; }
        public object DiscountOV { get; set; }
        public object TotalCostsOV { get; set; }
        public object ClosingCostsPaidBySellerOV { get; set; }
        public object CashFromToBorrowerOV { get; set; }
        public object AppDepositAmount { get; set; }
        public string AppDepositHeldBy { get; set; }
        public object EarnestMoneyAmount { get; set; }
        public string EarnestMoneyHeldBy { get; set; }
        public bool ExcludeSubFi { get; set; }
        public object _OtherCreditAmount5 { get; set; }
        public object _OtherCreditDesc5 { get; set; }
        public object _OtherCreditDesc6 { get; set; }
        public object _OtherCreditAmount6 { get; set; }
        public object _LineA { get; set; }
        public object _LineB { get; set; }
        public object _LineC { get; set; }
        public object _LineD { get; set; }
        public double _LineE { get; set; }
        public double _LineF { get; set; }
        public object _LineG { get; set; }
        public double _LineH { get; set; }
        public double _LineI { get; set; }
        public object _LineJ { get; set; }
        public object _LineK { get; set; }
        public object _LineL1 { get; set; }
        public object _LineL2 { get; set; }
        public object _LineL3 { get; set; }
        public object _LineL4 { get; set; }
        public object _LineM { get; set; }
        public object _LineN { get; set; }
        public object _LineO { get; set; }
        public object _LineP { get; set; }
        public object _CashFromToBorrower { get; set; }
        public object LECashToCloseAdjustmentAmount { get; set; }
        public string LECashToCloseAdjustmentDesc { get; set; }
        public object RefiSubPropLoansToBePaidOffOV { get; set; }
        public object OtherDebtsToBePaidOffOV { get; set; }
        public object BorrowerClosingCostsOV { get; set; }
        public object DiscountPointsOV { get; set; }
        public int FileDataID { get; set; }
    }
}
