﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Borrowers
    {
        public int FileDataID { get; set; }
        public int BorrowerID { get; set; }
        public Borrower Borrower { get; set; }
        public List<Residence> Residences { get; set; }
        public List<Employer> Employers { get; set; }
        public List<Income> Incomes { get; set; }
        public List<Asset> Assets { get; set; }
        public List<REO> REOs { get; set; }
        public List<Debt> Debts { get; set; }
        public List<CreditAlias> CreditAliases { get; set; }
        public List<Expense> Expenses { get; set; }
        public List<Gift> Gifts { get; set; }
    }
}
