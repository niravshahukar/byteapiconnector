﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Gift
    {
        public int FileDataID { get; set; }
        public int AppNo { get; set; }
        public int? BorrowerID { get; set; }
        public Enum.GiftAssetType AssetType { get; set; }
        public Decimal? GiftAmount { get; set; }
        public Enum.GiftDepositedStatus DepositedStatus { get; set; }
        public Enum.GiftSource Source { get; set; }
        public string DonorRelationshipToBorrowerDesc { get; set; }
        public string DonorName1 { get; set; }
        public string DonorName2 { get; set; }
        public string DonorStreet { get; set; }
        public string DonorCity { get; set; }
        public string DonorState { get; set; }
        public string DonorZip { get; set; }
        public string DonorPhone { get; set; }
        public string DonorAccountInstitution { get; set; }
        public string DonorInstitutionAddress { get; set; }
        public string DonorAccountNo { get; set; }
        public Enum.YesNoNA FundsProvidedAtClosing { get; set; }
        public DateTime? TransferDate { get; set; }
        public string SourceOtherDesc { get; set; }
        public int AccountHeldByType { get; set; }
    }
}
