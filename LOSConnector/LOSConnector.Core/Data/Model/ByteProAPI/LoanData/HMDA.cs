﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class HMDA
    {
        public int HMDAID { get; set; }
        public int PropertyType { get; set; }
        public object ActionDate { get; set; }
        public int ActionTaken { get; set; }
        public string CountyCode { get; set; }
        public string CensusTract { get; set; }
        public bool GrossAnnualIncomeNA { get; set; }
        public bool StateCodeNA { get; set; }
        public bool ApplicationDateNA { get; set; }
        public bool CountyCodeNA { get; set; }
        public bool CensusTractNA { get; set; }
        public bool MSANoNA { get; set; }
        public int _ApplicantEthnicity { get; set; }
        public int _CoApplicantEthnicity { get; set; }
        public int _ApplicantRace1 { get; set; }
        public int _ApplicantRace2 { get; set; }
        public int _ApplicantRace3 { get; set; }
        public int _ApplicantRace4 { get; set; }
        public int _ApplicantRace5 { get; set; }
        public int _CoApplicantRace1 { get; set; }
        public int _CoApplicantRace2 { get; set; }
        public int _CoApplicantRace3 { get; set; }
        public int _CoApplicantRace4 { get; set; }
        public int _CoApplicantRace5 { get; set; }
        public int _ApplicantSex { get; set; }
        public int _CoApplicantSex { get; set; }
        public int _Occupancy { get; set; }
        public string _GrossAnnualIncomeInThousands { get; set; }
        public string _ApplicationDate { get; set; }
        public object _CountyCodeDisplay { get; set; }
        public object _CensusTractDisplay { get; set; }
        public object _MSANo { get; set; }
        public string _StateCode { get; set; }
        public string DenialReasonOther { get; set; }
        public int AUSResult1 { get; set; }
        public int AUSResult2 { get; set; }
        public int AUSResult3 { get; set; }
        public int AUSResult4 { get; set; }
        public int AUSResult5 { get; set; }
        public int AUSSystem1 { get; set; }
        public int AUSSystem2 { get; set; }
        public int AUSSystem3 { get; set; }
        public int AUSSystem4 { get; set; }
        public int AUSSystem5 { get; set; }
        public string AUSResultOtherDesc { get; set; }
        public string AUSSystemOther { get; set; }
        public bool PropertyValueNA { get; set; }
        public object Term { get; set; }
        public bool TermNA { get; set; }
        public object TotalLoanCosts { get; set; }
        public bool TotalLoanCostsNA { get; set; }
        public object TotalPointsAndFees { get; set; }
        public bool TotalPointsAndFeesNA { get; set; }
        public object OriginationCharges { get; set; }
        public bool OriginationChargesNA { get; set; }
        public object DiscountPoints { get; set; }
        public bool DiscountPointsNA { get; set; }
        public object LenderCredits { get; set; }
        public bool LenderCreditsNA { get; set; }
        public object InterestRate { get; set; }
        public bool InterestRateNA { get; set; }
        public object DTIRatio { get; set; }
        public bool DTIRatioNA { get; set; }
        public object CLTV { get; set; }
        public bool CLTV_NA { get; set; }
        public object IntroductoryRatePeriod { get; set; }
        public bool IntroductoryRatePeriodNA { get; set; }
        public bool HasBalloonPayment { get; set; }
        public bool HasInterestOnlyPayments { get; set; }
        public bool HasOtherNonAmortizingFeature { get; set; }
        public string _ApplicantGMICombined { get; set; }
        public string _CoApplicantGMICombined { get; set; }
        public object ApplicantAge { get; set; }
        public object CoApplicantAge { get; set; }
        public object ApplicantCreditScore { get; set; }
        public int ApplicantCreditScoreModel { get; set; }
        public string ApplicantCreditScoreModelOther { get; set; }
        public int CoApplicantCreditScore { get; set; }
        public int CoApplicantCreditScoreModel { get; set; }
        public string CoApplicantCreditScoreModelOther { get; set; }
        public object PropertyValue { get; set; }
        public bool MLO_NMLSID_NA { get; set; }
        public bool PrepaymentPenaltyNA { get; set; }
        public object LoanAmount { get; set; }
        public bool IsPartiallyExemptFromHMDA { get; set; }
        public int InitiallyPayableToYourInstitutionOV { get; set; }
        public int FileDataID { get; set; }
    }
}
