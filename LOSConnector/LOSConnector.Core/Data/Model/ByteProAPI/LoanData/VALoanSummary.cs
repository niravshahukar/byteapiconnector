﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class VALoanSummary
    {
        public int VALoanSummaryID { get; set; }
        public string EntitlementCode { get; set; }
        public object EntitlementAmount { get; set; }
        public int VAServiceBranch { get; set; }
        public int VAMilitaryStatus { get; set; }
        public int VALoanProcedure { get; set; }
        public int VALoanPurpose { get; set; }
        public int VALoanCode { get; set; }
        public int VAMortgageType { get; set; }
        public int VAHybridARMType { get; set; }
        public int VAOwnershipType { get; set; }
        public object EnergyImpAmount { get; set; }
        public bool EnergyImpNone { get; set; }
        public bool EnergyImpSolar { get; set; }
        public bool EnergyImpReplacement { get; set; }
        public bool EnergyImpAddition { get; set; }
        public bool EnergyImpInsulation { get; set; }
        public bool EnergyImpOther { get; set; }
        public int VAPropertyType { get; set; }
        public int VAAppraisalType { get; set; }
        public int VAStructureType { get; set; }
        public int VAPropertyDesignation { get; set; }
        public string MCRVNo { get; set; }
        public int VAManufacturedHomeType { get; set; }
        public string LenderSARID { get; set; }
        public object SARIssueDate { get; set; }
        public int AdjustedBySAR { get; set; }
        public int ProcessedWithAUS { get; set; }
        public int VAAUSSystem { get; set; }
        public int VARiskClassification { get; set; }
        public object VAMedianCreditScore { get; set; }
        public object ResidualIncomeGuideline { get; set; }
        public int ConsiderSpouseIncome { get; set; }
        public object SpouseIncome { get; set; }
        public int TotalDiscountOption { get; set; }
        public int VeteranDiscountOption { get; set; }
        public int VAFundingFeeExempt { get; set; }
        public string OriginalVALoanNo { get; set; }
        public object OriginalLoanAmount { get; set; }
        public object OriginalIntRate { get; set; }
        public string VALoanSummaryRemarks { get; set; }
        public object VADiscountPoints { get; set; }
        public object VADiscountAmount { get; set; }
        public object VADiscountPointVeteran { get; set; }
        public object VADiscountAmountVeteran { get; set; }
        public int VAPriorLoanType { get; set; }
        public int FileDataID { get; set; }
    }
}
