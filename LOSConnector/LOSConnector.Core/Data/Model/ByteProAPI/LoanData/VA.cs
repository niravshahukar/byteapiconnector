﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class VA
    {
        public int VAID { get; set; }
        public object VeteranBorrowerID { get; set; }
        public int VATitleVested { get; set; }
        public string VATitleVestedOtherDesc { get; set; }
        public object DateOfApproval { get; set; }
        public object DateApprovalExpires { get; set; }
        public int HasVADebt { get; set; }
        public object CashFromVeteran { get; set; }
        public object IRRRCCPlusPPOV { get; set; }
        public object Maintenance { get; set; }
        public object Utilities { get; set; }
        public object DownPaymentOV { get; set; }
        public object LiquidAssetsOV { get; set; }
        public int UtilitiesIncludedInPresPITI { get; set; }
        public object SpecialAssessments { get; set; }
        public int PastCreditRecord { get; set; }
        public int MeetCreditStandards { get; set; }
        public string LoanAnalysisRemarks { get; set; }
        public string Line36OtherDeductionsDesc { get; set; }
        public string Line39OtherIncomesDesc { get; set; }
        public object Line44SupportBalanceOV { get; set; }
        public int HaveFiledClaim { get; set; }
        public string CertMailingFullAddress { get; set; }
        public string CertMailingCityStateZip { get; set; }
        public string CertMailingStreet { get; set; }
        public string CertMailingCity { get; set; }
        public string CertMailingState { get; set; }
        public string CertMailingZip { get; set; }
        public int VeteranHasDisability { get; set; }
        public object LoanDisburementReportDate { get; set; }
        public string RelativeName { get; set; }
        public string RelativeFullAddress { get; set; }
        public string RelativeCityStateZip { get; set; }
        public string RelativeStreet { get; set; }
        public string RelativeCity { get; set; }
        public string RelativeState { get; set; }
        public string RelativeZip { get; set; }
        public string RelativePhone { get; set; }
        public int IssuanceOfEvidence { get; set; }
        public object FullyPaidOutDate { get; set; }
        public string LienTypeOther { get; set; }
        public object UnpaidSpecialAssessments { get; set; }
        public object AnnualMaintenanceAssessment { get; set; }
        public string NonRealtyAcquired { get; set; }
        public string AdditionalSecurity { get; set; }
        public int WithheldAmountDepositType { get; set; }
        public bool ConstructionCompletedProperly { get; set; }
        public string ApprovedUnderwriter { get; set; }
        public bool VetHasNotBeenDischarged { get; set; }
        public bool OmitGovInfoFromLoanDisb { get; set; }
        public object ApproxAnnualAssessmentOV { get; set; }
        public int LienTypeOV { get; set; }
        public object AmountWithheldFromProceeds { get; set; }
        public object ApproxAnnualRealEstateTaxesOV { get; set; }
        public object PreviousLoanAmount { get; set; }
        public object PreviousTerm { get; set; }
        public object PreviousMonthlyPI { get; set; }
        public object PreviousIntRate { get; set; }
        public int ShowLendersCert { get; set; }
        public string PreviousVALoanNo { get; set; }
        public object PrevLoanClosed { get; set; }
        public object PreviousMonthlyPITI { get; set; }
        public int ActiveDutyDayFollowingSeperation { get; set; }
        public string VeteranStatusFileRef { get; set; }
        public bool ServedUnderAnotherName { get; set; }
        public string OtherNamesUsedDuringMilitaryService { get; set; }
        public bool BorrowerHadPreviousVALoan { get; set; }
        public int PrevLoanMoreThan30DaysLateInPast6Mo { get; set; }
        public int RecommendedAction { get; set; }
        public int FinalAction { get; set; }
        public object CRVValue { get; set; }
        public object CRVExpirationDate { get; set; }
        public object CRVEconomicLife { get; set; }
        public int PreviousLoanType { get; set; }
        public string PreviousLoanTypeOtherDesc { get; set; }
        public object PreviousTotalOfPIAndMIPayments { get; set; }
        public bool NTBEliminatesMI { get; set; }
        public bool NTBIncreasesResidualIncome { get; set; }
        public bool NTBRefinancesConstLoan { get; set; }
        public int VACashOutRefiType { get; set; }
        public int FileDataID { get; set; }
    }
}
