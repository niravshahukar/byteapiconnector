﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class FHA203K
    {
        public int FHA203KID { get; set; }
        public int HUDOwned { get; set; }
        public int CommitmentStage { get; set; }
        public int OccupancyType { get; set; }
        public object PurchaseDate { get; set; }
        public bool EscrowCommitment { get; set; }
        public object ExistingDebt { get; set; }
        public object AsIsValue { get; set; }
        public object AfterImprovedValue { get; set; }
        public object BorClosingCostsOV { get; set; }
        public object ContingencyPerc { get; set; }
        public object ContingencyPaidInCash { get; set; }
        public object CostOfRepairs { get; set; }
        public object InspectionCount { get; set; }
        public object InspectionCost { get; set; }
        public object TitleUpdateCount { get; set; }
        public object TitleUpdateCost { get; set; }
        public object MonthsEscrowed { get; set; }
        public object MortgagePayment { get; set; }
        public object ArchAndEngFees { get; set; }
        public object ConsultantFees { get; set; }
        public object PermitsAndOtherFees { get; set; }
        public object ReviewerMiles { get; set; }
        public object ReviewerCostPerMile { get; set; }
        public object ReviewerOtherFee { get; set; }
        public bool WaiveSuppOrigFee { get; set; }
        public object SuppOrigFeeOV { get; set; }
        public object DiscoutOnRepairs { get; set; }
        public object AllowableDownPayment { get; set; }
        public object PurRequiredInvOV { get; set; }
        public object PurMaxMortOV { get; set; }
        public object AllowableFinPrepaids { get; set; }
        public object DiscountOnLoan { get; set; }
        public object DiscountLoanBasis { get; set; }
        public object LineD4OV { get; set; }
        public object LineE1OV { get; set; }
        public object LineE2OV { get; set; }
        public object TotalEscrowedFunds { get; set; }
        public string Remarks { get; set; }
        public bool ShowDiscountInDollars { get; set; }
        public string DETitle { get; set; }
        public bool StreamlinedK { get; set; }
        public object FeasibilityStudyFee { get; set; }
        public object PurInducements { get; set; }
        public object LeadBasedPaintCredit { get; set; }
        public object MortgageLimit { get; set; }
        public object InitialBaseMortgageOV { get; set; }
        public object SolarWindCost { get; set; }
        public object MaterialCostForItemsOrderedPrepaid { get; set; }
        public object MaterialCostForItemsOrderedNotPrepaid { get; set; }
        public int FHA203KType { get; set; }
        public object DrawArchAndEngFees { get; set; }
        public object DrawPermitsAndOtherFees { get; set; }
        public object DrawSuppOrigFee { get; set; }
        public object DrawDiscountPointsAndFees { get; set; }
        public int PropertyAcquired { get; set; }
        public double _InitialBaseMortgage { get; set; }
        public object DiscountPointsAndFeesOV { get; set; }
        public object ContingencyTotalOV { get; set; }
        public int FileDataID { get; set; }
    }
}
