﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Closing
    {
        public int ClosingID { get; set; }
        public string NotaryName { get; set; }
        public string PropertyTaxDesc { get; set; }
        public string PropertyTaxYearDesc { get; set; }
        public string TrustName { get; set; }
        public int TrustType { get; set; }
        public string TrustTypeOtherDescription { get; set; }
        public object TrustEstablishedDate { get; set; }
        public string TrustState { get; set; }
        public string TrustName2 { get; set; }
        public int TrustType2 { get; set; }
        public string TrustTypeOtherDescription2 { get; set; }
        public object TrustEstablishedDate2 { get; set; }
        public string TrustState2 { get; set; }
        public string VestingDescription { get; set; }
        public int LoanProceedsTo { get; set; }
        public int RefiOriginalCreditorIncrease { get; set; }
        public int CDSplitDisclosureIndicator { get; set; }
        public object CDMainEmbeddedDocID { get; set; }
        public object CDSellerEmbeddedDocID { get; set; }
        public int TexasA6Status { get; set; }
        public int FileDataID { get; set; }
    }
}
