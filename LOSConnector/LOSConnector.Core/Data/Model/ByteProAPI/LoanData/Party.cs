﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class Party
    {
        public int PartyID { get; set; }
        public Enum.ContactCat CategoryID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string FullAddress { get; set; }
        public string CityStateZip { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string EMail { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string OtherPhone { get; set; }
        public string Pager { get; set; }
        public string Fax { get; set; }
        public string LicenseNo { get; set; }
        public string CHUMSNo { get; set; }
        public string FHAOrigOrSponsorID { get; set; }
        public string BranchID { get; set; }
        public string Notes { get; set; }
        public string ContactNMLSID { get; set; }
        public string CompanyNMLSID { get; set; }
        public bool LockToUser { get; set; }
        public string CompanyEIN { get; set; }
        public string MobilePhoneSMSGateway { get; set; }
        public string CompanyLicenseNo { get; set; }
        public string WirePrimaryBankName { get; set; }
        public string WirePrimaryBankFullAddress { get; set; }
        public string WirePrimaryBankCityStateZip { get; set; }
        public string WirePrimaryBankStreet { get; set; }
        public string WirePrimaryBankCity { get; set; }
        public string WirePrimaryBankState { get; set; }
        public string WirePrimaryBankZip { get; set; }
        public string WirePrimaryABANo { get; set; }
        public string WirePrimaryAccountNo { get; set; }
        public string WireFCTBankName { get; set; }
        public string WireFCTBankFullAddress { get; set; }
        public string WireFCTBankCityStateZip { get; set; }
        public string WireFCTBankStreet { get; set; }
        public string WireFCTBankCity { get; set; }
        public string WireFCTBankState { get; set; }
        public string WireFCTBankZip { get; set; }
        public string WireFCTABANo { get; set; }
        public string WireFCTAccountNo { get; set; }
        public bool SyncData { get; set; }
        public object EAndOPolicyExpirationDate { get; set; }
        public string LicensingAgencyCode { get; set; }
        public string EMail2 { get; set; }
        public string EMail3 { get; set; }
        public int FileDataID { get; set; }
    }
}
