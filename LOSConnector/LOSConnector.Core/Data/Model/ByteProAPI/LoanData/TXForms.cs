﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Data.Model.ByteProAPI.LoanData
{
    public class TXForms
    {
        public int TXFormsID { get; set; }
        public string BrokerOrLO { get; set; }
        public bool SubmitToLender { get; set; }
        public bool IndependentContractor { get; set; }
        public bool ActingAsFollows { get; set; }
        public string ActingAsFollowsText { get; set; }
        public bool RetailPrice { get; set; }
        public bool WholesaleOptions { get; set; }
        public object FeeAmount { get; set; }
        public object ApplicationFee { get; set; }
        public object ProcessingFee { get; set; }
        public object AppraisalFee { get; set; }
        public object CreditReportFee { get; set; }
        public object AUFee { get; set; }
        public string OtherFee1Desc { get; set; }
        public object OtherFee1Amount { get; set; }
        public string OtherFee2Desc { get; set; }
        public object OtherFee2Amount { get; set; }
        public object NonRefundableAmount { get; set; }
        public string LicensedEntity { get; set; }
        public string LicenseNo { get; set; }
        public bool PricingBasedOnCustomFlag { get; set; }
        public string PricingBasedOnCustomDesc { get; set; }
        public bool EstFeesShownOnGFEFlag { get; set; }
        public int FileDataID { get; set; }
    }
}
