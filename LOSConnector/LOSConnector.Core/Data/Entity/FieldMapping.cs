﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LOSConnector.Core.Data.Entity
{
    public class FieldMapping
    {
        [Key]
        public int MappingID { get; set; }
        public string BorrowerFieldValue { get; set; }
        public string ClientFieldName { get; set; }
        public string LOSFieldName { get; set; }
        public string LOSFieldNameAlias { get; set; }
        public int? FieldTypeID { get; set; }
        public bool ImportToLOS { get; set; }
        public bool ExportFromLOS { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [ForeignKey("FieldTypeID")]
        public virtual FieldType FieldType { get; set; }
    }
}
