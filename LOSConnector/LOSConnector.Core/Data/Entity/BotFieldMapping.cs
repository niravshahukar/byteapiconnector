﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LOSConnector.Core.Data.Entity
{
    public class BotFieldMapping
    {
        [Key]
        public long ID { get; set; }
        public int BotID { get; set; }
        public int MappingID { get; set; }
    }
}
