﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LOSConnector.Core.Data.Entity
{
    public class BotConfiguration
    {
        [Key]
        public int BotID { get; set; }
        public string BotName { get; set; }
        public string MortgageProcess { get; set; }
        public string LoanMilestone { get; set; }
        public string RuleAgent { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
