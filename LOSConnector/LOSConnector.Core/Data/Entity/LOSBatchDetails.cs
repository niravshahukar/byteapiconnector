﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LOSConnector.Core.Data.Entity
{
    public class LOSBatchDetails
    {
        [Key]
        public int Id { get; set; }
        public long? LOSBatchId { get; set; }
        public string ApplicationBatchId { get; set; }
        public string LOSFileName { get; set; }
        public string LOSFileUniqueId { get; set; }
        public string ApplicationBatchFileName { get; set; }
        public decimal? LOSFileSizeInKB { get; set; }
        public string LOSEntityId { get; set; }
        public string LOSEntityUniqueId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsLOSToAIF { get; set; }
        public string BucketName { get; set; }
    }
}
