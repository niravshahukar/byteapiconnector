﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LOSConnector.Core.Data.Entity
{
    public class LOSBatchs
    {
        [Key]
        public long LOSBatchId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public int? Status { get; set; }
        public int? DataRulesValidated { get; set; }
        public int? DocRulesValidated { get; set; }
        public string LOSBatchFileName { get; set; }
        public int RetryCount { get; set; }
    }
}
