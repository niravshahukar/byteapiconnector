﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LOSConnector.Core.Data.Entity
{
    public class RuleSetMapping
    {
        [Key]
        public int ID { get; set; }
        public string FieldID { get; set; }
        public string ComparisonType { get; set; }
        public string Value { get; set; }
        public string RuleSet { get; set; }
        public string CreatedDate { get; set; }
        public string DeletedDate { get; set; }
    }
}
