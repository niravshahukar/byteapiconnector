﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LOSConnector.Core.Data.Entity
{
    public class FieldType
    {
        public FieldType()
        {
            this.FieldMappings = new HashSet<FieldMapping>();
        }
        [Key]
        public int FieldTypeID { get; set; }
        public string FieldDataType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual ICollection<FieldMapping> FieldMappings { get; set; }

    }
}
