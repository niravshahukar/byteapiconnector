﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LOSConnector.Core.Data.Repository
{
    public interface IGenericRepository<TEntity> : IDisposable where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<IEnumerable<TEntity>> GetPaginated(int page, int size);
        public bool Exist(Expression<Func<TEntity, bool>> expression);
        public Task<bool> ExistAsync(Expression<Func<TEntity, bool>> expression);
        public Task<IEnumerable<TEntity>> GetAll(Expression<Func<TEntity, bool>> expression);
        public IQueryable<TEntity> IQueryableGetMany(Expression<Func<TEntity, bool>> filter = null);
        Task<TEntity> GetById(object id);
        Task Create(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(object id);
    }
}
