﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LOSConnector.Core.Extensions
{
    public static class AppHelper
    {
        /// <summary>
        /// Get list of object from comma sepereted value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="appSettingsKeyValue"></param>
        /// <returns></returns>
        public static List<T> GetAppSettingsKeyValue<T>(string appSettingsKeyValue) where T : class
        {
            List<T> obj = Activator.CreateInstance<List<T>>();
            if (appSettingsKeyValue == null || appSettingsKeyValue.Length == 0)
                return default(List<T>);
            try
            {
                var value = appSettingsKeyValue.Split(',').Select(s => s.Trim()).ToList<object>();
                obj = value.Cast<T>().ToList();
            }
            catch
            {
                throw;
            }
            return obj;
        }
        #region DataTable  to List of Model type
        /// <summary>
        /// Convert data table to any class obj
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<T> ToList<T>(this DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static List<string> ToListString<T>(this DataTable dt)
        {
            List<string> data = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                string item = Convert.ToString(row[0]);
                data.Add(item);
            }
            return data;
        }
        /// <summary>
        /// get data row details
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName && dr[column.ColumnName] != DBNull.Value)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion
    }
}
