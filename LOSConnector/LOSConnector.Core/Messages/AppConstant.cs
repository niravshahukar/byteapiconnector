﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Messages
{
    public class AppConstant
    {
        #region BytePro Loan Search
        public const string AC_FileDataID = "FileDataID";
        public const string AC_Status = "Status";
        public const string AC_FileName = "FileName";
        public const string AC_FileData = "FileData";
        public const string AC_LoanStatus = "LoanStatus";
        public const string AC_LoanUniqueId = "LoanUniqueId";
        public const string AC_usp_get_unprocessed_documents = "usp_get_unprocessed_documents";
        public const string AC_usp_updateStatus_unprocessed_documents = "usp_updateStatus_unprocessed_documents";
        public const string AC_At_EmbeddedDocID = "@EmbeddedDocID";
        public const string AC_At_Status = "@Status";
        #endregion
        public const string AC_DOT = ".";
        public const char AC_Char_DOT = '.';
        public const char AC_Comma = ',';
        public const string AC_String_Comma = ",";
        public const string AC_Asterisk_Symbol = "*";
        public const char AC_Char_Underscore = '_';
        public const string AC_String_Underscore = "_";
        public const char AC_Char_Dash = '-';
        public const string AC_Open_Square_Bracket = "[";
        public const string AC_Close_Square_Bracket = "]";
        public const string AC_Open_Curly_Bracket = "{";
        public const string AC_Close_Curly_Bracket = "}";
        public const char AC_Char_Open_Curly_Bracket = '{';
        public const char AC_Char_Close_Curly_Bracket = '}';
        public const string AC_Right_ShiftOperator = ">>";
        public const string AC_Space = " ";
        public const string AC_File_Location_Slash = "\\";
        public const string AC_String_Forward_Slash = "/";
        public const string AC_Colon = ":";
        public const string AC_DOT_Xml = ".xml";
        public const string AC_DOT_Pdf = ".pdf";
        public const string AC_Asterisk_Pdf = "*.pdf";
        public const string AC_Pdf = "pdf";
        public const string AC_DOT_Tif = ".tif";
        public const string AC_Tif = "tif";
        public const string AC_Tiff = "tiff";
        public const string AC_DOT_Json = ".json";
        public const string AC_DOT_Zip = ".zip";
    }
}
