﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Enum
{
    #region Application Model
    public enum ApplicationMethod
    {
        NotAssigned = 0,
        FaceToFace = 1,
        Mail = 2,
        Telephone = 3,
        Internet = 4
    }
    public enum OtherExpenseType
    {
        NotAssigned = 0,
        Alimony = 1,
        ChildSupport = 2,
        SeparateMaintenance = 3,
        OtherExpense = 4
    }
    public enum StockBondType
    {
        NotAssigned = 0,
        Stock = 1,
        Bond = 2
    }
    public enum StatementsCompleted
    {
        NotAssigned = 0,
        Jointly = 1,
        NotJointly = 2
    }
    #endregion
    #region FileData Model
    public enum OccupancyType
    {
        NotAssigned = 0,
        PrimaryResidence = 1,
        SecondaryResidence = 2,
        InvestmentProperty = 3
    }
    public enum DownPaymentType
    {
        NotAssigned = 0,
        CheckingOrSavings = 1,
        DepositOnSalesContract = 2,
        EquityOnSoldProperty = 3,
        EquityOnPendingSale = 4,
        EquityOnSubjectProperty = 5,
        GiftFunds = 6,
        StocksAndBonds = 7,
        LotEquity = 8,
        BridgeLoan = 9,
        UnsecuredBorrowedFunds = 10,
        TrustFunds = 11,
        RetirementFunds = 12,
        RentWithOptionToPurchase = 13,
        LifeInsuranceCashValue = 14,
        SaleOfChattel = 15,
        TradeEquity = 16,
        SweatEquity = 17,
        CashOnHand = 18,
        Other = 19,
        SecuredBorrowedFunds = 20,
        FHAGiftNA = 21,
        FHAGiftRelative = 22,
        FHAGiftGovAssist = 23,
        FHAGiftEmployer = 24,
        FHAGiftNonProfitSellerFunded = 25,
        FHAGiftNonProfitNonSellerFunded = 26
    }
    public enum EscrowWaiverType
    {
        NotAssigned = 0,
        WaiveBoth = 1,
        NotWaived = 2,
        WaiveTaxesOnly = 3,
        WaiveInsuranceOnly = 4
    }
    public enum VAFundingFeeCategory
    {
        NotAssigned = 0,
        RegularMilitaryFirstUse = 1,
        RegularMilitarySubsequentUse = 2,
        ReservesOrGuardFirstUse = 3,
        ReservesOrGuardSubsequentUse = 4,
        InterestRateReductionRefi = 5,
        ManufacturedHome = 6,
        LoanAssumption = 7,
        Exempt = 8
    }
    public enum FirstMortgageHolder
    {
        NotAssigned = 0,
        FannieMae = 1,
        FreddieMac = 2,
        SellerOrOther = 3,
        Unknown = 4
    }
    public enum UserAccess
    {
        FullAccess = 0,
        ReadOnly = 1,
        EditConversationLogOnly = 2
    }
    public enum DocumentationType
    {
        NotAssigned = 0,
        Alternative = 1,
        Full = 2,
        Reduced = 3,
        StreamlinedRefi = 4,
        NoDocumentation = 5,
        NoRatio = 6,
        LimitedDocumentation = 7,
        NoIncNoEmpNoAssets = 8,
        NoIncNoAssets = 9,
        NoAssets = 10,
        NoIncNoEmp = 11,
        NoInc = 12,
        NoVerifOfIncEmpAssets = 13,
        NoVerifOfIncAssets = 14,
        NoVerifOfAssets = 15,
        NoVerifOfIncEmp = 16,
        NoVerifOfInc = 17,
        VerbalEmp = 18,
        OnePaystub = 19,
        OnePaystubAndVVOE = 20,
        OnePaystubOneW2AndVVOEOr1040 = 21
    }
    public enum EvidenceOfTitleOption
    {
        NotAssigned = 0,
        AbstractOfTitle = 1,
        TorrensTitleGuaranteePolicy = 2,
        MortgageTitleGuaranteePolicy = 3,
        LawyersOpinionOfTitle = 4,
        Other = 5
    }
    public enum CommitmentReturnOption
    {
        AboveAddress = 0,
        FollowingAddress = 1
    }
    public enum SuperLoanType
    {
        NewLoan = 0,
        LoanModification = 1
    }
    public enum GFEVersion
    {
        Pre2010 = 0,
        _2010 = 1
    }
    public enum CompanyType
    {
        NotAssigned = 0,
        Lender = 1,
        Broker = 2
    }
    public enum OriginationChannel
    {
        NotAssigned = 0,
        Retail = 1,
        BrokeredOut = 2,
        Wholesale = 3,
        Purchased = 4,
        MiniCorr = 5,
        ConsumerDirect = 6
    }
    public enum TILVersionOV
    {
        NotAssigned = 0,
        _2011 = 1,
        Pre2011 = 2
    }
    public enum LockRequestType
    {
        NotAssigned = 0,
        NewLock = 1,
        LockExtension = 2,
        Relock = 3,
        Renegotiation = 4,
        LockCancellation = 5,
        LockRequestedInPPE = 6,
        Snapshot = 7,
        CurrentState = 8,
        BestExInPPE = 9,
        ChangedInPPE = 10,
        LockAcceptedInPPE = 11,
        LockCancelledInPPE = 12,
        LockRejectedInPPE = 13,
        RegisteredInPPE = 14,
        UpdatedInPPE = 15
    }
    public enum DocTypeEmployment
    {
        NotAssigned = 0,
        Verified = 1,
        Stated = 2,
        None = 3
    }
    public enum DocTypeIncome
    {
        NotAssigned = 0,
        Verified = 1,
        Stated = 2,
        StatedWith4506T = 3,
        None = 4
    }
    public enum DocTypeAsset
    {
        NotAssigned = 0,
        Verified = 1,
        Stated = 2,
        None = 3
    }
    public enum LOCompType
    {
        NotAssigned = 0,
        BorrowerPaid = 1,
        LenderPaid = 2
    }
    public enum SyncType
    {
        None = 0,
        PiggybackFirst = 1,
        PiggybackSecond = 2,
        PiggybackThird = 3
    }
    public enum EscrowAbsenceReason
    {
        NotAssigned = 0,
        Declined = 1,
        NotOffered = 2
    }
    public enum PartialPaymentOption
    {
        NotAssigned = 0,
        AppliedToTheLoan = 1,
        HeldInSeparateAccount = 2,
        NotAccepted = 3
    }
    public enum LiabilityAfterForeclosure
    {
        NotAssigned = 0,
        StateLawMayProtectYou = 1,
        StateLawDoesNotProtectYou = 2
    }
    public enum ConstTILACalcMethod
    {
        AppendixD = 0,
        AppendixJ = 1,
        AppendixDAppendixJ = 2
    }
    public enum URLAVersion
    {
        Classic = 0,
        ClassicWithGMIAddendum = 1,
        _2019 = 2
    }
    #endregion
    #region Status Model
    public enum LoanStatus
    {
        NotAssigned = 0,
        Prequal = 1,
        CreditOnly = 2,
        InProcessing = 3,
        Submitted = 4,
        Approved = 5,
        Resubmitted = 6,
        Declined = 7,
        InClosing = 8,
        Closed = 9,
        Canceled = 10,
        Lead = 11,
        Suspended = 12,
        DocsSigned = 13,
        Funded = 14,
        Purchased = 15,
        ClearToClose = 16,
        DocsSent = 17,
        CollateralSent = 18,
        Shipped = 19
    }
    public enum FollowUpFlagFile
    {
        NotAssigned = 0,
        RedFlag = 1,
        BlueFlag = 2,
        YellowFlag = 3,
        GreenFlag = 4,
        OrangeFlag = 5,
        PurpleFlag = 6
    }
    public enum GFEDeliveryMethod
    {
        NotAssigned = 0,
        InPerson = 1,
        Mailed = 2,
        EMailed = 3,
        Faxed = 4,
        ESigned = 5,
        EDelivered = 6
    }
    public enum ComplianceTestResult
    {
        NotRun = 0,
        NotApplicable = 1,
        Pass = 2,
        Fail = 3
    }
    public enum ComplianceEaseRiskIndicator
    {
        NotRun = 0,
        Critical = 1,
        Elevated = 2,
        Minimal = 3,
        Moderate = 4,
        Significant = 5
    }
    public enum AppraisalDeliveryMethod
    {
        NotAssigned = 0,
        InPerson = 1,
        Mailed = 2,
        EMailed = 3,
        Faxed = 4
    }
    public enum CDDateIssuedOption
    {
        DateOnDocuments = 0,
        ScheduledClosingDate = 1,
        OtherDate = 2
    }
    public enum ApplicationTestResult
    {
        NotAssigned = 0,
        HasSixDataPointsAndAppDateSet = 1,
        HasSixDataPointsAndAppDateNotSet = 2,
        DoesNotHaveSixDataPointsAndAppDateSet = 3,
        DoesNotHaveSixDataPointsAndAppDateNotSet = 4
    }
    public enum FeeReconciliationTestResult
    {
        NotAssigned = 0,
        NotATRIDLoan = 1,
        LENotDelivered = 2,
        NoUnreconciledFeeIncreases = 3,
        UnreconciledFeeIncreases = 4
    }
    public enum TRIDDisclosureTestResult
    {
        NotAssigned = 0,
        NotATRIDLoan = 1,
        ApplicationDateNotSet = 2,
        LEInitialDisclosureRequired = 3,
        RedisclosureRequired = 4,
        RedisclosureNotRequired = 5
    }
    public enum LEDisclosureTestResult
    {
        NotAssigned = 0,
        NotATRIDLoan = 1,
        ApplicationDateNotSet = 2,
        LEInitialDisclosureRequired = 3,
        LERedisclosureRequired = 4,
        LERedisclosureNotRequired = 5,
        LERedisclosureNotPermittedCDDisclosed = 6
    }
    public enum CDDisclosureTestResult
    {
        NotAssigned = 0,
        NotATRIDLoan = 1,
        ApplicationDateNotSet = 2,
        CDNotDisclosed = 3,
        CDRedisclosureNotRequired = 4,
        CDRedisclosureRequired = 5
    }
    public enum SigningAppointmentLocationType
    {
        NotAssigned = 0,
        SettlementCompany = 1,
        TitleCompany = 2,
        EscrowCompany = 3
    }
    public enum ESignTestResult
    {
        NoESignPackagesSent = 0,
        NoUnsignedPackages = 1,
        NoUndeliveredPackages = 2,
        UndeliveredPackages = 3,
        PackagesPastEDeliveryDeadline = 4
    }
    public enum NMLSApplicationDateCalcOption
    {
        Default = 0,
        HMDADeniedDate = 1,
        StatusDeclinedDate = 2
    }
    #endregion
    #region Party Model
    public enum ContactCat
    {
        NotAssigned = 0,
        LoanOfficer = 1,
        LoanProcessor = 2,
        LoanCloser = 3,
        Underwriter = 4,
        Lender = 10,
        MortgageeForInsurance = 11,
        FHASponsoringLender = 12,
        FHAOriginatingLender = 13,
        RealEstateAgent = 20,
        ListingAgent = 21,
        SellingAgent = 22,
        BuilderOrSeller = 25,
        Warrantor = 26,
        HazardInsuranceCompany = 31,
        MortgageInsuranceCompany = 32,
        FloodInsuranceCompany = 33,
        FloodDeterminationCompany = 34,
        TitleCompany = 40,
        Appraiser = 42,
        EscrowCompany = 44,
        SurveyCompany = 46,
        CreditBureau = 48,
        Attorney = 59,
        BorrowerAttorney = 60,
        SellerAttorney = 61,
        LenderAttorney = 62,
        Inspector = 70,
        PestInspector = 71,
        Creditor = 80,
        Bank = 81,
        Employer = 82,
        Landlord = 83,
        LoanSeller = 84,
        Investor = 85,
        Contractor = 86,
        Borrower = 99,
        OpenHouseAgent = 101,
        OtherUser = 102,
        ReferralSource = 103,
        HomeOwnersAssoc = 104,
        SettlementLocation = 105,
        OtherUser2 = 106,
        OtherUser3 = 107,
        OtherUser4 = 108,
        MortgageeForTitle = 109,
        Broker = 110,
        Appraiser2 = 111,
        TaxServiceCompany = 112,
        HazardInsuranceCompany2 = 113,
        Trustee = 114,
        OtherParty1 = 501,
        OtherParty2 = 502,
        OtherParty3 = 503,
        OtherParty4 = 504,
        OtherParty5 = 505,
        Opener = 600,
        DocDrawer = 601,
        QualityControl = 602,
        Compliance = 603,
        Shipper = 604,
        LockDesk = 605,
        Accounting = 606,
        Servicing = 607,
        Insuring = 608,
        Secondary = 609,
        OtherUser5 = 610,
        OtherUser6 = 611,
        OtherUser7 = 612,
        OtherUser8 = 613,
        OtherUser9 = 614,
        OtherUser10 = 615,
        OtherUser11 = 616,
        OtherUser12 = 617,
        OtherUser13 = 618,
        OtherUser14 = 619,
        OtherUser15 = 620,
        OtherUser16 = 621,
        OtherUser17 = 622,
        OtherUser18 = 623,
        OtherUser19 = 624,
        OtherUser20 = 625,
        OtherUser21 = 626,
        OtherUser22 = 627,
        OtherUser23 = 628,
        OtherUser24 = 629,
        OtherUser25 = 630
    }
    #endregion
    #region ClosingCost Model
    public enum HUDCCLineNo
    {
        NotAssigned = 0,
        _801 = 801,
        _802 = 802,
        _803 = 803,
        _804 = 804,
        _805 = 805,
        _806 = 806,
        _807 = 807,
        _808 = 808,
        _809 = 809,
        _810 = 810,
        _811 = 811,
        _812 = 812,
        _813 = 813,
        _814 = 814,
        _815 = 815,
        _816 = 816,
        _817 = 817,
        _818 = 818,
        _819 = 819,
        _820 = 820,
        _821 = 821,
        _822 = 822,
        _823 = 823,
        _824 = 824,
        _825 = 825,
        _826 = 826,
        _827 = 827,
        _828 = 828,
        _829 = 829,
        _830 = 830,
        _831 = 831,
        _832 = 832,
        _833 = 833,
        _834 = 834,
        _835 = 835,
        _836 = 836,
        _837 = 837,
        _838 = 838,
        _839 = 839,
        _840 = 840,
        _841 = 841,
        _842 = 842,
        _843 = 843,
        _844 = 844,
        _845 = 845,
        _846 = 846,
        _847 = 847,
        _848 = 848,
        _849 = 849,
        _850 = 850,
        _851 = 851,
        _852 = 852,
        _853 = 853,
        _854 = 854,
        _855 = 855,
        _856 = 856,
        _857 = 857,
        _858 = 858,
        _859 = 859,
        _860 = 860,
        _861 = 861,
        _862 = 862,
        _863 = 863,
        _864 = 864,
        _865 = 865,
        _866 = 866,
        _867 = 867,
        _868 = 868,
        _869 = 869,
        _870 = 870,
        _871 = 871,
        _872 = 872,
        _873 = 873,
        _874 = 874,
        _875 = 875,
        _876 = 876,
        _877 = 877,
        _878 = 878,
        _879 = 879,
        _880 = 880,
        _881 = 881,
        _882 = 882,
        _883 = 883,
        _884 = 884,
        _885 = 885,
        _886 = 886,
        _887 = 887,
        _888 = 888,
        _889 = 889,
        _890 = 890,
        _1101 = 1101,
        _1102 = 1102,
        _1103 = 1103,
        _1104 = 1104,
        _1105 = 1105,
        _1106 = 1106,
        _1107 = 1107,
        _1108 = 1108,
        _1109 = 1109,
        _1110 = 1110,
        _1111 = 1111,
        _1112 = 1112,
        _1113 = 1113,
        _1114 = 1114,
        _1115 = 1115,
        _1116 = 1116,
        _1117 = 1117,
        _1118 = 1118,
        _1119 = 1119,
        _1120 = 1120,
        _1121 = 1121,
        _1122 = 1122,
        _1123 = 1123,
        _1124 = 1124,
        _1125 = 1125,
        _1201 = 1201,
        _1202 = 1202,
        _1203 = 1203,
        _1204 = 1204,
        _1205 = 1205,
        _1206 = 1206,
        _1207 = 1207,
        _1208 = 1208,
        _1209 = 1209,
        _1210 = 1210,
        _1211 = 1211,
        _1212 = 1212,
        _1213 = 1213,
        _1214 = 1214,
        _1215 = 1215,
        _1301 = 1301,
        _1302 = 1302,
        _1303 = 1303,
        _1304 = 1304,
        _1305 = 1305,
        _1306 = 1306,
        _1307 = 1307,
        _1308 = 1308,
        _1309 = 1309,
        _1310 = 1310,
        _1311 = 1311,
        _1312 = 1312,
        _1313 = 1313,
        _1314 = 1314,
        _1315 = 1315,
        _1316 = 1316,
        _1317 = 1317,
        _1318 = 1318,
        _1319 = 1319,
        _1320 = 1320,
        _1321 = 1321,
        _1322 = 1322,
        _1323 = 1323,
        _1324 = 1324,
        _1325 = 1325,
    }
    public enum ClosingCostType
    {
        NotAssigned = 0,
        _203KDiscountOnRepairs = 1,
        _203KPermits = 2,
        _203KArchitecturalAndEngineeringFee = 3,
        _203KInspectionFee = 4,
        _203KSupplementalOriginationFee = 5,
        _203KConsultantFee = 6,
        _203KTitleUpdate = 7,
        AbstractOrTitleSearchFee = 8,
        AmortizationFee = 9,
        ApplicationFee = 10,
        AppraisalFee = 11,
        AssignmentFee = 12,
        AssignmentRecordingFee = 13,
        AssumptionFee = 14,
        AttorneyFee = 15,
        BondReviewFee = 16,
        CityCountyDeedTaxStampFee = 17,
        CityCountyMortgageTaxStampFee = 18,
        CLOAccessFee = 19,
        CommitmentFee = 20,
        CopyFaxFee = 21,
        CourierFee = 22,
        CreditReportFee = 23,
        DeedRecordingFee = 24,
        DocumentPreparationFee = 25,
        DocumentaryStampFee = 26,
        EscrowWaiverFee = 27,
        FloodCertification = 28,
        GeneralCounselFee = 29,
        InspectionFee = 30,
        LoanDiscountPoints = 31,
        LoanOriginationFee = 32,
        ModificationFee = 33,
        MortgageBrokerFee = 34,
        MortgageRecordingFee = 35,
        MunicipalLienCertificateFee = 36,
        MunicipalLienCertificateRecordingFee = 37,
        NewLoanAdministrationFee = 38,
        NotaryFee = 39,
        Other = 40,
        PestInspectionFee = 41,
        ProcessingFee = 42,
        RedrawFee = 43,
        RealEstateCommission = 44,
        ReinspectionFee = 45,
        ReleaseRecordingFee = 46,
        RuralHousingFee = 47,
        SettlementOrClosingFee = 48,
        StateDeedTaxStampFee = 49,
        StateMortgageTaxStampFee = 50,
        SurveyFee = 51,
        TaxRelatedServiceFee = 52,
        TitleExaminationFee = 53,
        TitleInsuranceBinderFee = 54,
        TitleInsuranceFee = 55,
        UnderwritingFee = 56,
        AppraisalDeskReviewFee = 57,
        AppraisalFieldReviewFee = 58,
        AssignmentEndorsementFee = 59,
        AttorneyFeeOther = 60,
        BankruptcyMonitoringFee = 61,
        BondFee = 62,
        CertificationFee = 63,
        ClosingProtectionLetterFee = 64,
        ComplianceAuditQualityControlFee = 65,
        DebtCancellationFee = 66,
        ElectronicDocumentDeliveryFee = 67,
        EscrowServiceFee = 68,
        FloodCertificationInitialFee = 69,
        FloodCertificationLifeOfLoanFee = 70,
        FundingWireOrDisbursementFee = 71,
        HELOCAnnualFee = 72,
        InspectionFeePostClosing = 73,
        InspectionFeePriorToClosing = 74,
        IntangibleTax = 75,
        MERSRegistrationFee = 76,
        MortgageInsuranceApplicationFee = 77,
        PayoffRequestFee = 78,
        RateLockFee = 79,
        ReconveyanceFee = 80,
        RecordingServiceFee = 81,
        SigningAgentFee = 82,
        SubescrowFee = 83,
        SubordinationFee = 84,
        SubordinationRecordingFee = 85,
        TieInFee = 86,
        TitleCourierFee = 87,
        TitleDocumentPreparationFee = 88,
        TitleEndorsementFee = 89,
        WarehousingFee = 90,
        WireTransferFee = 91,
        AccidentInsurancePremium = 92,
        CreditLifeInsurancePremium = 93,
        CreditPropertyInsurancePremium = 94,
        HealthInsurancePremium = 95,
        HighCostMortgagePreLoanCounselingFee = 96,
        LLPAFee = 97,
        LossOfIncomeInsurancePremium = 98,
        PrepaymentPenalty = 99,
        RealEstateCommissionBuyersBroker = 100,
        RealEstateCommissionSellersBroker = 101
    }
    public enum HUDCCLineNo2010
    {
        _801 = 801,
        _802 = 802,
        _804 = 804,
        _805 = 805,
        _806 = 806,
        _807 = 807,
        _808 = 808,
        _809 = 809,
        _810 = 810,
        _811 = 811,
        _812 = 812,
        _813 = 813,
        _814 = 814,
        _815 = 815,
        _816 = 816,
        _817 = 817,
        _818 = 818,
        _819 = 819,
        _820 = 820,
        _821 = 821,
        _822 = 822,
        _823 = 823,
        _824 = 824,
        _825 = 825,
        _826 = 826,
        _827 = 827,
        _828 = 828,
        _829 = 829,
        _830 = 830,
        _831 = 831,
        _832 = 832,
        _833 = 833,
        _834 = 834,
        _835 = 835,
        _836 = 836,
        _837 = 837,
        _838 = 838,
        _839 = 839,
        _840 = 840,
        _841 = 841,
        _842 = 842,
        _843 = 843,
        _844 = 844,
        _845 = 845,
        _846 = 846,
        _847 = 847,
        _848 = 848,
        _849 = 849,
        _850 = 850,
        _851 = 851,
        _852 = 852,
        _853 = 853,
        _854 = 854,
        _855 = 855,
        _856 = 856,
        _857 = 857,
        _858 = 858,
        _859 = 859,
        _860 = 860,
        _861 = 861,
        _862 = 862,
        _863 = 863,
        _864 = 864,
        _865 = 865,
        _866 = 866,
        _867 = 867,
        _868 = 868,
        _869 = 869,
        _870 = 870,
        _871 = 871,
        _872 = 872,
        _873 = 873,
        _874 = 874,
        _875 = 875,
        _876 = 876,
        _877 = 877,
        _878 = 878,
        _879 = 879,
        _880 = 880,
        _881 = 881,
        _882 = 882,
        _883 = 883,
        _884 = 884,
        _885 = 885,
        _886 = 886,
        _887 = 887,
        _888 = 888,
        _889 = 889,
        _890 = 890,
        _1101 = 1101,
        _1102 = 1102,
        _1103 = 1103,
        _1104 = 1104,
        _1109 = 1109,
        _1110 = 1110,
        _1111 = 1111,
        _1112 = 1112,
        _1113 = 1113,
        _1114 = 1114,
        _1115 = 1115,
        _1116 = 1116,
        _1117 = 1117,
        _1118 = 1118,
        _1119 = 1119,
        _1120 = 1120,
        _1121 = 1121,
        _1122 = 1122,
        _1123 = 1123,
        _1124 = 1124,
        _1125 = 1125,
        _1126 = 1126,
        _1127 = 1127,
        _1128 = 1128,
        _1129 = 1129,
        _1130 = 1130,
        _1201 = 1201,
        _1202 = 1202,
        _1203 = 1203,
        _1204 = 1204,
        _1205 = 1205,
        _1206 = 1206,
        _1207 = 1207,
        _1208 = 1208,
        _1209 = 1209,
        _1210 = 1210,
        _1211 = 1211,
        _1212 = 1212,
        _1213 = 1213,
        _1214 = 1214,
        _1215 = 1215,
        _1302 = 1302,
        _1303 = 1303,
        _1304 = 1304,
        _1305 = 1305,
        _1306 = 1306,
        _1307 = 1307,
        _1308 = 1308,
        _1309 = 1309,
        _1310 = 1310,
        _1311 = 1311,
        _1312 = 1312,
        _1313 = 1313,
        _1314 = 1314,
        _1315 = 1315,
        _1316 = 1316,
        _1317 = 1317,
        _1318 = 1318,
        _1319 = 1319,
        _1320 = 1320,
        _1321 = 1321,
        _1322 = 1322,
        _1323 = 1323,
        _1324 = 1324,
        _1325 = 1325,
        _1326 = 1326,
        _1327 = 1327
    }
    public enum GFEBlock
    {
        OurOriginationCharge = 1,
        CreditOrChargeForIntRate = 2,
        ServicesLenderSelected = 3,
        TitleServices = 4,
        OwnersTitleInsurance = 5,
        ServicesYouCanShopFor = 6,
        RecordingCharges = 7,
        TransferTaxes = 8,
        None = 99
    }
    public enum ResponsiblePartyType
    {
        Buyer = 0,
        Seller = 1
    }
    public enum PaidToType
    {
        Lender = 1,
        Broker = 2,
        Investor = 3,
        Other = 4,
        AffiliateOfLender = 5,
        AffiliateOfBroker = 6
    }
    public enum TRIDBlock
    {
        OriginationCharges = 1,
        ServicesYouCannotShopFor = 2,
        ServicesYouCanShopFor = 3,
        RecordingFeesAndOtherTaxes = 4,
        TransferTaxes = 5,
        Other = 6,
        PostClosing = 7,
        None = 99
    }
    #endregion
    public enum FollowUpFlag
    {
        NotAssigned = 0,
        RedFlag = 1,
        BlueFlag = 2,
        YellowFlag = 3,
        GreenFlag = 4,
        OrangeFlag = 5,
        PurpleFlag = 6
    }
    public enum AlertFlag
    {
        NotAssigned = 0,
        RedAlert = 1,
        YellowAlert = 2
    }

    #region Loan Model
    public enum MortgageType
    {
        NotAssigned = 0,
        VA = 1,
        FHA = 2,
        Conventional = 3,
        RHS = 4,
        Other = 5,
        HELOC = 6,
        StateAgency = 7,
        LocalAgency = 8
    }
    public enum LoanPurpose
    {
        NotAssigned = 0,
        Purchase = 1,
        Refinance = 2,
        Construction = 3,
        ConstructionPerm = 4,
        Second = 5,
        Third = 6,
        PurchaseMoneySecond = 7,
        Other = 8,
        PurchaseMoneyThird = 9
    }
    public enum RefiPurpAU
    {
        NotAssigned = 0,
        CashOutDebtConsolidation = 1,
        CashOutHomeImprovement = 2,
        CashOutLimited = 3,
        CashOutOther = 4,
        NoCashOutFHAStreamlinedRefinance = 5,
        NoCashOutFREOwnedRefinance = 6,
        NoCashOutOther = 7,
        NoCashOutStreamlinedRefinance = 8,
        ChangeInRateTerm = 9
    }
    public enum RefiTypeFHA
    {
        NotAssigned = 0,
        StreamlinedWithAppraisal = 1,
        StreamlinedWithoutAppraisal = 2,
        NoCashOut = 3,
        CashOut = 4,
        SimpleRefinance = 5
    }
    public enum RefiTypeVA
    {
        NotAssigned = 0,
        IRRR = 1,
        Other = 2
    }
    public enum AmortizationType
    {
        NotAssigned = 0,
        Fixed = 1,
        ARM = 2,
        GPM = 3,
        Other = 4
    }
    public enum BuyDownType
    {
        None = 0,
        _5_4_3_2_1 = 1,
        _4_3_2_1 = 2,
        _3_2_1 = 3,
        _2_1 = 4,
        _1_0 = 5,
        Compressed_5_4_3_2_1 = 6,
        Compressed_4_3_2_1 = 7,
        Compressed_3_2_1 = 8,
        Compressed_2_1 = 9,
        Compressed_1_0 = 10,
        HalfStepFrom5 = 11,
        HalfStepFrom4 = 12,
        HalfStepFrom3 = 13,
        HalfStepFrom2_5 = 14,
        HalfStepFrom2 = 15,
        HalfStepFrom1_5 = 16,
        HalfStepFrom1 = 17,
        HalfStepFrom0_5 = 18
    }
    public enum SubFiType
    {
        NotHELOC = 0,
        HELOC = 1
    }
    public enum ARMIndexType
    {
        NotAssigned = 0,
        EleventhDistrictCostOfFunds = 1,
        OneYearTreasury = 2,
        ThreeYearTreasury = 3,
        SixMonthTreasury = 4,
        DailyCDRate = 5,
        FannieMae60DayRequiredNetYield = 6,
        FannieMaeLIBOR = 7,
        FederalCostOfFunds = 8,
        FreddieMac60DayRequiredNetYield = 9,
        FreddieMacLIBOR = 10,
        LIBOR = 11,
        MonthlyAverageCMT = 12,
        NationalAverageContractRateFHLBB = 13,
        NationalMonthlyMedianCostOfFunds = 14,
        TreasuryBillDailyAverage = 15,
        WallStreetJournalLIBOR = 16,
        WeeklyAverageCDRate = 17,
        WeeklyAverageCMT = 18,
        WeeklyAveragePrimeRate = 19,
        WeeklyAverageSMTI = 20,
        WeeklyAverageTAABD = 21,
        WeeklyAverageTAAI = 22,
        Other = 23,
        OneMonthLIBOR = 24,
        ThreeMonthLIBOR = 25,
        SixMonthLIBOR = 26,
        OneYearLIBOR = 27,
        FiveYearTreasury = 28
    }
    public enum ARMRounding
    {
        NoRounding = 0,
        NearestEighth = 1,
        UpToNearestEighth = 2,
        DownToNearestEighth = 3,
        NearestQuarter = 4,
        UpToNearestQuarter = 5,
        DownToNearestQuarter = 6
    }
    public enum LoanProductType
    {
        Fixed = 0,
        ARM = 1,
        GPM = 2,
        NoPayment = 3,
        CustomProduct = 4
    }
    public enum FHAProgramType
    {
        NotAssigned = 0,
        _203b = 1,
        _203b2 = 2,
        _203b_251 = 3,
        _203k = 4,
        _203k_251 = 5,
        _221d2 = 6,
        _221d2_251 = 7,
        _234c = 8,
        _234c_251 = 9,
        _184 = 10,
        _247 = 11,
        _203h = 12
    }
    public enum MIMethod
    {
        MonthlyNonRefundable = 0,
        None = 1,
        LevelAnnual = 2,
        StandardAnnual = 3,
        SinglePremiumRefundable = 4,
        SinglePremiumNonRefundable = 5,
        LenderPaid = 7,
        SplitPremiumNonRefundable = 8,
        QMSinglePremium = 9,
        QMSplitPremium = 10,
        QMStandardAnnual = 11,
        MonthlyRefundable = 12,
        SplitPremiumRefundable = 13
    }
    public enum MIRefundedOrCredited
    {
        Refunded = 0,
        Credited = 1
    }
    public enum YSPWordingOption
    {
        MortgageBrokerFee = 0,
        PremiumReleaseFee = 1,
        YieldSpreadPremium = 2,
        Other = 3
    }
    public enum YSPValueOption
    {
        PercentRange = 0,
        PercentValue = 1,
        DollarRange = 2,
        DollarValue = 3
    }
    public enum TILStatus
    {
        NotAssigned = 0,
        Preliminary = 1,
        Final = 2,
        FinalWithEstimates = 3
    }
    public enum AssumptionOption
    {
        NotAssigned = 0,
        CannotAssume = 1,
        MayAssume = 2
    }
    public enum TILHazardInsOption
    {
        NotAssigned = 0,
        SpecificAmount = 1,
        ReplacementValue = 2,
        AmountOfLoan = 3
    }
    public enum IsIsNotNA
    {
        NotAssigned = 0,
        _Is = 1,
        IsNot = 2
    }
    public enum LateChargeBasis
    {
        NotAssigned = 0,
        OverDuePayment = 1,
        InterestPayment = 2,
        Payment = 3,
        PIPortion = 4,
        PaymentAndEscrows = 5,
        PIDue = 6
    }
    public enum LateChargeWording
    {
        Standard = 0,
        Custom = 1
    }
    public enum MayWillWillNot
    {
        NotAssigned = 0,
        May = 1,
        Will = 2,
        WillNot = 3
    }
    public enum MayWillNot
    {
        NotAssigned = 0,
        May = 1,
        WillNot = 2
    }
    public enum RepaymentType
    {
        NotAssigned = 0,
        InterestOnly = 1,
        NoNegativeAmortization = 2,
        PotentialNegativeAmortization = 3,
        ScheduledAmortization = 4,
        ScheduledNegativeAmortization = 5
    }
    public enum InterimIntDaysPerYearOV
    {
        NotAssigned = 0,
        _360 = 1,
        _365 = 2
    }
    public enum BDInterimIntCalcMethodOV
    {
        NotAssigned = 0,
        NoteRate = 1,
        BuydownStartRate = 2,
        FullyIndexedRate = 3
    }
    public enum InterimIntDecimalsOV
    {
        NotAssigned = 0,
        _2Decimals = 1,
        _3Decimals = 2,
        _4Decimals = 3
    }
    public enum LienPosition
    {
        NotAssigned = 0,
        First = 1,
        Second = 2,
        Third = 3
    }
    public enum LockGuaranteedByType
    {
        NotAssigned = 0,
        Lender = 1,
        Broker = 2
    }
    public enum HMDALoanPurpose
    {
        NotAssigned = 0,
        HomePurchase = 1,
        HomeImprovement = 2,
        Refinancing = 3
    }
    public enum HMDAPreapproval
    {
        NotAssigned = 0,
        PreapprovalWasRequested = 1,
        PreapprovalWasNotRequested = 2,
        NotApplicable = 3
    }
    public enum TypeOfPurchaser
    {
        LoanWasNotOriginatedOrWasNotSoldInCalendarYear = 0,
        FannieMae = 1,
        GinnieMae = 2,
        FreddieMac = 3,
        FarmerMac = 4,
        PrivateSecuritization = 5,
        CommercialBankOrSavingBankOrSavingsAssociation = 6,
        LifeInsuranceCompanyOrCreditUnionOrMortgageBankOrFinanceCompany = 7,
        AffiliateInstitution = 8,
        OtherTypeOfPurchaser = 9,
        CreditUnionMortgageCompanyOrFinanceCompany = 71,
        LifeInsuranceCompany = 72,
        NotAssigned = -1
    }
    public enum HOEPAStatus
    {
        NotAssigned = 0,
        HOEPA = 1,
        NotHOEPA = 2,
        NotApplicable = 3
    }
    public enum DenialReason
    {
        NotAssigned = 0,
        DebtToIncomeRatio = 1,
        EmploymentHistory = 2,
        CreditHistory = 3,
        Collateral = 4,
        InsufficientCash = 5,
        UnverifiableInformation = 6,
        CreditApplicationIncomplete = 7,
        MortgageInsuranceDenied = 8,
        Other = 9
    }
    public enum LienStatus
    {
        NotAssigned = 0,
        FirstLien = 1,
        SubordinateLien = 2,
        NotSecuredByLien = 3,
        NotApplicable = 4
    }
    public enum HMDALoanType
    {
        NotAssigned = 0,
        Conventional = 1,
        FHAInsured = 2,
        VAGuaranteed = 3,
        FmHAInsured = 4
    }
    public enum PaidByOtherType
    {
        Seller = 0,
        Lender = 1,
        ThirdParty = 2
    }
    public enum EntityOnTILOption
    {
        _Default = 0,
        ActiveCompany = 1,
        Lender = 2,
        Investor = 3
    }
    public enum HPMLTestResult
    {
        NotAssigned = 0,
        HPML = 1,
        NotHPML = 2,
        NotEvaluated = 3,
        Unknown = 4
    }
    public enum GFEPointsOption
    {
        YouPayACharge = 0,
        YouReceiveACredit = 1,
        IncludedInOurOriginationCharge = 2
    }
    public enum InterimIntDayCountCalcMethodOV
    {
        NotAssigned = 0,
        Actual = 1,
        _30 = 2
    }
    public enum SharedEquityOrAppreciationOption
    {
        Neither = 0,
        SharedEquity = 1,
        SharedAppreciation = 2
    }
    public enum ReverseMortgageType
    {
        NotAReverseMortgage = 0,
        HECM_Standard = 1,
        HECM_Saver = 2,
        OtherReverseMortgage = 3
    }
    public enum ReverseMortgageLoanPurpose
    {
        NotAPurchase = 0,
        Purchase = 1
    }
    public enum OtherCreditType
    {
        NotAssigned = 0,
        CashDepositOnSalesContract = 1,
        SellerCredit = 2,
        LenderCredit = 3,
        RelocationFunds = 4,
        EmployerAssistedHousing = 5,
        LeasePurchaseFunds = 6,
        BorrowerPaidFees = 7,
        Other = 8
    }
    public enum NMLSInvestorType
    {
        NotAssigned = 0,
        SoldToAgency = 1,
        SoldToOthersNonAffiliate = 2,
        SoldToOthersAffiliate = 3,
        KeptInPortfolio = 4,
        SoldWithSaleTreatment = 5,
        SoldWithoutSaleTreatment = 6
    }
    public enum BonaFideDiscountPointsIndicator
    {
        NotAssigned = 0,
        NotBonaFide = 1,
        BonaFideForStateTestsOnly = 2,
        BonaFideForGSETestsOnly = 3,
        BonaFideForStateAndGSETestsOnly = 4,
        BonaFideForFederalTestsOnly = 5,
        BodaFideForFederalAndStateTestsOnly = 6,
        BonaFideForFederalAndGSETestOnly = 7,
        BonaFideForAllTests = 8
    }
    public enum ARMQualRateOption
    {
        NotAssigned = 0,
        NoteRate = 1,
        MaxRateInFirst5Years = 2,
        RatePlus2OrFIR = 3,
        RateOrFIR = 4,
        RatePlus1 = 5
    }
    public enum ATRAssessmentMethod
    {
        NotAssigned = 0,
        ATRNotQM = 1,
        QMGeneral = 2,
        QMTemporaryGSE = 3,
        QMTemporaryAgency = 4,
        QMSmallCreditor = 5,
        QMTemporaryBalloon = 6,
        QMBalloon = 7,
        NoneNotCovered = 99
    }
    public enum QMTestResult
    {
        NotAssigned = 0,
        NotEvaluated = 1,
        Unknown = 2,
        Passed = 3,
        Failed = 4,
        PassedSafeHarbor = 5,
        PassedRebuttablePresumption = 6
    }
    public enum HCMTestResult
    {
        NotAssigned = 0,
        NotEvaluated = 1,
        Unknown = 2,
        Passed = 3,
        Failed = 4
    }
    public enum IntOnlyQualPaymentCalcOption
    {
        Default = 0,
        AmortizingOverFullTerm = 1,
        AmortizingOverNonInterestOnlyTerm = 2,
        InterestOnly = 3
    }
    public enum TRIDLoanPurpose
    {
        NotAssigned = 0,
        Purchase = 1,
        Refinance = 2,
        Construction = 3,
        HomeEquityLoan = 4
    }
    public enum TimeZoneCity
    {
        NotAssigned = 0,
        NewYork = 10,
        Chicago = 20,
        Boise = 30,
        Phoenix = 40,
        LosAngeles = 50,
        Anchorage = 60,
        Honolulu = 70,
        Bermuda = 80,
        StJohns = 90,
        Marquesas = 100,
        PagoPago = 110,
        Caracas = 120,
        Halifax = 130,
        BuenosAires = 140,
        SouthGeorgia = 150,
        Azores = 160,
        Reykjavik = 170,
        London = 180,
        Guam = 190,
        SanJuan = 200
    }
    public enum ConstructionPurpose
    {
        NotAssigned = 0,
        InitialConstruction = 1,
        HomeImprovement = 2
    }
    public enum MilitaryAPRTestResult
    {
        NotAssigned = 0,
        NotCovered = 1,
        Unknown = 2,
        Passed = 3,
        Failed = 4
    }
    public enum HMDALoanPurpose2
    {
        NotAssigned = 0,
        HomePurchase = 1,
        HomeImprovement = 2,
        Other = 4,
        NotApplicable = 5,
        Refinancing = 31,
        CashOutRefinancing = 32
    }
    public enum MIPFFFinancingCalcOption
    {
        Financed = 0,
        NotFinanced = 1
    }
    public enum ULDDARMConversionStatus
    {
        NotAssigned = 0,
        Active = 1,
        Exercised = 2,
        Expired = 3
    }
    #endregion
    #region Borrower Model
    public enum MaritalStatus
    {
        NotAssigned = 0,
        Married = 1,
        Separated = 2,
        Unmarried = 3
    }
    public enum Ethnicity
    {
        NotAssigned = 0,
        HispanicOrLatino = 1,
        NotHispanicOrLatino = 2,
        NotProvided = 3,
        NotApplicable = 4
    }
    public enum Ethnicity2
    {
        None = 0,
        IDoNotWishToFurnish = 1,
        HispanicOrLatino = 2,
        NotHispanicOrLatino = 4,
        Mexican = 8,
        PuertoRican = 16,
        Cuban = 32,
        OtherHispanicOrLatino = 64
    }
    public enum Gender
    {
        NotAssigned = 0,
        Female = 1,
        Male = 2,
        NotProvided = 3,
        NotApplicable = 4
    }
    public enum Gender2
    {
        None = 0,
        IDoNotWishToFurnish = 1,
        Female = 2,
        Male = 4
    }
    public enum YesNoNA
    {
        NotAssigned = 0,
        Yes = 1,
        No = 2
    }
    public enum DeclarationPropertyType
    {
        NotAssigned = 0,
        PRPrincipalResidence = 1,
        SHSecondHome = 2,
        IPInvestmentProperty = 3
    }
    public enum DeclarationTitleHeld
    {
        NotAssigned = 0,
        SSolely = 1,
        SPJointlyWithSpouse = 2,
        OJointlyWithAnotherPerson = 3
    }
    public enum CitizenResidencyType
    {
        NotAssigned = 0,
        USCitizen = 1,
        PermanentResidentAlien = 2,
        NonPermanentResidentAlien = 3,
        NonResidentAlien = 4, Unknown = 5
    }
    public enum CreditBureauBitFlag
    {
        BitFlagNone = 0,
        BitFlagEquifax = 1,
        BitFlagTransUnion = 2,
        BitFlagExperian = 4
    }
    public enum CreditBureau
    {
        NotAssigned = 0,
        Equifax = 1,
        TransUnion = 2,
        Experian = 3
    }
    public enum CounselingConfirmationType
    {
        NotAssigned = 0,
        GovernmentAgency = 1,
        HUDApprovedCounselingAgency = 2,
        LenderTrainedCounseling = 3,
        NoBorrowerCounseling = 4,
        BorrowerDidNotParticipate = 100,
        MortgageInsuranceCompany = 101,
        NonProfitOrganization = 102
    }

    public enum CounselingFormatType
    {
        NotAssigned = 0,
        BorrowerEducationNotRequired = 1,
        Classroom = 2,
        HomeStudy = 3,
        Individual = 4,
        BorrowerDidNotParticipate = 100
    }

    public enum LegalEntityType
    {
        NotAssigned = 0,
        Corporation = 1,
        GovernmentEntity = 2,
        JointVenture = 3,
        LimitedLiabilityCompany = 4,
        LimitedPartnership = 5,
        NonProfitCorporation = 6,
        Partnership = 8,
        LandTrustAndBeneficiaryIsIndividual = 100,
        LivingTrust = 101,
        IllinoisLandTrust = 102,
        LandTrust = 103,
        NativeAmericanTribeOrTribalOrganization = 104
    }
    public enum TaxpayerIdentifierType
    {
        SSN = 0,
        EIN = 1,
        ITIN = 2
    }
    public enum ForeclosureExplanation
    {
        NotAssigned = 0,
        Confirmed_CR_DIL = 1,
        Confirmed_CR_PFS = 2,
        Confirmed_CR_FC_Incorrect = 4,
        Confirmed_CR_FC_EC = 8,
        Confirmed_CR_BK_Incorrect = 16,
        Confirmed_CR_BK_EC = 32,
        Confirmed_Mtg_Del_Incorrect = 64
    }
    public enum VAOccupancyType
    {
        NotAssigned = 0,
        CurrentOccupant = 1,
        SpouseIsVeteranAndWillOccupy = 2,
        PreviousOccupant = 3,
        SpouseIsVeteranAndPrevOccupant = 4,
        ChildWillOccupy = 5,
        ChildOccupiedIRRL = 6
    }
    public enum PriceExceedingValueAwareness
    {
        NotAssigned = 0,
        AwareAtSigning = 1,
        NotAwareAtSigning = 2
    }
    public enum CDSignatureMethod
    {
        NotAssigned = 0,
        NoSignatureLine = 1,
        Wet = 2,
        Digital = 3,
        Image = 4,
        Text = 5,
        Other = 6
    }
    public enum SSAMatchResult
    {
        NotRun = 0,
        Match = 1,
        NoMatch = 2
    }
    public enum DemographicInfoProvidedMethod
    {
        NotAssigned = 0,
        FaceToFace = 1,
        Telephone = 2,
        FaxOrMail = 3,
        EmailOrInternet = 4
    }

    public enum Race2
    {
        None = 0,
        IDoNotWishToFurnish = 1,
        AmericanIndian = 2,
        Asian = 4,
        Black = 8,
        PacificIslander = 16,
        White = 32,
        AsianIndian = 64,
        Chinese = 128,
        Filipino = 256,
        Japanese = 512,
        Korean = 1024,
        Vietnamese = 2048,
        OtherAsian = 4096,
        NativeHawaiian = 8192,
        GuamanianOrChamorro = 16384,
        Samoan = 32768,
        OtherPacificIslander = 65536
    }
    public enum GMICompletionMethod
    {
        NotAssigned = 0,
        ProvidedByBorrower = 1,
        NotProvidedInMailTelephoneOrInternetApplication = 2,
        CollectedOnTheBasisOfVisualObservationOrSurname = 3
    }
    public enum MilitaryStatusType
    {
        NotAssigned = 0,
        ActiveDuty = 1,
        ReserveNationalGuardNeverActivated = 2,
        Veteran = 3
    }
    public enum LanguagePreferenceType
    {
        NotAssigned = 0,
        English = 1,
        Chinese = 2,
        Korean = 3,
        Spanish = 4,
        Tagalog = 5,
        Vietnamese = 6,
        Other = 7,
        IDoNotWishToRespond = 8
    }
    public enum DomesticRelationshipType
    {
        NotAssigned = 0,
        CivilUnion = 1,
        DomesticPartnership = 2,
        RegisteredReciprocalBeneficiaryRelationship = 3,
        Other = 4
    }
    public enum BankruptcyChapterType
    {
        NotAssigned = 0,
        ChapterSeven = 1,
        ChapterEleven = 2,
        ChapterTwelve = 4,
        ChapterThirteen = 8
    }
    #endregion
    #region Residence Model
    public enum LivingStatus
    {
        NotAssigned = 0,
        Own = 1,
        Rent = 2,
        LivingRentFree = 3
    }
    #endregion
    #region Employer Model
    public enum EmployerStatus
    {
        Former = 0,
        SecondaryCurrent = 1,
        Primary = 2
    }
    #endregion
    #region Income Model
    public enum IncomeType
    {
        NotAssigned = 0,
        BaseIncome = 1,
        Overtime = 2,
        Bonus = 3,
        Commission = 4,
        DividendInterest = 5,
        NetRentalIncome = 6,
        AlimonyChildSupport = 7,
        AutomobileExpenseAccount = 8,
        FosterCare = 9,
        NotesReceivableInstallment = 10,
        Pension = 11,
        SocialSecurity = 12,
        SubjectPropertyNetCashFlow = 13,
        Trust = 14,
        Unemployment = 15,
        PublicAssistance = 16,
        VABenefitsNonEducational = 17,
        MortgageDifferential = 18,
        MilitaryBasePay = 19,
        MilitaryRationsAllowance = 20,
        MilitaryFlightPay = 21,
        MilitaryHazardPay = 22,
        MilitaryClothesAllowance = 23,
        MilitaryQuartersAllowance = 24,
        MilitaryPropPay = 25,
        MilitaryOverseasPay = 26,
        MilitaryCombatPay = 27,
        MilitaryVariableHousingAllowance = 28,
        MortgageCreditCertificate = 29,
        TrailingCoBorrowerIncome = 30,
        Other = 31,
        BoarderIncome = 32,
        CapitalGains = 33,
        EmploymentRelatedAssets = 34,
        ForeignIncome = 35,
        RoyaltyPayment = 36,
        SeasonalIncome = 37,
        TemporaryLeave = 38,
        TipIncome = 39,
        Section8 = 40,
        NonBorHouseholdIncome = 41,
        AccessoryUnitIncomeOther = 42,
        Alimony = 43,
        ChildSupport = 44,
        ContractBasis = 45,
        DefinedContributionPlan = 46,
        Disability = 47,
        HousingAllowance = 48,
        SeparateMaintenance = 49
    }
    public enum IncomeFrequencyType
    {
        NotAssigned = 0,
        Yearly = 1,
        Monthly = 2,
        SemiMonthly = 3,
        BiWeekly = 4,
        Weekly = 5,
        Hourly = 6,
        Variable = 7,
        Other = 8
    }
    #endregion
    #region Asset Model
    public enum AssetType
    {
        NotAssigned = 0,
        Savings = 1,
        Checking = 2,
        CashDepositOnSalesContract = 3,
        GiftNotDeposited = 4,
        CertificateOfDeposit = 5,
        MoneyMarketFund = 6,
        MutualFunds = 7,
        Stocks = 8,
        Bonds = 9,
        SecuredBorrowedFundsNotDeposited = 10,
        BridgeLoanNotDeposited = 11,
        RetirementFunds = 12,
        NetWorthOfBusinessOwned = 13,
        TrustFunds = 14,
        OtherNonLiquidAsset = 15,
        OtherLiquidAsset = 16,
        NetProceedsFromSaleOfRealEstate = 17,
        NetEquity = 18,
        CashOnHand = 19,
        GiftOfEquity = 20,
        IndividualDevelopmentAccount = 21,
        LifeInsuranceCashValue = 22,
        ProceedsFromSaleOfNonRealEstateAsset = 23,
        SecuredBorrowedFunds = 24,
        StockOptions = 25,
        UnsecuredBorrowedFunds = 26
    }
    #endregion
    #region REO Model
    public enum REOStatus
    {
        NotAssigned = 0,
        Sold = 1,
        PendingSale = 2,
        Rental = 3,
        Retained = 4
    }
    public enum REOType
    {
        NotAssigned = 0,
        SingleFamily = 1,
        Condominium = 2,
        Townhouse = 3,
        Cooperative = 4,
        TwoToFourUnitProperty = 5,
        MultifamilyMoreThanFourUnits = 6,
        ManufacturedMobileHome = 7,
        CommercialNonResidential = 8,
        MixedUseResidential = 9,
        Farm = 10,
        HomeAndBusinessCombined = 11,
        Land = 12
    }
    public enum VAEntitlementRestoration
    {
        NotAssigned = 0,
        OneTimeRestorationToPurchase = 1,
        RegularCashOutRefi = 2,
        IRRRL = 3
    }
    #endregion
    #region Debt Model
    public enum DebtType
    {
        NotAssigned = 0,
        Revolving = 1,
        Installment = 2,
        Mortgage = 3,
        HELOC = 4,
        Liens = 5,
        LeasePayments = 6,
        Open = 7,
        Taxes = 8,
        Other = 9,
        TaxLien = 10
    }
    public enum DebtMortgageType
    {
        NotAssigned = 0,
        VA = 1,
        FHA = 2,
        Conventional = 3,
        RHS = 4,
        Other = 5
    }
    #endregion
    #region CreditAlias Model
    public enum CreditAliasType
    {
        CreditAliasAndAKA = 0,
        CreditAliasOnly = 1,
        AKAOnly = 2
    }
    #endregion
    #region SubProp Model
    public enum SubjectPropertyType
    {
        NotAssigned = 0,
        Detached = 1,
        Attached = 2,
        Condominium = 3,
        HighRiseCondo = 4,
        DetachedCondo = 5,
        PUD = 6,
        Cooperative = 7,
        Manufactured = 8,
        Manufactured_Condo_PUD_COOP = 9,
        ManufacturedSinglewide = 10,
        ManufacturedMultiwide = 11,
        Other = 12,
        VacantLand = 13,
        ManufacturedMHAdvantage = 14
    }
    public enum ImprovementsStatus
    {
        NotAssigned = 0,
        Made = 1,
        ToBeMade = 2
    }
    public enum EstateHeld
    {
        NotAssigned = 0,
        FeeSimple = 1,
        Leasehold = 2
    }
    public enum PropertyClass
    {
        NotAssigned = 0,
        A_III_Condo = 1,
        B_II_Condo = 2,
        C_I_Condo = 3,
        E_PUD = 4,
        F_PUD = 5,
        III_PUD = 6,
        _1_COOP = 7,
        _2_COOP = 8,
        FHA_VACondoOrSpotLoan = 9,
        PCondo = 10,
        QCondo = 11,
        RCondo = 12,
        SCondo = 13,
        TCondo = 14,
        UCondo = 15,
        StreamlinedReview = 16,
        EstablishedProject = 17,
        NewProject = 18,
        DetachedProject = 19,
        _2to4UnitProject = 20,
        ReciprocalReview = 21,
        VRefiPlus = 22,
        T_PUD = 23,
        T_COOP = 24,
        ExemptFromReview = 25,
        GNotInAProjectOrDevelopment = 999
    }
    public enum ProjectStatusType
    {
        NotAssigned = 0,
        Established = 1,
        _New = 2
    }
    public enum ProjectDesignType
    {
        NotAssigned = 0,
        GardenProject = 1,
        HighriseProject = 2,
        MidriseProject = 3,
        TownhouseRowhouse = 4,
        OtherSelectedOnValuationDocumentation = 5
    }
    public enum PropertyValuationMethod
    {
        NotAssigned = 0,
        AutomatedValuationModel = 1,
        DesktopAppraisal = 3,
        DriveBy = 4,
        FullAppraisal = 5,
        None = 6,
        PriorAppraisalUsed = 8,
        DeskReview = 10,
        FieldReview = 100
    }
    public enum AVMModelType
    {
        NotAssigned = 0,
        AutomatedPropertyService = 1,
        Casa = 2,
        FidelityHansen = 3,
        HomePriceAnalyzer = 4,
        HomePriceIndex = 5,
        HomeValueExplorer = 6,
        Indicator = 7,
        NetValue = 8,
        Pass = 10,
        PropertySurveyAnalysisReport = 11,
        ValueFinder = 12,
        ValuePoint = 13,
        ValuePoint4 = 14,
        ValuePointPlus = 15,
        ValueSure = 16,
        ValueWizard = 17,
        ValueWizardPlus = 18,
        VeroIndexPlus = 19,
        VeroValue = 20,
        MTM = 21,
        AVMax = 100,
        CAValue = 101,
        CollateralMarketValue = 102,
        FraudGuard = 103,
        FREAllowedAVM = 104,
        I_AVM = 105,
        IVal = 106,
        PowerBase6 = 107,
        RapidValue = 108,
        RealValue = 109,
        RealAssessment = 110,
        RealtorValuationModel = 111,
        Relar = 112,
        SiteXValue = 113,
        Vector = 114,
        Veros = 115,
        VeroValueAdvantage = 116,
        VeroValuePreferred = 117
    }
    public enum LoanOwnerType
    {
        NotAssigned = 0,
        Fannie = 1,
        Freddie = 2,
        Other = 3,
        Seller = 4,
        Unknown = 5
    }
    public enum UCDPFindingsStatus
    {
        NotAssigned = 0,
        Successful = 1,
        InProgress = 2,
        OverrideRequested = 4
    }
    public enum ULDDManufacturedWidthType
    {
        NotAssigned = 0,
        MultiWide = 1,
        SingleWide = 2
    }
    public enum ULDDPropertyValuationForm
    {
        NotAssigned = 0,
        AppraisalUpdateAndOrCompletionReport = 1,
        DesktopUnderwriterPropertyInspectionReport = 3,
        ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport = 5,
        ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport = 6,
        ExteriorOnlyInspectionResidentialAppraisalReport = 7,
        IndividualCondominiumUnitAppraisalReport = 10,
        IndividualCooperativeInterestAppraisalReport = 11,
        LoanProspectorConditionAndMarketability = 12,
        ManufacturedHomeAppraisalReport = 13,
        OneUnitResidentialAppraisalFieldReviewReport = 15,
        SmallResidentialIncomePropertyAppraisalReport = 17,
        TwoToFourUnitResidentialAppraisal = 18
    }
    public enum AppraisedValueStatus
    {
        NotAssigned = 0,
        Estimated = 1,
        Actual2
    }
    public enum LandLoanStatus
    {
        NotAssigned = 0,
        LandOwnedFreeAndClear = 1,
        ExistingLandLoanToBeRefinanced = 2,
        LandToBePurchasedWithLoanProceeds = 3
    }
    public enum TRIDAltImpRepOption
    {
        IncludeInAdjustmentsAndOtherCredits = 0,
        TreatAsPartOfPurchasePrice = 1,
        OmitFromCashToCloseCalc = 2
    }
    public enum FREAppraisalFormType
    {
        NotAssigned = 0,
        FNM1004FRE70 = 1,
        FNM1004BFRE439 = 2,
        FNM1004CFRE70B = 3,
        FNM1004DFRE442 = 4,
        FNM1073FRE465 = 6,
        FNM1075FRE466 = 7,
        FNM1025FRE72 = 8,
        FNM2000FRE1032 = 9,
        FNM2000AFRE1072 = 10,
        FNM2055FRE2055 = 11,
        FNM2065 = 12,
        FNM2075 = 13,
        FNM2090 = 14,
        FNM2095 = 15,
        FRE20 = 18,
        Other = 19
    }
    public enum ManufacturedHomeLandPropertyInterest
    {
        NotAssigned = 0,
        DirectOwnership = 1,
        IndirectOwnership = 2,
        PaidLeasehold = 3,
        UnpaidLeasehold = 4,
        NotApplicable = 5
    }
    public enum ManufacturedHomeSecuredPropertyType
    {
        NotAssigned = 0,
        ManufacturedHomeAndLand = 1,
        ManufacturedHomeAndNotLand = 2,
        NotApplicable = 3
    }
    public enum ManufacturedHomeCondition
    {
        NotAssigned = 0,
        New = 1,
        Used = 2
    }
    #endregion
    #region Expense Model
    public enum ExpenseType
    {
        NotAssigned = 0,
        Alimony = 1,
        ChildSupport = 2,
        SeparateMaintenance = 3,
        JobRelatedExpenses = 4,
        Other = 5
    }
    #endregion
    #region Gift Model
    public enum GiftAssetType
    {
        NotAssigned = 0,
        GiftOfCash = 1,
        GiftOfPropertyEquity = 2,
        Grant = 3
    }
    public enum GiftDepositedStatus
    {
        NotAssigned = 0,
        Deposited = 1,
        NotDeposited = 2
    }
    public enum GiftSource
    {
        NotAssigned = 0,
        CommunityNonProfit = 1,
        Employer = 2,
        FederalAgency = 3,
        LocalAgency = 4,
        NonParentRelative = 5,
        Other = 6,
        Parent = 7,
        Relative = 8,
        ReligiousNonProfit = 9,
        StateAgency = 10,
        UnmarriedPartner = 11,
        UnrelatedFriend = 12
    }
    #endregion
    #region Document Model (EmbeddedDoc)
    public enum Status
    {
        NotReviewed = 0,
        Approved = 1,
        Inactive = 2,
        Incomplete = 3,
        MissingPages = 4,
        Unacceptable = 5,
        Duplicate = 6,
        Illegible = 7,
        ReadyToShip = 8,
        Reviewed = 9
    }
    #endregion
}
