﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LOSConnector.Core.Service
{
    public static class Utility
    {
        /// <summary>
        /// delete file and folder
        /// </summary>
        /// <param name="deletePath"></param>
        /// <returns></returns>
        public static bool DeleteFileAndFolder(string deletePath)
        {
            bool isDelete = false;
            try
            {
                if (!string.IsNullOrEmpty(deletePath))
                {
                    if (Directory.Exists(deletePath))
                    {
                        Directory.Delete(deletePath, true);
                        isDelete = true;
                    }
                    if (File.Exists(deletePath))
                    {
                        File.Delete(deletePath);
                        isDelete = true;
                    }
                }
            }
            catch
            {
                isDelete = false;
            }
            return isDelete;
        }
    }
}
