﻿using LOSConnector.Core.Data.Model.AgileAPI;
using LOSConnector.Core.Data.Model.ByteProAPI.SearchLoan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOSConnector.Core.Service.ByteProAPI
{
    public interface IByteProEnterprise
    {
        public Task<string> SearchByStatus(LoanSearch loanSearch);
        public Task<string> GetAllLoanDocuments(string fileDataID);
        public Task<string> GetSingleDocument(string fileDataID, string documentID);
        public Task<string> GetLoanData(string fileDataID);
        public Task<string> AddDocument(string documentDetails);
        public Task<string> UpdateFormData(List<LOSMetadata> lOSMetadata);
    }
}
