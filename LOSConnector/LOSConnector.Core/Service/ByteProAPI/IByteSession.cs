﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Service.ByteProAPI
{
    public interface IByteSession
    {
        public string Auth();
        public string GetSession
        {
            get;
        }
    }
}
