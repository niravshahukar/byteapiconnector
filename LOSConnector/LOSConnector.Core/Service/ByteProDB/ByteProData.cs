﻿using LOSConnector.Core.Data.Model.ByteProDB;
using LOSConnector.Core.Data.Model.Settings;
using LOSConnector.Core.Extensions;
using LOSConnector.Core.Logging;
using LOSConnector.Core.Messages;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace LOSConnector.Core.Service.ByteProDB
{
    public class ByteProData
    {
        //private readonly ILogger<ByteProData> _logger;
        private ConnectorSettings ConnectorSettings { get; }
        public ByteProData(IOptions<ConnectorSettings> connectorSettings)
        {
            //_logger = logger;
            ConnectorSettings = connectorSettings.Value;
        }
        /// <summary>
        /// Update byte document status
        /// </summary>
        /// <param name="embeddedDocID"></param>
        /// <param name="statusId"></param>
        /// <param name="updateSourceFile"></param>
        /// <returns></returns>
        public int UpdateFileDocumentStatus(string embeddedDocID, int statusId, bool updateSourceFile)
        {
            int result = 0;
            //_logger.LogTrace(AppResource.StartToUpdateEmbadedDocsTableforDocStatus + ": " + statusId);
            try
            {
                if (updateSourceFile)
                {
                    using (SqlConnection con = new SqlConnection(ConnectorSettings.OtherConfig.ByteProDB))
                    {
                        SqlCommand cmd = new SqlCommand(AppConstant.AC_usp_updateStatus_unprocessed_documents, con)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        cmd.Connection.Open();
                        cmd.Parameters.AddWithValue(AppConstant.AC_At_EmbeddedDocID, embeddedDocID);
                        cmd.Parameters.AddWithValue(AppConstant.AC_At_Status, statusId);
                        result = (int)cmd.ExecuteScalar();
                        cmd.Connection.Close();
                        //_logger.LogTrace(AppResource.CompleteToUpdateStatusInembededDocTableToIncomplete);
                    };
                }
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, "Failed to update byte document status.");
                result = 0;
            }
            return result;
        }
        /// <summary>
        /// Get all Not Reviewed based on loan status
        /// </summary>
        /// <param name="loanStatus"></param>
        /// <returns></returns>
        public List<ByteDocumentDetails> GetByteDocument(List<string> loanStatus)
        {
            List<ByteDocumentDetails> byteDocumentList = new List<ByteDocumentDetails>();
            try
            {
                var strStatus = String.Join(",", loanStatus);
                using (SqlConnection con = new SqlConnection(ConnectorSettings.OtherConfig.ByteProDB))
                {
                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand(AppConstant.AC_usp_get_unprocessed_documents, con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@LoanStatus", strStatus);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    byteDocumentList = dt.ToList<ByteDocumentDetails>();
                }
                return byteDocumentList;
            }
            catch
            {
                return byteDocumentList;
            }
        }
    }
}
