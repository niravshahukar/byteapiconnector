﻿using Ionic.Zip;
using LOSConnector.Core.Data.Entity;
using LOSConnector.Core.Data.Model.AgileAPI;
using LOSConnector.Core.Data.Model.ByteProAPI.Document;
using LOSConnector.Core.Data.Model.ByteProAPI.LoanData;
using LOSConnector.Core.Data.Model.ByteProAPI.SearchLoan;
using LOSConnector.Core.Data.Model.ByteProDB;
using LOSConnector.Core.Data.Model.Settings;
using LOSConnector.Core.Data.Repository;
using LOSConnector.Core.Extensions;
using LOSConnector.Core.Logging;
using LOSConnector.Core.Messages;
using LOSConnector.Core.Service.AgileAPI;
using LOSConnector.Core.Service.ByteProAPI;
using LOSConnector.Core.Service.ByteProDB;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOSConnector.Core.Service.Download
{
    public class DownloadLoanDocument : IDownloadLoanDocument
    {
        private readonly ILogger<DownloadLoanDocument> _logger;
        private readonly IAgileMortgage _iAgileMortgage;
        private readonly IByteProEnterprise _iByteProEnterprise;
        private readonly IGenericRepository<LOSBatchs> _repoBatch;
        private readonly IGenericRepository<LOSBatchDetails> _repoLOSBatchDetail;
        private readonly IGenericRepository<BotConfiguration> _repoBotConfiguration;
        private readonly IGenericRepository<FieldMapping> _repoFieldMapping;
        private readonly IGenericRepository<BotFieldMapping> _repoBotFieldMapping;
        private readonly List<string> _downloadDocumentStatus;
        private ByteProData byteProData;
        private ConnectorSettings ConnectorSettings { get; }
        public DownloadLoanDocument(ILogger<DownloadLoanDocument> logger, IByteProEnterprise iByteProEnterprise,
            IAgileMortgage iAgileMortgage, IGenericRepository<LOSBatchs> repoBatch, IGenericRepository<BotConfiguration> repoBotConfiguration,
            IGenericRepository<LOSBatchDetails> repoLOSBatchDetail, IGenericRepository<FieldMapping> repoFieldMapping,
            IGenericRepository<BotFieldMapping> repoBotFieldMapping, IOptions<ConnectorSettings> connectorSettings)
        {
            _logger = logger;
            _iByteProEnterprise = iByteProEnterprise;
            _iAgileMortgage = iAgileMortgage;
            _repoBatch = repoBatch;
            _repoLOSBatchDetail = repoLOSBatchDetail;
            ConnectorSettings = connectorSettings.Value;
            _repoBotConfiguration = repoBotConfiguration;
            _repoFieldMapping = repoFieldMapping;
            _repoBotFieldMapping = repoBotFieldMapping;
            _downloadDocumentStatus = AppHelper.GetAppSettingsKeyValue<string>(ConnectorSettings.ByteProAPIConfig.DocumentStatus);
            byteProData = new ByteProData(connectorSettings);
        }
        /// <summary>
        /// Download document and loan data
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseMessage> DownloadByteDocumentAndData()
        {
            try
            {
                var newDocument = string.Empty;
                var enumLoanStatus = GetEnumValues(typeof(Enum.LoanStatus));
                var botConfiguration = await _repoBotConfiguration.GetAll(b => b.IsActive);
                List<string> statusList = new List<string>();
                botConfiguration.ToList().ForEach(s =>
                {
                    s.LoanMilestone.Split(AppConstant.AC_Comma).ToList().ForEach(r =>
                    {
                        var status = r.Trim().Replace(AppConstant.AC_Space, string.Empty).ToLower();
                        if (!statusList.Exists(e => e == status))
                        {
                            statusList.Add(status);
                        }
                    });
                });
                var loanStatusList = enumLoanStatus.Where(s => statusList.Any(b => b == s.Value.ToLower())).Select(s => s.Key).ToList();
                #region Download document using stored procedure 
                //Get all Not Reviewed documents from stored procedure 
                var documents = byteProData.GetByteDocument(loanStatusList);
                //Download documents
                foreach (var documentDetails in documents)
                {
                    newDocument += await DownloadDocuments(documentDetails) + AppConstant.AC_Space;
                }
                #endregion
                #region Download documents using byte API
                ////Create search loan body
                //LoanSearch loanSearch = LoanSearchBody(loanStatusList);
                //var searchLoans = await _iByteProEnterprise.SearchByStatus(loanSearch);
                //var loans = ParseJsonString<LoanSearch>(searchLoans) ?? new LoanSearch();
                ////Download document using byte API.Byte API does not have to update document status
                //foreach (var row in loans.SearchRows)
                //{
                //    var fileDataID = row.SearchValues[0].FieldValue.Replace(AppConstant.AC_String_Comma, string.Empty);
                //    var loanFileName = row.SearchValues[1].FieldValue;
                //    var loanStatus = row.SearchValues[2].FieldValue;
                //    newDocument += await DownloadDocument(fileDataID, loanFileName, loanStatus) + AppConstant.AC_Space;
                //}
                #endregion
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(string.IsNullOrEmpty(newDocument.Trim()) ? AppResource.ConfiguredStatusDocumentsNotFound : newDocument, System.Text.Encoding.UTF8, AppResource.ApplicationJson)
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, AppResource.DownloadDocumentProcessFailed);
                throw ex;
            }
        }
        /// <summary>
        /// Download Not Reviewed status document and update document status
        /// Byte API does not have to update document status, uspdate document status using stored procedure  
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        private async Task<string> DownloadDocuments(ByteDocumentDetails document)
        {
            LOSBatchs lOSBatch = null;
            var newDocument = string.Empty;
            var fileDataID = Convert.ToString(document.FileDataID);
            var loanFileName = document.FileName;
            var loanStatus = Convert.ToString(document.LoanStatus);
            try
            {
                _logger.LogTrace("New document found for loan# " + loanFileName);
                Document documentDetails = ParseJsonString<Document>(await _iByteProEnterprise.GetSingleDocument(fileDataID, Convert.ToString(document.EmbeddedDocID)));
                lOSBatch = CreateLOSBatchObject(ConnectorSettings.ByteProAPIConfig.UserName);
                await _repoBatch.Create(lOSBatch);
                var zipFileName = DownloadDocumentCreateZip(documentDetails);
                if (zipFileName != null)
                {
                    _logger.LogTrace("Document downloaded and zip completed for loan# " + loanFileName);
                    var loanData = await GetLoanData(fileDataID, loanFileName, loanStatus);
                    //First Create the Package
                    dynamic packageInfo = _iAgileMortgage.CreatePackage(loanStatus, loanFileName, loanData);
                    if (packageInfo != null)
                    {
                        //check if the createdate is more than 60 seconds from now if so we submitted duplicate and so need to do an updateMetaData
                        DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                        var CreatedDate = unixStart.AddMilliseconds(packageInfo.createDate);
                        if (((DateTime.UtcNow - unixStart).TotalMilliseconds - packageInfo.createDate) / 1000 > 60)
                        {
                            _iAgileMortgage.UpdatePackageMetadata(packageInfo.packageName, loanStatus, loanFileName, loanData);
                        }
                        //now create the Leapfrog Batch                                
                        if (_iAgileMortgage.UploadBatch(packageInfo.packageGuid, Path.Combine(ConnectorSettings.FolderConfig.DownloadDocPath, zipFileName)))
                        {
                            LOSBatchDetails lOSBatchDetails = CreateLOSBatchDetailObject(lOSBatch.LOSBatchId, documentDetails, loanFileName);
                            await _repoLOSBatchDetail.Create(lOSBatchDetails);
                            lOSBatch.LOSBatchFileName = zipFileName;
                            await _repoBatch.Update(lOSBatch);
                            //byteProData.UpdateFileDocumentStatus(Convert.ToString(document.EmbeddedDocID), 3, true);
                            newDocument = newDocument + documentDetails.DocumentName + AppConstant.AC_String_Comma + AppConstant.AC_Space;
                        }
                        else
                        {
                            //mark batch as error
                            await _repoBatch.Delete(lOSBatch.LOSBatchId);
                            if (File.Exists(Path.Combine(ConnectorSettings.FolderConfig.DownloadDocPath, zipFileName)))
                            {
                                File.Delete(Path.Combine(ConnectorSettings.FolderConfig.DownloadDocPath, zipFileName));
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Create package process failed.");
                    }
                }
                if (!string.IsNullOrEmpty(newDocument))
                {
                    newDocument = newDocument + AppResource.IsDownloaded + " from loan# " + loanFileName + AppConstant.AC_DOT;
                    _logger.LogTrace("Process start download document is completed for loan# " + loanFileName);
                }
                return newDocument;
            }
            catch (Exception ex)
            {
                if (lOSBatch != null)
                {
                    await _repoBatch.Delete(lOSBatch.LOSBatchId);
                }
                throw ex;
            }
        }
        /// <summary>
        /// Get all documents using fileDataID and compare in DB for download configured status document
        /// Byte API does not have to update document status.
        /// Use byte API get all document and iterate each document
        /// </summary>
        /// <param name="fileDataID"></param>
        /// <param name="loanFileName"></param>
        /// <param name="loanStatus"></param>
        /// <returns></returns>
        private async Task<string> DownloadDocument(string fileDataID, string loanFileName, string loanStatus)
        {
            LOSBatchs lOSBatch = null;
            var newDocument = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(fileDataID))
                {
                    _logger.LogTrace("Process start download document for loan# " + loanFileName);
                    var allDocResponse = await _iByteProEnterprise.GetAllLoanDocuments(fileDataID);
                    List<Document> allDocuments = ParseJsonArray<List<Document>>(allDocResponse) ?? new List<Document>();
                    foreach (var document in allDocuments)
                    {
                        //Go to database verify is prev doc?
                        var docExist = await _repoLOSBatchDetail.ExistAsync(s => s.LOSFileUniqueId == Convert.ToString(document.DocumentID));
                        if (_downloadDocumentStatus.Exists(d => d == Convert.ToString(document.DocumentStatus)) && !docExist)
                        {
                            _logger.LogTrace("New document found for loan# " + loanFileName);
                            Document documentDetails = ParseJsonString<Document>(await _iByteProEnterprise.GetSingleDocument(fileDataID, Convert.ToString(document.DocumentID)));
                            lOSBatch = CreateLOSBatchObject(ConnectorSettings.ByteProAPIConfig.UserName);
                            await _repoBatch.Create(lOSBatch);
                            var zipFileName = DownloadDocumentCreateZip(documentDetails);
                            if (zipFileName != null)
                            {
                                _logger.LogTrace("Document downloaded and zip completed for loan# " + loanFileName);
                                var loanData = await GetLoanData(fileDataID, loanFileName, loanStatus);
                                //First Create the Package
                                dynamic packageInfo = _iAgileMortgage.CreatePackage(loanStatus, loanFileName, loanData);
                                if (packageInfo != null)
                                {
                                    //check if the createdate is more than 60 seconds from now if so we submitted duplicate and so need to do an updateMetaData
                                    DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                                    var CreatedDate = unixStart.AddMilliseconds(packageInfo.createDate);
                                    if (((DateTime.UtcNow - unixStart).TotalMilliseconds - packageInfo.createDate) / 1000 > 60)
                                    {
                                        _iAgileMortgage.UpdatePackageMetadata(packageInfo.packageName, loanStatus, loanFileName, loanData);
                                    }
                                    //now create the Leapfrog Batch                                
                                    if (_iAgileMortgage.UploadBatch(packageInfo.packageGuid, Path.Combine(ConnectorSettings.FolderConfig.DownloadDocPath, zipFileName)))
                                    {
                                        LOSBatchDetails lOSBatchDetails = CreateLOSBatchDetailObject(lOSBatch.LOSBatchId, documentDetails, loanFileName);
                                        await _repoLOSBatchDetail.Create(lOSBatchDetails);
                                        lOSBatch.LOSBatchFileName = zipFileName;
                                        await _repoBatch.Update(lOSBatch);
                                        newDocument = newDocument + documentDetails.DocumentName + AppConstant.AC_String_Comma + AppConstant.AC_Space;
                                    }
                                    else
                                    {
                                        //mark batch as error
                                        await _repoBatch.Delete(lOSBatch.LOSBatchId);
                                        if (File.Exists(Path.Combine(ConnectorSettings.FolderConfig.DownloadDocPath, zipFileName)))
                                        {
                                            File.Delete(Path.Combine(ConnectorSettings.FolderConfig.DownloadDocPath, zipFileName));
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception("Create package process failed.");
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(newDocument))
                    {
                        newDocument = newDocument + AppResource.IsDownloaded + " from loan# " + loanFileName + AppConstant.AC_DOT;
                        _logger.LogTrace("Process start download document is completed for loan# " + loanFileName);
                    }
                }
                return newDocument;
            }
            catch (Exception ex)
            {
                if (lOSBatch != null)
                {
                    await _repoBatch.Delete(lOSBatch.LOSBatchId);
                }
                throw ex;
            }
        }
        /// <summary>
        /// Create loan search body
        /// </summary>
        /// <param name="loanStatus"></param>
        /// <returns></returns>
        private LoanSearch LoanSearchBody(List<string> loanStatus)
        {
            try
            {
                LoanSearch loanSearch = new LoanSearch
                {
                    SearchExportCSV = false,
                    SearchDisplayHeaders = true,
                    ShowDetailRecords = false
                };
                List<SearchField> searchFields = new List<SearchField>
                {
                    CreateSearchFieldObj(AppConstant.AC_FileDataID, AppConstant.AC_Status),
                    CreateSearchFieldObj(AppConstant.AC_FileName, AppConstant.AC_FileData),
                    CreateSearchFieldFilterObj(AppConstant.AC_LoanStatus, AppConstant.AC_Status, loanStatus)
                };
                loanSearch.SearchFields = searchFields;
                return loanSearch;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Download loan data from byte los and create metadata list
        /// </summary>
        /// <param name="fileDataID"></param>
        /// <param name="loanFileName"></param>
        /// <param name="loanStatus"></param>
        /// <returns></returns>
        private async Task<List<LOSMetadata>> GetLoanData(string fileDataID, string loanFileName, string loanStatus)
        {
            try
            {
                _logger.LogTrace("Process start download loan data for loan# " + loanFileName);
                var loanData = await _iByteProEnterprise.GetLoanData(fileDataID);
                var fieldDetails = _repoBotConfiguration.IQueryableGetMany(bot => bot.IsActive).ToList().Where(s => AppHelper.GetAppSettingsKeyValue<string>(s.LoanMilestone).Exists(e => e == loanStatus))
                 .Join(_repoBotFieldMapping.IQueryableGetMany(), botConfig => botConfig.BotID, botFieldMapping => botFieldMapping.BotID, (botConfig, botFieldMapping) => new { botConfig, botFieldMapping })
                 .Join(_repoFieldMapping.IQueryableGetMany(fieldMap => fieldMap.ExportFromLOS && fieldMap.IsActive), botConfigFieldMapping => botConfigFieldMapping.botFieldMapping.MappingID, fieldMap => fieldMap.MappingID, (botConfig, fieldMap) => new FieldMapping
                 {
                     BorrowerFieldValue = fieldMap.BorrowerFieldValue,
                     ClientFieldName = fieldMap.ClientFieldName,
                     LOSFieldName = fieldMap.LOSFieldName,
                     LOSFieldNameAlias = fieldMap.LOSFieldNameAlias
                 }).AsQueryable<FieldMapping>().Distinct().ToList();//Get Active Bot field IDs
                var loanDataObj = ParseJsonString<ByteLoanData>(loanData) ?? new ByteLoanData();
                JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
                {
                    Converters = new List<Newtonsoft.Json.JsonConverter>() { new Newtonsoft.Json.Converters.StringEnumConverter() }
                };
                loanData = Newtonsoft.Json.JsonConvert.SerializeObject(loanDataObj);
                return CreateMetadata(fileDataID, loanFileName, loanData, fieldDetails);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Create loan metadata list for configured los field Ids.
        /// </summary>
        /// <param name="fileDataID"></param>
        /// <param name="loanFileName"></param>
        /// <param name="loanData"></param>
        /// <param name="fieldDetails"></param>
        /// <returns></returns>
        private List<LOSMetadata> CreateMetadata(string fileDataID, string loanFileName, string loanData, List<FieldMapping> fieldDetails)
        {
            try
            {
                List<LOSMetadata> lOSMetadataList = new List<LOSMetadata>();
                //Get mapped data                
                fieldDetails.ForEach(f =>
                {
                    JObject.Parse(loanData)
                           .Descendants()
                           .Where(jt => jt.Type == JTokenType.Property && ((JProperty)jt).Value.HasValues)
                           .Cast<JProperty>()
                           .Select(prop =>
                           {
                               string[] dataRowList = f.LOSFieldName.Split('.');
                               string tableName = dataRowList[0];
                               string fieldName = dataRowList[1];
                               var fieldVal = string.Empty;
                               JProperty fieldID;
                               if (prop.Name == "Parties" && tableName == "Party")
                               {
                                   var partyTypeValue = dataRowList[2];
                                   for (int i = 0; i < prop.Value.Count(); i++)
                                   {
                                       var partyTypeID = prop.Value[i].Children<JProperty>()
                                                 .Where(jp => jp.Value.Type != JTokenType.Object && jp.Value.Type != JTokenType.Array)
                                                 .Where(v => v.Name == "CategoryID").FirstOrDefault();
                                       fieldID = prop.Value[i].Children<JProperty>()
                                                .Where(jp => jp.Value.Type != JTokenType.Object && jp.Value.Type != JTokenType.Array)
                                                .Where(v => v.Name == fieldName).FirstOrDefault();
                                       if ((partyTypeID != null && partyTypeID.Count > 0) && (fieldID != null && fieldID.Count > 0))
                                       {
                                           if (Convert.ToString(partyTypeID?.Value) == partyTypeValue.Replace(AppConstant.AC_Space, string.Empty))
                                           {
                                               fieldVal = Convert.ToString(fieldID?.Value);
                                               if (!lOSMetadataList.Exists(e => (!string.IsNullOrEmpty(e.FieldNameAlias) && e.FieldNameAlias.ToLower() == f.LOSFieldNameAlias.ToLower())))
                                               {
                                                   lOSMetadataList.Add(new LOSMetadata
                                                   {
                                                       FieldFor = f.BorrowerFieldValue,
                                                       LOSFieldID = f.LOSFieldName,
                                                       FieldNameAlias = f.LOSFieldNameAlias,//LOSFieldNameAlias
                                                       FieldValue = fieldVal
                                                   });
                                               }
                                           }
                                       }
                                   }
                               }
                               else if (prop.Name == tableName || prop.Name.Remove(prop.Name.Length - 1, 1) == tableName)
                               {
                                   if (prop.Value.Type == JTokenType.Array)
                                   {
                                       var parentName = prop.Ancestors()
                                               .Where(jt => jt.Type == JTokenType.Property)
                                               .Select(jt => ((JProperty)jt).Name)
                                               .FirstOrDefault();
                                       if (!string.IsNullOrEmpty(parentName) && parentName.ToLower() == f.BorrowerFieldValue.ToLower())
                                       {
                                           for (int i = 0; i < prop.Value.Count(); i++)
                                           {
                                               fieldID = prop.Value[i].Children<JProperty>()
                                                        .Where(jp => jp.Value.Type != JTokenType.Object && jp.Value.Type != JTokenType.Array)
                                                        .Where(v => v.Name == fieldName).FirstOrDefault();
                                               if (fieldID != null && fieldID.Count > 0)
                                               {
                                                   fieldVal = Convert.ToString(fieldID?.Value);
                                                   if (!lOSMetadataList.Exists(e => (!string.IsNullOrEmpty(e.FieldNameAlias) && e.FieldNameAlias.ToLower() == f.LOSFieldNameAlias.ToLower())))
                                                   {
                                                       lOSMetadataList.Add(new LOSMetadata
                                                       {
                                                           FieldFor = f.BorrowerFieldValue,
                                                           LOSFieldID = f.LOSFieldName,
                                                           FieldNameAlias = f.LOSFieldNameAlias,//LOSFieldNameAlias
                                                           FieldValue = fieldVal
                                                       });
                                                   }
                                                   // + "_" + prop.Path.Split('.')[0].Split('[')[1].Replace("]", null) + "_" + i
                                               }
                                           }
                                       }
                                   }
                                   else
                                   {
                                       fieldID = prop.Value.Children<JProperty>()
                                         .Where(jp => jp.Value.Type != JTokenType.Object && jp.Value.Type != JTokenType.Array)
                                         .Where(v => v.Name == fieldName).FirstOrDefault();
                                       if (fieldID != null && fieldID.Count > 0)
                                       {
                                           fieldVal = Convert.ToString(fieldID?.Value);
                                           var parentName = prop.Ancestors()
                                                       .Where(jt => jt.Type == JTokenType.Property)
                                                       .Select(jt => ((JProperty)jt).Name)
                                                       .FirstOrDefault();
                                           if (!string.IsNullOrEmpty(parentName) && parentName.ToLower() == f.BorrowerFieldValue.ToLower())
                                           {
                                               if (!lOSMetadataList.Exists(e => (!string.IsNullOrEmpty(e.FieldNameAlias) && e.FieldNameAlias.ToLower() == f.LOSFieldNameAlias.ToLower())))
                                               {
                                                   lOSMetadataList.Add(new LOSMetadata
                                                   {
                                                       FieldFor = f.BorrowerFieldValue,
                                                       LOSFieldID = f.LOSFieldName,
                                                       FieldNameAlias = f.LOSFieldNameAlias,//LOSFieldNameAlias
                                                       FieldValue = fieldVal
                                                   });
                                                   // + "_" + prop.Path.Split('.')[0].Split('[')[1].Replace("]", null)
                                               }
                                           }
                                           else if (string.IsNullOrEmpty(f.BorrowerFieldValue))
                                           {
                                               lOSMetadataList.Add(new LOSMetadata
                                               {
                                                   FieldFor = f.BorrowerFieldValue,
                                                   LOSFieldID = f.LOSFieldName,
                                                   FieldNameAlias = f.LOSFieldNameAlias,//LOSFieldNameAlias
                                                   FieldValue = fieldVal
                                               });
                                           }
                                       }
                                   }
                               }
                               return true;
                           }).ToList();
                });
                _logger.LogTrace("Process complete download loan data for loan# " + loanFileName);
                lOSMetadataList.Add(new LOSMetadata { LOSEntityID = loanFileName, LOSFieldID = AppConstant.AC_FileDataID, FieldNameAlias = AppConstant.AC_LoanUniqueId, FieldValue = Convert.ToString(fileDataID) });
                return lOSMetadataList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Create search field filter Object
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="tableName"></param>
        /// <param name="loanStatus"></param>
        /// <returns></returns>
        private static SearchField CreateSearchFieldFilterObj(string fieldName, string tableName, List<string> loanStatus)
        {
            try
            {
                FieldFilter fieldFilter = new FieldFilter
                {
                    FilterList = loanStatus,
                    FilterType = 10
                };
                return new SearchField
                {
                    FieldName = fieldName,
                    TableName = tableName,
                    FieldFilter = fieldFilter
                };
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Create search field object
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private static SearchField CreateSearchFieldObj(string fieldName, string tableName)
        {
            try
            {
                return new SearchField
                {
                    FieldName = fieldName,
                    TableName = tableName
                };
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Create PDF from base64 and call create zip functionality 
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        private string DownloadDocumentCreateZip(Document document)
        {
            try
            {
                var folderGUID = Guid.NewGuid();
                var downloadDir = ConnectorSettings.FolderConfig.DownloadDocPath;
                string destFolderName = Convert.ToString(document.FileDataID) + AppConstant.AC_String_Underscore + document.DocumentID + AppConstant.AC_String_Underscore + folderGUID;
                var targetDir = Path.Combine(downloadDir, destFolderName);
                if (!Directory.Exists(targetDir))
                {
                    Directory.CreateDirectory(targetDir);
                }
                byte[] bytes = Convert.FromBase64String(document.DocumentData.Replace("\"", string.Empty));
                File.WriteAllBytes(targetDir + AppConstant.AC_String_Forward_Slash + document.DocumentID + AppConstant.AC_String_Underscore + document.DocumentName + AppConstant.AC_DOT_Pdf, bytes);
                return CreateZip(downloadDir, destFolderName, targetDir);
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Create zip file with downloaded documents
        /// </summary>
        /// <param name="zipFilePath"></param>
        /// <param name="destFolderName"></param>
        /// <param name="targetDir"></param>
        /// <returns></returns>
        private static string CreateZip(string zipFilePath, string destFolderName, string targetDir)
        {
            try
            {
                var zipFileName = destFolderName + AppConstant.AC_DOT_Zip;
                using (var zipFile = new ZipFile())
                {
                    //zipFile.AddEntry(AppSettingsKey.ImportJson, json);
                    zipFile.AddDirectory(targetDir.ToString());
                    zipFile.Save(Path.Combine(zipFilePath, zipFileName));
                }
                Directory.Delete(targetDir.ToString(), true);
                return zipFileName;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Create a list with Enum key and value
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public List<(string Key, string Value)> GetEnumValues(Type enumType)
        {
            try
            {
                if (!typeof(System.Enum).IsAssignableFrom(enumType))
                    throw new ArgumentException(AppResource.EnumTypeShouldDescribeEnum);
                Array names = System.Enum.GetNames(enumType);
                Array values = System.Enum.GetValues(enumType);
                List<(string Key, string Value)> result = new List<(string Key, string Value)>(capacity: names.Length);
                for (int i = 0; i < names.Length; i++)
                {
                    result.Add((Convert.ToString((int)values.GetValue(i)), names.GetValue(i).ToString()));
                }
                return result;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Create class object from json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public T ParseJsonString<T>(string jsonData) where T : new()
        {
            _ = new T();
            if (jsonData == null || jsonData.Length == 0)
                return default;
            T obj;
            try
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonData);
            }
            catch
            {
                throw;
            }
            return obj;
        }
        /// <summary>
        /// Create class object from json array
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public static T ParseJsonArray<T>(string jsonData) where T : class, new()
        {
            _ = Activator.CreateInstance<T>();
            if (jsonData == null || jsonData.Length == 0)
                return default;
            T obj;
            try
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonData);
            }
            catch
            {
                throw;
            }
            return obj;
        }
        /// <summary>
        /// Create new batch object
        /// </summary>
        /// <returns></returns>
        public LOSBatchs CreateLOSBatchObject(string userID)
        {
            try
            {
                return new LOSBatchs
                {
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = userID
                };
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Create LOSBatchDetail object
        /// </summary>
        /// <param name="lOSBatchId"></param>
        /// <param name="document"></param>
        /// <param name="loanFileName"></param>
        /// <returns></returns>
        public LOSBatchDetails CreateLOSBatchDetailObject(long? lOSBatchId, Document document, string loanFileName)
        {
            try
            {
                return new LOSBatchDetails()
                {
                    LOSBatchId = lOSBatchId,
                    LOSFileName = document.DocumentName,
                    LOSFileSizeInKB = Convert.ToDecimal(0),
                    LOSEntityId = loanFileName,//loanFileName,
                    LOSEntityUniqueId = Convert.ToString(document.FileDataID), //FileDataId,
                    CreatedDate = DateTime.UtcNow,
                    IsLOSToAIF = true,
                    LOSFileUniqueId = Convert.ToString(document.DocumentID),//EmbeddedDoc ID,
                };
            }
            catch
            {
                throw;
            }
        }
    }
}
