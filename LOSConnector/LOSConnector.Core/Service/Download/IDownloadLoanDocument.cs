﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOSConnector.Core.Service.Download
{
    public interface IDownloadLoanDocument
    {
        public Task<HttpResponseMessage> DownloadByteDocumentAndData();
    }
}
