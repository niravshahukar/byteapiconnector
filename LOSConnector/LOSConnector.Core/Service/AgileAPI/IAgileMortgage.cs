﻿using LOSConnector.Core.Data.Model.AgileAPI;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http;
using System.Text;

namespace LOSConnector.Core.Service.AgileAPI
{
    public interface IAgileMortgage
    {
        public ExpandoObject CreatePackage(string loanStatus, String LoanNumber, List<LOSMetadata> listLOSMetadata, List<LOSConnector.Core.Data.Model.AgileAPI.RuleSetMapping> RuleSetMapping = null);
        public Boolean UploadBatch(String PackageGuid, String document);
        public Boolean UpdatePackageMetadata(String PackageGuid, string loanStatus, String LoanNumber, List<LOSMetadata> listLOSMetadata);
        public HttpResponseMessage DownloadZipFile(String uploadBatchGUID, String documentPath);
        public ClassifiedBatchDetails GetClassifiedDocumentData(String uploadBatchGUID);
        public string Auth();
    }
}
