﻿using LOSConnector.Core.Data.Entity;
using LOSConnector.Core.Data.Model.AgileAPI;
using LOSConnector.Core.Data.Model.Settings;
using LOSConnector.Core.Data.Repository;
using LOSConnector.Core.Extensions;
using LOSConnector.Core.Logging;
using LOSConnector.Core.Service.AgileAPI;
using LOSConnector.Core.Service.ByteProAPI;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using LOSConnector.Core.Messages;
using Ionic.Zip;
using LOSConnector.Core.Data.Model.ByteProAPI.Document;
using Newtonsoft.Json;
using System.Reflection;
using LOSConnector.Core.Enum;
using LOSConnector.Core.Service.ByteProDB;

namespace LOSConnector.Core.Service.Upload
{
    public class UploadLoanDocument : IUploadLoanDocument
    {
        private readonly ILogger<UploadLoanDocument> _logger;
        private readonly IAgileMortgage _iAgileMortgage;
        private readonly IByteProEnterprise _iByteProEnterprise;
        private readonly IGenericRepository<LOSBatchs> _repoBatch;
        private readonly IGenericRepository<LOSBatchDetails> _repoLOSBatchDetail;
        private readonly IGenericRepository<BotConfiguration> _repoBotConfiguration;
        private readonly IGenericRepository<FieldMapping> _repoFieldMapping;
        private readonly IGenericRepository<BotFieldMapping> _repoBotFieldMapping;
        private readonly List<string> _downloadDocumentStatus;
        private ByteProData byteProData;
        private ConnectorSettings ConnectorSettings { get; }
        public UploadLoanDocument(ILogger<UploadLoanDocument> logger, IByteProEnterprise iByteProEnterprise,
            IAgileMortgage iAgileMortgage, IGenericRepository<LOSBatchs> repoBatch, IGenericRepository<BotConfiguration> repoBotConfiguration,
            IGenericRepository<LOSBatchDetails> repoLOSBatchDetail, IGenericRepository<FieldMapping> repoFieldMapping,
            IGenericRepository<BotFieldMapping> repoBotFieldMapping, IOptions<ConnectorSettings> connectorSettings)
        {
            _logger = logger;
            _iByteProEnterprise = iByteProEnterprise;
            _iAgileMortgage = iAgileMortgage;
            _repoBatch = repoBatch;
            _repoLOSBatchDetail = repoLOSBatchDetail;
            ConnectorSettings = connectorSettings.Value;
            _repoBotConfiguration = repoBotConfiguration;
            _repoFieldMapping = repoFieldMapping;
            _repoBotFieldMapping = repoBotFieldMapping;
            _downloadDocumentStatus = AppHelper.GetAppSettingsKeyValue<string>(ConnectorSettings.ByteProAPIConfig.DocumentStatus);
            byteProData = new ByteProData(connectorSettings);
        }
        /// <summary>
        /// Upload document and update form data on Byte LOS
        /// </summary>
        /// <param name="webhookObject"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> UploadByteFileDocument(WebhookObject webhookObject)
        {
            var result = string.Empty;
            var formData = string.Empty;
            var rec1 = await _repoBotConfiguration.GetAll(b => b.IsActive);
            String uploadBatchGUID = webhookObject.CommonLabels.Batch_guid;
            String loanFile = webhookObject.CommonLabels.Package_name;//Package name on Agile server is as byte loan file name
            try
            {
                if (!Directory.Exists(ConnectorSettings.FolderConfig.UploadDocPath))
                {
                    // Try to create the directory.
                    _logger.LogTrace("New directory created.");
                    Directory.CreateDirectory(ConnectorSettings.FolderConfig.UploadDocPath);
                }
                _logger.LogTrace("Process start to upload file for BatchGUID: " + uploadBatchGUID);
                var uploadgile = _iAgileMortgage.DownloadZipFile(uploadBatchGUID, Path.Combine(ConnectorSettings.FolderConfig.UploadDocPath, uploadBatchGUID + ".zip"));
                if (uploadgile.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseData = _iAgileMortgage.GetClassifiedDocumentData(uploadBatchGUID);
                    _logger.LogTrace("Start to upload file.");
                    string[] fileEntries = Directory.GetFiles(ConnectorSettings.FolderConfig.UploadDocPath, uploadBatchGUID + AppConstant.AC_DOT_Zip);
                    if (fileEntries.Contains(ConnectorSettings.FolderConfig.UploadDocPath + uploadBatchGUID + AppConstant.AC_DOT_Zip))
                    {
                        ExtractFileToDirectory(ConnectorSettings.FolderConfig.UploadDocPath + responseData.BatchGuid + AppConstant.AC_DOT_Zip, ConnectorSettings.FolderConfig.UploadDocPath + responseData.BatchGuid);
                        var pdfFiles = Directory.GetFiles(ConnectorSettings.FolderConfig.UploadDocPath + uploadBatchGUID, AppConstant.AC_Asterisk_Pdf, SearchOption.AllDirectories).Select(f => Path.GetFullPath(f)).ToList();
                        var sourceDocumentID = responseData.BatchFileName.Split(AppConstant.AC_Char_Underscore)[0];
                        var sourceDocumentDetails = _repoLOSBatchDetail.IQueryableGetMany(s => s.LOSFileUniqueId == sourceDocumentID).FirstOrDefault();
                        bool updateSourceFile = false;
                        foreach (var attachmentDetails in responseData.ProcessedDocuments)
                        {
                            var fileName = attachmentDetails.ExportFileName.Substring(attachmentDetails.ExportFileName.LastIndexOf(AppConstant.AC_String_Forward_Slash) + 1);
                            var documentPath = pdfFiles.Where(f => f.Contains(fileName)).FirstOrDefault();
                            if (!string.IsNullOrEmpty(documentPath))
                            {
                                result = await UploadDocument(attachmentDetails, sourceDocumentDetails, documentPath);
                                if (result.Contains("Invalid Data"))
                                {
                                    throw new Exception("{\"Documents\":\"" + "Failed to upload document.\"" + "," + "\"Message\":" + result + ", \"FormData\":" + "\"Failed to update form data.\"}");
                                }
                                else if (!string.IsNullOrEmpty(result))
                                {
                                    _logger.LogTrace("Process complete to upload file for BatchGUID: " + uploadBatchGUID);
                                    var res = await UpdateFormData(attachmentDetails, sourceDocumentDetails.LOSEntityUniqueId);
                                    if (res.Contains("Invalid Data"))
                                    {
                                        formData = "\"Failed to update form data.\"" + "," + "\"Message\":" + res;
                                        _logger.LogTrace("Process failed to update form data for BatchGUID: " + uploadBatchGUID);
                                    }
                                    else
                                    {
                                        formData = res;
                                        _logger.LogTrace("Process complete to update form data for BatchGUID: " + uploadBatchGUID);
                                    }
                                }
                                else
                                {
                                    _logger.LogTrace("Process not complete to upload file for BatchGUID: " + uploadBatchGUID);
                                }
                            }
                            else
                            {
                                _logger.LogTrace(fileName + " not found in upload folder.");
                            }
                        }

                        Utility.DeleteFileAndFolder(ConnectorSettings.FolderConfig.UploadDocPath + uploadBatchGUID);
                    }
                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("{\"Documents\":" + result + ", \"FormData\":" + formData + "}", System.Text.Encoding.UTF8, "application/json")
                    };
                }
                else
                {
                    return uploadgile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Update Form data in a loan
        /// </summary>
        /// <param name="processedDocument"></param>
        /// <param name="fileDataID"></param>
        public async Task<string> UpdateFormData(ProcessedDocument processedDocument, string fileDataID)
        {
            var updated = string.Empty;
            try
            {
                var fieldToUpdate = _repoFieldMapping.IQueryableGetMany(s => s.IsActive && s.ImportToLOS).ToList();
                List<LOSMetadata> lOSMetadata = new List<LOSMetadata>();
                processedDocument.ExtractedFields.ForEach(field =>
                {
                    var mappingFields = fieldToUpdate.Where(z => z.LOSFieldName == field.FieldName).ToList();
                    if (mappingFields.Count == 1)
                    {
                        if (!lOSMetadata.Exists(f => f.FieldNameAlias.ToLower() == mappingFields[0].LOSFieldNameAlias.ToLower()))
                        {
                            lOSMetadata.Add(CreateDocumentMetadataObject(mappingFields[0], field, fileDataID));
                        }
                    }
                    else if (mappingFields.Count >= 2)
                    {
                        foreach (var mappingField in mappingFields)
                        {
                            if (!lOSMetadata.Exists(f => f.FieldNameAlias.ToLower() == mappingFields[0].LOSFieldNameAlias.ToLower()))
                            {
                                lOSMetadata.Add(CreateDocumentMetadataObject(mappingFields[0], field, fileDataID));
                            }
                        }
                    }
                });
                updated = await _iByteProEnterprise.UpdateFormData(lOSMetadata);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return updated;
        }
        /// <summary>
        /// get Enum value
        /// </summary>
        /// <param name="fieldDataType"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string GetEnumData(string fieldDataType, string value)
        {
            try
            {
                Assembly assem = typeof(LoanStatus).Assembly;
                Type type = assem.GetType("LOSConnector.Core." + fieldDataType);
                if (type != null && type.IsEnum)
                {
                    return System.Enum.Parse(type, value, true).ToString();
                }
                else
                {
                    return value;
                }
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Create document metadata object
        /// </summary>
        /// <param name="fieldMapping"></param>
        /// <param name="field"></param>
        /// <param name="fileDataID"></param>
        /// <returns></returns>
        private static LOSMetadata CreateDocumentMetadataObject(FieldMapping fieldMapping, ExtractedField field, string fileDataID)
        {
            try
            {
                string[] dataRowList = fieldMapping.LOSFieldName.Trim().Split('.');
                string tableName = dataRowList[0];
                string fieldName = dataRowList[1];
                //var fieldValue = GetEnumData(fieldMapping.FieldType.FieldDataType, field.FieldValues[0].Value);
                return new LOSMetadata()
                {
                    FieldFor = fieldMapping.BorrowerFieldValue,
                    LOSFieldID = fieldName,
                    LOSTable = tableName,
                    LOSEntityID = fileDataID,
                    FieldValue = field.FieldValues[0].Value,
                    FieldNameAlias = fieldMapping.LOSFieldNameAlias
                };
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Extract pdf from downloaded zip
        /// </summary>
        /// <param name="zipFileName"></param>
        /// <param name="outputDirectory"></param>
        public void ExtractFileToDirectory(string zipFileName, string outputDirectory)
        {
            try
            {
                using (var zip = ZipFile.Read(zipFileName))
                {
                    for (int i = 0; i < zip.Count; i++)
                    {
                        ZipEntry zipEntry = zip[i];
                        if (zipEntry.FileName.LastIndexOf(AppConstant.AC_String_Forward_Slash) != -1)
                        {
                            string folderName = zipEntry.FileName.Substring(0, zipEntry.FileName.LastIndexOf(AppConstant.AC_String_Forward_Slash));
                        }
                    }
                    foreach (ZipEntry e in zip)
                    {
                        e.Extract(outputDirectory, ExtractExistingFileAction.OverwriteSilently);
                    }
                }
                Utility.DeleteFileAndFolder(zipFileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Upload document on Byte LOS
        /// </summary>
        /// <param name="attachmenDetails"></param>
        /// <param name="sourceDocumentDetails"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<string> UploadDocument(ProcessedDocument attachmenDetails, LOSBatchDetails sourceDocumentDetails, string filePath)
        {
            var result = string.Empty;
            try
            {
                String file = ConvertBase64String(filePath);
                var document = new Document()
                {
                    FileDataID = Convert.ToInt32(sourceDocumentDetails.LOSEntityUniqueId),
                    DocumentName = string.IsNullOrEmpty(sourceDocumentDetails.LOSFileName) ? ConnectorSettings.OtherConfig.FileDescription : sourceDocumentDetails.LOSFileName,
                    DocumentType = attachmenDetails.DocTypeAlias,
                    DocumentCategory = attachmenDetails.DocTypeGroup,// != "INC" ? "INC" : attachmenDetails.DocTypeGroup,
                    DocumentStatus = (int)Enum.Status.Reviewed,
                    DocumentExension = AppConstant.AC_Pdf,
                    DocumentData = file
                };
                var objDocument = JsonConvert.SerializeObject(document, new JsonSerializerSettings { StringEscapeHandling = StringEscapeHandling.EscapeHtml });
                result = await _iByteProEnterprise.AddDocument(objDocument);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, AppResource.UploadDocumentOnByteLOSFailed);
            }
            return result;
        }
        /// <summary>
        /// Create Base64String from document
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private string ConvertBase64String(string filePath)
        {
            try
            {
                String file = string.Empty;
                Byte[] bytes = File.ReadAllBytes(filePath);
                return file = Convert.ToBase64String(bytes);
            }
            catch
            {
                throw;
            }
        }
    }
}
