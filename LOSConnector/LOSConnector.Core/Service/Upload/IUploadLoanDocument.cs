﻿using LOSConnector.Core.Data.Model.AgileAPI;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOSConnector.Core.Service.Upload
{
    public interface IUploadLoanDocument
    {
        public Task<HttpResponseMessage> UploadByteFileDocument(WebhookObject webhookObject);
    }
}
