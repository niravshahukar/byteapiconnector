﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOSConnector.Core.Logging
{
    public enum LogLevel
    {
        Debug,
        Info,
        Warning,
        Error,
        Trace
    }
}
