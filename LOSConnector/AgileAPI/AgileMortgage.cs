﻿using LOSConnector.Core.Data.Entity;
using LOSConnector.Core.Data.Model.AgileAPI;
using LOSConnector.Core.Data.Model.AgileAPI.BotLoanSchema;
using LOSConnector.Core.Data.Model.Settings;
using LOSConnector.Core.Data.Repository;
using LOSConnector.Core.Extensions;
using LOSConnector.Core.Logging;
using LOSConnector.Core.Messages;
using LOSConnector.Core.Service.AgileAPI;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace AgileAPI
{
    public class AgileMortgage : IAgileMortgage
    {
        private readonly string _authToken;
        private const string _AUTHURL = "/auth/token";
        private const string _PACKAGEURL = "/v1/packages";
        private const string _BATCHURL = "/v1/batches";
        private const string _BATCHZIPURL = "/v1/downloads/batches/";
        private readonly ILogger<AgileMortgage> _logger;
        private readonly IGenericRepository<BotConfiguration> _repoBotConfiguration;
        private ConnectorSettings ConnectorSettings { get; }
        public AgileMortgage(ILogger<AgileMortgage> logger, IOptions<ConnectorSettings> connectorSettings, IGenericRepository<BotConfiguration> repoBotConfiguration)
        {
            _logger = logger;
            ConnectorSettings = connectorSettings.Value;
            _repoBotConfiguration = repoBotConfiguration;
            //get bearer token upon creation of class
            _authToken = Auth();
        }


        /// <summary>
        /// Create package on agile server
        /// </summary>
        /// <param name="loanStatus"></param>
        /// <param name="LoanNumber"></param>
        /// <param name="listLOSMetadata"></param>
        /// <param name="RuleSetMapping"></param>
        /// <returns></returns>
        public ExpandoObject CreatePackage(string loanStatus, String LoanNumber, List<LOSMetadata> listLOSMetadata, List<LOSConnector.Core.Data.Model.AgileAPI.RuleSetMapping> RuleSetMapping = null)
        {
            string packageJson = string.Empty;
            try
            {
                _logger.LogTrace("Creating new LF package");
                //First create the loan package in Leapfrog via REST API
                MetaData packageMetaData = new MetaData
                {
                    ClientId = listLOSMetadata.Find(x => x.FieldNameAlias == "LoanUniqueId").FieldValue,
                    LoanNumber = LoanNumber,
                    LoanType = (string.IsNullOrEmpty(listLOSMetadata.Find(x => x.LOSFieldID == "Loan.MortgageType")?.FieldValue) || listLOSMetadata.Find(x => x.LOSFieldID == "Loan.MortgageType")?.FieldValue == "NotAssigned") ? "Other" : listLOSMetadata.Find(x => x.LOSFieldID == "Loan.MortgageType")?.FieldValue,
                    BorrowerName = string.IsNullOrEmpty(listLOSMetadata.Find(x => x.LOSFieldID == "Borrower.FirstName")?.FieldValue) ? "BorrowerFirstName" : listLOSMetadata.Find(x => x.LOSFieldID == "Borrower.FirstName")?.FieldValue,
                    RequiredDocuments = ConnectorSettings.AgileAPIConfig.RequiredDocuments
                };
                dynamic leapfrogData = new ExpandoObject();
                dynamic lfMetaData = new ExpandoObject();
                lfMetaData.clientId = packageMetaData.ClientId;
                lfMetaData.loanNumber = packageMetaData.LoanNumber;
                lfMetaData.loanType = packageMetaData.LoanType;
                lfMetaData.borrowerName = packageMetaData.BorrowerName;
                lfMetaData.notificationOptions = packageMetaData.NotificationOptions;
                lfMetaData.processingOptions = packageMetaData.ProcessingOptions;
                lfMetaData.requiredDocuments = ConnectorSettings.AgileAPIConfig.RequiredDocuments;
                foreach (LOSMetadata item in listLOSMetadata)
                {
                    if (item.LOSFieldID != "")
                    {
                        ((IDictionary<String, object>)lfMetaData)[item.FieldNameAlias] = item.FieldValue;
                    }
                }
                lfMetaData.exportOptions = new Dictionary<string, object>() { { "documentBundling", packageMetaData.ExportOptions.documentBundling }, { "documentNaming", packageMetaData.ExportOptions.documentNaming }, { "folderGrouping", packageMetaData.ExportOptions.folderGrouping } };
                leapfrogData.metaData = Newtonsoft.Json.JsonConvert.SerializeObject(lfMetaData, new JsonSerializerSettings { StringEscapeHandling = StringEscapeHandling.EscapeHtml });
                leapfrogData.metaDataSchemaName = ConnectorSettings.AgileAPIConfig.PackageSchema;
                leapfrogData.packageName = LoanNumber;
                if (RuleSetMapping != null)
                {
                    //compare field to decide if it should be included in the ruleset
                    var rulesets = new List<string>();

                    foreach (LOSConnector.Core.Data.Model.AgileAPI.RuleSetMapping rule in RuleSetMapping)
                    {
                        switch (rule.ComparisonType)
                        {
                            case "equals":
                                if (listLOSMetadata.Where(x => x.LOSFieldID == rule.FieldID).SingleOrDefault().FieldValue.Equals(rule.Value, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    rulesets.Add(rule.RuleSet);
                                }
                                break;
                            case "instr":
                                if (listLOSMetadata.Where(x => x.LOSFieldID == rule.FieldID).SingleOrDefault().FieldValue.IndexOf(rule.Value, 0, StringComparison.InvariantCultureIgnoreCase) != -1)
                                {
                                    rulesets.Add(rule.RuleSet);
                                }
                                break;
                            case "matchany":
                                if (rule.Value.IndexOf(listLOSMetadata.Where(x => x.LOSFieldID == rule.FieldID).SingleOrDefault().FieldValue, 0, StringComparison.InvariantCultureIgnoreCase) != -1)
                                {
                                    rulesets.Add(rule.RuleSet);
                                }
                                break;
                        }
                    }

                    leapfrogData.ruleSets = rulesets.ToArray();
                }
                else
                {
                    leapfrogData.ruleSets = GetRuleAgent(loanStatus);
                }
                packageJson = Newtonsoft.Json.JsonConvert.SerializeObject(leapfrogData);
                _logger.LogTrace("Sending data to " + ConnectorSettings.AgileAPIConfig.CEHost + " data: " + packageJson);
                HttpClient restAPI = new HttpClient();
                restAPI.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
                var response = restAPI.PostAsync(ConnectorSettings.AgileAPIConfig.CEHost + _PACKAGEURL, new StringContent(packageJson, System.Text.Encoding.UTF8, AppResource.ApplicationJson)).Result;
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    //received an error on package creation, log it to the file
                    var contents = response.Content.ReadAsStringAsync();
                    if (contents != null)
                        _logger.LogTrace("ERROR: Creating new LF package, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " data: " + packageJson + " result: " + contents.Result);
                    else
                        _logger.LogTrace("ERROR: Creating new LF package, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " data: " + packageJson + " result: NULL response.");

                    return null;
                }
                else
                {
                    var contents = response.Content.ReadAsStringAsync().Result;
                    var converter = new ExpandoObjectConverter();
                    return JsonConvert.DeserializeObject<ExpandoObject>(contents, converter);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ERROR: Creating new LF package " + ConnectorSettings.AgileAPIConfig.CEHost + " data: " + packageJson);
                return null;
            }
        }
        /// <summary>
        /// Get the configured rule agent from database
        /// </summary>
        /// <param name="loanStatus"></param>
        /// <returns></returns>
        private List<string> GetRuleAgent(string loanStatus)
        {
            List<string> ruleList = new List<string>();
            try
            {
                var rules = _repoBotConfiguration.IQueryableGetMany(s => s.IsActive).ToList().Where(s => AppHelper.GetAppSettingsKeyValue<string>(s.LoanMilestone).Exists(e => e == loanStatus));
                rules.ToList().ForEach(s =>
                {
                    s.RuleAgent.Split(',').ToList().ForEach(r =>
                    {
                        if (!ruleList.Exists(e => e == r.Trim()))
                        {
                            ruleList.Add(r.Trim());
                        }
                    });
                });
                return ruleList;
            }
            catch
            {
                return ruleList;
            }
        }

        /// <summary>
        /// Update zip documents on agile server
        /// </summary>
        /// <param name="PackageGuid"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Boolean UploadBatch(String PackageGuid, String document)
        {
            try
            {
                _logger.LogTrace("Creating new LF batch for Package #" + PackageGuid + " and file " + document);

                //First create the batch in Leapfrog via REST API
                HttpClient restAPI = new HttpClient();
                restAPI.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
                restAPI.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                using (var formData = new MultipartFormDataContent())
                {
                    formData.Headers.ContentType.MediaType = "multipart/form-data";
                    HttpContent packageguidContent = new StringContent(PackageGuid);
                    formData.Add(packageguidContent, "packageguid");
                    FileStream fs = File.OpenRead(document);
                    var fileStreamContent = new StreamContent(fs);
                    fileStreamContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/zip");
                    formData.Add(fileStreamContent, "batchfile", Path.GetFileName(document));
                    var response = restAPI.PostAsync(ConnectorSettings.AgileAPIConfig.CEHost + _BATCHURL, formData).Result;
                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        //received an error on package creation, log it to the file
                        var contents = response.Content.ReadAsStringAsync();
                        if (contents != null)
                            _logger.LogTrace("ERROR: Creating new LF batch, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " Package #" + PackageGuid + " File: " + document + " result: " + contents.Result);
                        else
                            _logger.LogTrace("ERROR: Creating new LF batch, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " Package #" + PackageGuid + " File: " + document + " result: NULL response.");

                        return false;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var contents = response.Content.ReadAsStringAsync();
                        if (contents != null)
                        {
                            if (contents.Result.IndexOf("token expired") != -1)
                            {
                                Auth();
                                return UploadBatch(PackageGuid, document);
                            }
                        }

                        return false;
                    }
                    else
                    {
                        var contents = response.Content.ReadAsStringAsync();
                        if (contents != null)
                            _logger.LogTrace("Got response from LF Create Batch for Package #" + PackageGuid + " File: " + document + " result: " + contents.Result);
                        else
                            _logger.LogTrace("ERROR: Got null response but 200 OK for LF Create Batch, Package #" + PackageGuid + " File: " + document + " result: NULL response.");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Creating new LF batch, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") Package #" + PackageGuid + " and file " + document);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Update document metadata on agile server
        /// </summary>
        /// <param name="packageGuid"></param>
        /// <param name="loanStatus"></param>
        /// <param name="loanNumber"></param>
        /// <param name="listLOSMetadata"></param>
        /// <returns></returns>
        public Boolean UpdatePackageMetadata(string packageGuid, string loanStatus, String loanNumber, List<LOSMetadata> listLOSMetadata)
        {
            string packageJson = string.Empty;

            try
            {
                _logger.LogTrace("Updating LF package metadata.");
                //First create the loan package in Leapfrog via REST API
                MetaData packageMetaData = new MetaData
                {
                    ClientId = listLOSMetadata.Find(x => x.FieldNameAlias == "LoanUniqueId").FieldValue,
                    LoanNumber = loanNumber,
                    LoanType = (string.IsNullOrEmpty(listLOSMetadata.Find(x => x.LOSFieldID == "Loan.MortgageType")?.FieldValue) || listLOSMetadata.Find(x => x.LOSFieldID == "Loan.MortgageType")?.FieldValue == "NotAssigned") ? "Other" : listLOSMetadata.Find(x => x.LOSFieldID == "Loan.MortgageType")?.FieldValue,
                    BorrowerName = string.IsNullOrEmpty(listLOSMetadata.Find(x => x.LOSFieldID == "Borrower.FirstName")?.FieldValue) ? "BorrowerFirstName" : listLOSMetadata.Find(x => x.LOSFieldID == "Borrower.FirstName")?.FieldValue,
                    RequiredDocuments = ConnectorSettings.AgileAPIConfig.RequiredDocuments
                };
                dynamic leapfrogData = new ExpandoObject();
                dynamic lfMetaData = new ExpandoObject();
                lfMetaData.clientId = packageMetaData.ClientId;
                lfMetaData.loanNumber = packageMetaData.LoanNumber;
                lfMetaData.loanType = packageMetaData.LoanType;
                lfMetaData.borrowerName = packageMetaData.BorrowerName;
                lfMetaData.notificationOptions = packageMetaData.NotificationOptions;
                lfMetaData.processingOptions = packageMetaData.ProcessingOptions;
                lfMetaData.requiredDocuments = ConnectorSettings.AgileAPIConfig.RequiredDocuments;
                foreach (LOSMetadata item in listLOSMetadata)
                {
                    if (item.LOSFieldID != "")
                    {
                        ((IDictionary<String, object>)lfMetaData)[item.FieldNameAlias] = item.FieldValue;
                    }
                }
                lfMetaData.exportOptions = new Dictionary<string, object>() { { "documentBundling", packageMetaData.ExportOptions.documentBundling }, { "documentNaming", packageMetaData.ExportOptions.documentNaming }, { "folderGrouping", packageMetaData.ExportOptions.folderGrouping } };
                leapfrogData.metaData = Newtonsoft.Json.JsonConvert.SerializeObject(lfMetaData, new JsonSerializerSettings { StringEscapeHandling = StringEscapeHandling.EscapeHtml });
                leapfrogData.metaDataSchemaName = ConnectorSettings.AgileAPIConfig.PackageSchema;
                //leapfrogData.packageName = LoanNumber;
                leapfrogData.ruleSets = GetRuleAgent(loanStatus);
                packageJson = Newtonsoft.Json.JsonConvert.SerializeObject(leapfrogData);
                _logger.LogTrace("Sending data to " + ConnectorSettings.AgileAPIConfig.CEHost + " data: " + packageJson);
                HttpClient restAPI = new HttpClient();
                restAPI.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
                var response = restAPI.PutAsync(ConnectorSettings.AgileAPIConfig.CEHost + _PACKAGEURL + Path.AltDirectorySeparatorChar + packageGuid + Path.AltDirectorySeparatorChar + "metadata", new StringContent(packageJson, System.Text.Encoding.UTF8, "application/json")).Result;//By Chetu Team
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    //received an error on package creation, log it to the file
                    var contents = response.Content.ReadAsStringAsync();
                    if (contents != null)
                        _logger.LogTrace("ERROR: Creating new LF package, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " data: " + packageJson + " result: " + contents.Result);
                    else
                        _logger.LogTrace("ERROR: Creating new LF package, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " data: " + packageJson + " result: NULL response.");

                    return false;
                }
                else
                {
                    var contents = response.Content.ReadAsStringAsync().Result;
                    var converter = new ExpandoObjectConverter();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ERROR: Creating updating metadata, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") Package #" + packageGuid + " data: " + packageJson);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Download zip file from agile server
        /// </summary>
        /// <param name="uploadBatchGUID"></param>
        /// <param name="documentPath"></param>
        /// <returns></returns>
        public HttpResponseMessage DownloadZipFile(String uploadBatchGUID, String documentPath)
        {
            try
            {
                HttpClient restAPI = new HttpClient();
                restAPI.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
                var response = restAPI.GetAsync(ConnectorSettings.AgileAPIConfig.CEHost + _BATCHZIPURL + uploadBatchGUID + "/zip").Result;
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    var contents = response.Content.ReadAsStringAsync();
                    if (contents != null)
                        _logger.LogTrace("ERROR: Download zip, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " Batch Guid #" + uploadBatchGUID + " result: " + contents.Result);
                    else
                        _logger.LogTrace("ERROR:  Download zip, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " Batch Guid #" + uploadBatchGUID + " result: NULL response.");

                    return response;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    var contents = response.Content.ReadAsStringAsync();
                    if (contents != null)
                    {
                        if (contents.Result.IndexOf("token expired") != -1)
                        {
                            Auth();
                            return DownloadZipFile(uploadBatchGUID, documentPath);
                        }
                    }
                    return response;
                }
                else
                {
                    byte[] bytes = response.Content.ReadAsByteArrayAsync().Result;
                    if (bytes != null)
                    {
                        _logger.LogTrace("Got response from download zip for Batch #" + uploadBatchGUID);
                        File.WriteAllBytes(documentPath, bytes);
                    }
                    else
                    {
                        _logger.LogTrace("ERROR: Got null response but 200 OK for download zip, Batch #" + uploadBatchGUID + " result: NULL response.");
                    }
                    return response;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ERROR:Failed to download zip file, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") Batch #" + uploadBatchGUID);
                return new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("{\"Error\":\"" + "Failed to download zip file, REST API(" + ConnectorSettings.AgileAPIConfig.CEHost + ") Batch #" + uploadBatchGUID + "\"}", System.Text.Encoding.UTF8, "application/json")
                };
            }
        }
        /// <summary>
        /// Get classified batch document data
        /// </summary>
        /// <param name="uploadBatchGUID"></param>
        /// <returns></returns>
        public ClassifiedBatchDetails GetClassifiedDocumentData(String uploadBatchGUID)
        {
            ClassifiedBatchDetails classifiedBatchDetails = new ClassifiedBatchDetails();
            try
            {
                HttpClient restAPI = new HttpClient();
                restAPI.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
                var response = restAPI.GetAsync(ConnectorSettings.AgileAPIConfig.CEHost + _BATCHURL + "/" + uploadBatchGUID).Result;
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    var contents = response.Content.ReadAsStringAsync();
                    if (contents != null)
                        _logger.LogTrace("ERROR: Retrieve Batch, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " Batch Guid #" + uploadBatchGUID + " result: " + contents.Result);
                    else
                        _logger.LogTrace("ERROR: Retrieve Batch, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " Batch Guid #" + uploadBatchGUID + " result: NULL response.");

                    return classifiedBatchDetails;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    var contents = response.Content.ReadAsStringAsync();
                    if (contents != null)
                    {
                        if (contents.Result.IndexOf("token expired") != -1)
                        {
                            Auth();
                            return GetClassifiedDocumentData(uploadBatchGUID);
                        }
                    }
                    return classifiedBatchDetails;
                }
                else
                {
                    var contents = response.Content.ReadAsStringAsync();
                    if (contents != null)
                    {
                        _logger.LogTrace("Got response from Retrieve Batch, Batch #" + uploadBatchGUID);
                        classifiedBatchDetails = JsonConvert.DeserializeObject<ClassifiedBatchDetails>(contents.Result);
                    }
                    else
                    {
                        _logger.LogTrace("ERROR: Got null response but 200 OK for Retrieve Batch, Batch #" + uploadBatchGUID + " result: NULL response.");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogTrace("ERROR: Retrieve Batch, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") Batch #" + uploadBatchGUID + " exception: " + ex.ToString());
            }
            return classifiedBatchDetails;
        }

        /// <summary>
        /// Generate token authentication the api
        /// </summary>
        /// <returns></returns>
        public string Auth()
        {
            // var _utilityManager = new UtilityManager();
            //string test = "";
            try
            {
                //First create the loan package in Leapfrog via REST API
                HttpClient restAPI = new HttpClient();
                var formData = new FormUrlEncodedContent(new[] {
                     new KeyValuePair<string, string>("username", ConnectorSettings.AgileAPIConfig.CEUser),
                     new KeyValuePair<string, string>("password", ConnectorSettings.AgileAPIConfig.CEPassword),
                     new KeyValuePair<string, string>("grant_type", "password"),
                     new KeyValuePair<string, string>("client_id", "aim-api-gateway"),
                     new KeyValuePair<string, string>("client_secret", ConnectorSettings.AgileAPIConfig.CESecret)
                });
                var response = restAPI.PostAsync(ConnectorSettings.AgileAPIConfig.CEHost + _AUTHURL, formData).Result;
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    //received an error with auth
                    var contents = response.Content.ReadAsStringAsync();
                    if (contents != null)
                        _logger.LogTrace("ERROR: Getting JWT token from REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " result: " + contents.Result);
                    else
                        _logger.LogTrace("ERROR: Creating new LF batch, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") returned error code: " + response.StatusCode + " result: NULL response.");

                    return "";
                }
                else
                {
                    //get token from response;
                    var contents = response.Content.ReadAsStringAsync().Result;
                    var converter = new ExpandoObjectConverter();
                    dynamic jsonResponse = JsonConvert.DeserializeObject<ExpandoObject>(contents, converter);
                    return jsonResponse.access_token;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ERROR: Creating generating auth token, REST API (" + ConnectorSettings.AgileAPIConfig.CEHost + ") ");
                return "";
            }
        }
    }
}
