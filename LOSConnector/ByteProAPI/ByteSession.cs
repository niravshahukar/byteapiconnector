﻿using LOSConnector.Core.Data.Model.Settings;
using LOSConnector.Core.Service.ByteProAPI;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ByteProAPI
{
    public sealed class ByteSession : IByteSession
    {
        private const string _authURL = "/byteapi/auth/";
        private string _bytesession = null;
        private ConnectorSettings _connectorSettings { get; }
        public ByteSession(IOptions<ConnectorSettings> connectorSettings)
        {
            _connectorSettings = connectorSettings.Value;
        }
        private static readonly object sessionlock = new object();

        /// <summary>
        /// Generate byte session for use api
        /// </summary>
        /// <returns></returns>
        public string Auth()
        {
            string byteSession = string.Empty;
            try
            {
                using (var client = new System.Net.WebClient())
                {
                    ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                    client.Headers.Add("username", _connectorSettings.ByteProAPIConfig.UserName);
                    client.Headers.Add("password", _connectorSettings.ByteProAPIConfig.Password);
                    client.Headers.Add("authorizationkey", _connectorSettings.ByteProAPIConfig.AuthorizationKey);
                    client.Headers.Add("Accept", "application/json");
                    client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                    byteSession = client.DownloadString(_connectorSettings.ByteProAPIConfig.ServerName + "/" + _connectorSettings.ByteProAPIConfig.SiteName + _authURL);
                    _bytesession = byteSession.Replace("\"", "");
                }
            }
            catch
            {
                throw;
            }
            return byteSession = _bytesession;
        }
        /// <summary>
        /// Get byte session
        /// </summary>
        public string GetSession
        {
            get
            {
                lock (sessionlock)
                {
                    if (_bytesession == null)
                    {
                        _bytesession = Auth();
                    }
                    return _bytesession;
                }
            }
        }
    }
}
