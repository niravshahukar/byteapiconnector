﻿using LOSConnector.Core.Data.Model.AgileAPI;
using LOSConnector.Core.Data.Model.ByteProAPI.Document;
using LOSConnector.Core.Data.Model.ByteProAPI.SearchLoan;
using LOSConnector.Core.Data.Model.Settings;
using LOSConnector.Core.Logging;
using LOSConnector.Core.Service.ByteProAPI;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace ByteProAPI
{
    public class ByteProEnterprise : IByteProEnterprise
    {
        private const string _searchLoanURL = "/byteapi/Search";
        private const string _loanFileURL = "/byteapi/loanfile";
        private const string _loanDocumentURL = "/byteapi/Document";
        private const string _loanStatusURL = "/byteapi/loanstatus";
        private const string _loanBorrowerURL = "/byteapi/borrower";
        public string _byteSession;
        private readonly ILogger<ByteProEnterprise> _logger;
        private readonly IByteSession _iByteSession;
        private ConnectorSettings ConnectorSettings { get; }
        public ByteProEnterprise(ILogger<ByteProEnterprise> logger, IOptions<ConnectorSettings> connectorSettings,
            IByteSession iByteSession)
        {
            _logger = logger;
            ConnectorSettings = connectorSettings.Value;
            _iByteSession = iByteSession;
        }
        /// <summary>
        /// Search Byte loan based on search criteria
        /// </summary>
        /// <param name="loanSearch"></param>
        /// <returns></returns>
        public async Task<string> SearchByStatus(LoanSearch loanSearch)
        {
            string loanSearchData = null;
            try
            {
                string inputSearchBody = SerializeObject(loanSearch);
                var client = new RestClient(ConnectorSettings.ByteProAPIConfig.ServerName + "/" + ConnectorSettings.ByteProAPIConfig.SiteName + _searchLoanURL)
                {
                    Timeout = -1
                };
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json ");
                request.AddHeader("Session", _iByteSession.GetSession);
                request.AddParameter("application/json", inputSearchBody, ParameterType.RequestBody);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Content.Contains("Invalid Authorization. Session ID is invalid."))
                {
                    _byteSession = _iByteSession.GetSession;
                    await SearchByStatus(loanSearch);
                }
                else
                {
                    loanSearchData = response.Content;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Loan search process failed.");
                throw;
            }
            return loanSearchData;
        }
        /// <summary>
        /// Get a loan all document
        /// </summary>
        /// <param name="fileDataID"></param>
        /// <returns></returns>
        public async Task<string> GetAllLoanDocuments(string fileDataID)
        {
            string allDocuments = string.Empty;
            try
            {
                var client = new RestClient(ConnectorSettings.ByteProAPIConfig.ServerName + "/" + ConnectorSettings.ByteProAPIConfig.SiteName + _loanDocumentURL + "/" + fileDataID)
                {
                    Timeout = -1
                };
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json ");
                request.AddHeader("Session", _iByteSession.GetSession);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Content.Contains("Invalid Authorization. Session ID is invalid."))
                {
                    _byteSession = _iByteSession.GetSession;
                    await GetAllLoanDocuments(fileDataID);
                }
                else
                {
                    allDocuments = response.Content;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Get all document process failed.");
                throw;
            }
            return allDocuments;
        }
        /// <summary>
        /// Get a loan single document based on document ID
        /// </summary>
        /// <param name="fileDataID"></param>
        /// <param name="documentID"></param>
        /// <returns></returns>
        public async Task<string> GetSingleDocument(string fileDataID, string documentID)
        {
            string oneDocument = string.Empty;
            try
            {
                var client = new RestClient(ConnectorSettings.ByteProAPIConfig.ServerName + "/" + ConnectorSettings.ByteProAPIConfig.SiteName + _loanDocumentURL + "/" + fileDataID + "/" + documentID)
                {
                    Timeout = -1
                };
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json ");
                request.AddHeader("Session", _iByteSession.GetSession);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Content.Contains("Invalid Authorization. Session ID is invalid."))
                {
                    _byteSession = _iByteSession.GetSession;
                    await GetSingleDocument(fileDataID, documentID);
                }
                else
                {
                    oneDocument = response.Content;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Get single document process failed.");
                throw;
            }
            return oneDocument;
        }
        /// <summary>
        /// Get a loan form data
        /// </summary>
        /// <param name="fileDataID"></param>
        /// <returns></returns>
        public async Task<string> GetLoanData(string fileDataID)
        {
            string allLoanData = string.Empty;
            try
            {
                var client = new RestClient(ConnectorSettings.ByteProAPIConfig.ServerName + "/" + ConnectorSettings.ByteProAPIConfig.SiteName + _loanFileURL + "/" + fileDataID)
                {
                    Timeout = -1
                };
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json ");
                request.AddHeader("Session", _iByteSession.GetSession);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Content.Contains("Invalid Authorization. Session ID is invalid."))
                {
                    _byteSession = _iByteSession.GetSession;
                    await GetLoanData(fileDataID);
                }
                else
                {
                    allLoanData = response.Content;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Get all loan data process failed.");
                throw;
            }
            return allLoanData;
        }
        /// <summary>
        /// Add new attachment in a loan
        /// </summary>
        /// <param name="documentDetails"></param>
        /// <returns></returns>
        public async Task<string> AddDocument(string documentDetails)
        {
            string uploadDocument = string.Empty;
            try
            {
                var client = new RestClient(ConnectorSettings.ByteProAPIConfig.ServerName + "/" + ConnectorSettings.ByteProAPIConfig.SiteName + _loanDocumentURL)
                {
                    Timeout = -1
                };
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json ");
                request.AddHeader("Session", _iByteSession.GetSession);
                request.AddParameter("application/json", documentDetails, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized || response.Content.Contains("Invalid Authorization. Session ID is invalid."))
                {
                    _byteSession = _iByteSession.GetSession;
                    await AddDocument(documentDetails);
                }
                else
                {
                    uploadDocument = response.Content;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Upload document process failed.");
                throw ex;
            }
            return uploadDocument;
        }
        /// <summary>
        /// Update Extended and Custom field value byte LOS based on file data ID
        /// </summary>
        /// <returns></returns>
        public async Task<string> UpdateFormData(List<LOSMetadata> lOSMetadata)
        {
            var updated = string.Empty;
            var allData = lOSMetadata.GroupBy(x => x.LOSTable).ToList();
            try
            {
                dynamic byteLoanData = new ExpandoObject();
                RestClient client;
                //URL for update borrower details
                client = new RestClient(ConnectorSettings.ByteProAPIConfig.ServerName + "/" + ConnectorSettings.ByteProAPIConfig.SiteName + _loanFileURL)
                {
                    Timeout = -1
                };
                var fileDataID = lOSMetadata[0].LOSEntityID;
                byteLoanData.FileDataID = fileDataID;
                allData.ForEach(s =>
                {
                    var DyObjectsList = new List<dynamic>();
                    if (s.Key.Contains("Extended"))
                    {
                        foreach (LOSMetadata item in s)
                        {
                            dynamic tableData = new ExpandoObject();
                            tableData.FileDataID = fileDataID;
                            if (item.LOSFieldID != "")
                            {
                                ((IDictionary<String, object>)tableData)["Name"] = item.LOSFieldID;
                                ((IDictionary<String, object>)tableData)["Value"] = item.FieldValue;
                            }
                            DyObjectsList.Add(tableData);
                        }
                ((IDictionary<String, object>)byteLoanData)[s.Key + "s"] = DyObjectsList;
                    }
                    else if (s.Key.Contains("Custom"))
                    {
                        dynamic tableData = new ExpandoObject();
                        tableData.CustomFieldsID = fileDataID;//Custom field ID is required
                        tableData.FileDataID = fileDataID;//File data ID is required
                        foreach (LOSMetadata item in s)
                        {
                            if (item.LOSFieldID != "")
                            {
                                ((IDictionary<String, object>)tableData)[item.LOSFieldID] = item.FieldValue;
                            }
                        }
                        ((IDictionary<String, object>)byteLoanData)[s.Key + "s"] = tableData;
                    }
                });
                var packageJson = Newtonsoft.Json.JsonConvert.SerializeObject(byteLoanData);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json ");
                request.AddHeader("Session", _iByteSession.GetSession);
                request.AddParameter("application/json", packageJson, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized || response.Content.Contains("Invalid Authorization. Session ID is invalid."))
                {
                    _byteSession = _iByteSession.GetSession;
                    await UpdateFormData(lOSMetadata);
                }
                else
                {
                    updated = response.Content;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Update loan data process failed.");
                throw ex;
            }
            return updated;
        }
        /// <summary>
        /// It converts object to JSON format text
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private string SerializeObject(object data)
        {
            try
            {
                return JsonConvert.SerializeObject(data, Formatting.Indented);
            }
            catch
            {
                throw;
            }
        }
    }
}
