﻿using AgileAPI;
using ByteProAPI;
using LOSConnector.Core.Data.Entity;
using LOSConnector.Core.Data.Model.Settings;
using LOSConnector.Core.Data.Repository;
using LOSConnector.Core.Logging;
using LOSConnector.Core.Service;
using LOSConnector.Core.Service.AgileAPI;
using LOSConnector.Core.Service.ByteProAPI;
using LOSConnector.Core.Service.Download;
using LOSConnector.Core.Service.Upload;
using LOSConnector.Infrastructure.Database.Repository;
using LOSConnector.Infrastructure.Logging;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOSConnector.Web
{
    public static class DependencyResolver
    {
        public static void RepositoriesDependency(this IServiceCollection services)
        {
            //// Repositories
            services.AddScoped<IGenericRepository<LOSBatchs>, GenericRepository<LOSBatchs>>();
            services.AddScoped<IGenericRepository<LOSBatchDetails>, GenericRepository<LOSBatchDetails>>();
            services.AddScoped<IGenericRepository<FieldMapping>, GenericRepository<FieldMapping>>();
            services.AddScoped<IGenericRepository<BotConfiguration>, GenericRepository<BotConfiguration>>();
            services.AddScoped<IGenericRepository<BotFieldMapping>, GenericRepository<BotFieldMapping>>();
        }
        public static void ServicesDependency(this IServiceCollection services)
        {
            // Services
            services.AddScoped<IAgileMortgage, AgileMortgage>();
            services.AddScoped<IByteProEnterprise, ByteProEnterprise>();
            services.AddScoped<IDownloadLoanDocument, DownloadLoanDocument>();
            services.AddScoped<IUploadLoanDocument, UploadLoanDocument>();
            services.AddSingleton<IByteSession, ByteSession>();
        }
        public static void LoggingDependency(this IServiceCollection services)
        {
            // Logging
            services.AddScoped(typeof(ILogger<>), typeof(NLogLogger<>));
        }
    }
}
