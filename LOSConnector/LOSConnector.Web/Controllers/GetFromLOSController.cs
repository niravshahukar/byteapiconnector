﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using LOSConnector.Core.Logging;
using LOSConnector.Core.Service.Download;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LOSConnector.Web.Controllers
{
    [Route("V1")]
    [ApiController]
    public class GetFromLOSController : ControllerBase
    {
        private readonly ILogger<GetFromLOSController> _logger;
        //
        private readonly IDownloadLoanDocument _iDownloadLoanDocument;
        public GetFromLOSController(ILogger<GetFromLOSController> logger, IDownloadLoanDocument iDownloadLoanDocument)
        {
            _logger = logger;
            _iDownloadLoanDocument = iDownloadLoanDocument;
        }
        [HttpGet]
        [Route("documents/download")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _iDownloadLoanDocument.DownloadByteDocumentAndData();
                return Ok(result.Content.ReadAsStringAsync().Result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Download document process failed.");
                var converter = new ExpandoObjectConverter();
                var jsonError = JsonConvert.DeserializeObject<ExpandoObject>("{\"Error\":\"" + "Download document process failed.\"" + "," + "\"Message\":\"" + ex.Message + "\"}", converter);
                return BadRequest(jsonError);
            }
        }
    }
}