﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using LOSConnector.Core.Data.Model.AgileAPI;
using LOSConnector.Core.Logging;
using LOSConnector.Core.Service.Upload;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LOSConnector.Web.Controllers
{
    [Route("V1")]
    [ApiController]
    public class UploadToLOSController : ControllerBase
    {
        private readonly ILogger<UploadToLOSController> _logger;
        //
        private readonly IUploadLoanDocument _iUploadLoanDocument;
        public UploadToLOSController(ILogger<UploadToLOSController> logger, IUploadLoanDocument iUploadLoanDocument)
        {
            _logger = logger;
            _iUploadLoanDocument = iUploadLoanDocument;
        }
        [HttpPost]
        [Route("documents/upload")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UploadToLOS([FromBody] WebhookObject webhookObject)
        {
            try
            {
                var result = await _iUploadLoanDocument.UploadByteFileDocument(webhookObject);
                var converter = new ExpandoObjectConverter();
                var docDetails = JsonConvert.DeserializeObject<ExpandoObject>(result.Content.ReadAsStringAsync().Result, converter);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return NotFound(docDetails);
                }
                else if (result.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    return BadRequest(docDetails);
                }
                else
                {
                    return NotFound(docDetails);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to upload document and update form data. Exception: " + Convert.ToString(ex));
                return BadRequest(Convert.ToString(ex));
            }
        }
    }
}