using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOSConnector.Core.Data.Model.Settings;
using LOSConnector.Core.Service;
using LOSConnector.Infrastructure.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace LOSConnector.Web
{
    public class Startup
    {
        public IConfiguration _configuration { get; }
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Database
            services.AddDbContextPool<ApplicationDbContext>(options =>
            options.UseSqlServer(_configuration.GetConnectionString("LOSConnectorDB")));
            services.Configure<ConnectorSettings>(_configuration.GetSection("ConnectorSettings"));
            services.RepositoriesDependency();
            services.ServicesDependency();
            services.LoggingDependency();            
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
